from multiprocessing import Process, Queue

def foo(q, l:list):
  q.put(l)

if __name__=='__main__':
  q = Queue()
  p1 = Process(target=foo, args=(q,[42, None, 'process 1']))
  p2 = Process(target=foo, args=(q,[31, None, 'process 2']))
  p1.start()
  print(q.get())
  p1.join()

  p2.start()
  print(q.get())
  p2.join()