import multiprocessing as mp

def foo(q):
  q.put('hello')

if __name__=='__main__':
  # 'set_start_method'는 한 번만 사용되어야함.
  mp.set_start_method('spawn')
  q = mp.Queue()
  p = mp.Process(target=foo,args=(q,))
  p.start()
  print(q.get())
  p.join()