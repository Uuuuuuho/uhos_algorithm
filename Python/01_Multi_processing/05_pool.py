import time
from multiprocessing import Pool

def count(proc_name):
  for i in range(1,10000):
    print(proc_name, i)

if __name__=='__main__':
  start_time = time.time()
  p_list = ['proc1','proc2','proc3','proc4']
  pool = Pool(processes=4)
  pool.map(count, p_list)
  pool.close()
  pool.join()

  print(time.time() - start_time)
