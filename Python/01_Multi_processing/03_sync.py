from multiprocessing import Process, Lock

def foo(l, i):
  l.acquire()
  try:
    print('hello world',i)
  finally:
    l.release()

def boo(i):
  print('boo', i)
    
if __name__=='__main__':
  lock = Lock()

  for num in range(10):
    Process(target=boo, args=(num,)).start()
    # Process(target=foo, args=(lock, num)).start()