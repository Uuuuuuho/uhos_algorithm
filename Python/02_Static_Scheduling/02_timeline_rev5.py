﻿import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets, QtCore, QtGui


class Task(object):
    def __init__(self, name, start_time, end_time, period, wcet):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.period = period
        self.wcet = wcet


class TimelineWidget(QtWidgets.QWidget):
    def __init__(self, tasks, start_time, end_time, time_scale):
        super().__init__()
        self.tasks = tasks
        self.start_time = start_time
        self.end_time = end_time
        self.time_scale = time_scale

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)

        font = qp.font()
        font.setPointSize(10)
        qp.setFont(font)

        fm = QtGui.QFontMetrics(font)

        width = self.width()
        height = self.height()

        x_offset = fm.width(str(self.end_time)) + 10
        y_offset = fm.height() + 10

        task_height = (height - y_offset) / len(self.tasks)

        for i, task in enumerate(self.tasks):
            x1 = (task.start_time - self.start_time) * width / (self.end_time - self.start_time) + x_offset
            x2 = (task.end_time - self.start_time) * width / (self.end_time - self.start_time) + x_offset
            y1 = i * task_height + y_offset
            y2 = (i + 1) * task_height
            qp.fillRect(x1, y1, x2 - x1, y2 - y1, QtGui.QColor(0, 0, 255))
            qp.drawText(QtCore.QRect(x1, y1, x2 - x1, y2 - y1), QtCore.Qt.AlignCenter, task.name)

        # Draw vertical axis
        qp.drawLine(x_offset - 5, y_offset, x_offset - 5, height)

        # Draw horizontal axis
        qp.drawLine(x_offset, height - y_offset + 5, width, height - y_offset + 5)

        # Draw horizontal ticks and labels
        for i in range(self.start_time, self.end_time + self.time_scale, self.time_scale):
            x = (i - self.start_time) * width / (self.end_time - self.start_time) + x_offset
            qp.drawLine(x, height - y_offset + 5, x, height - y_offset + 10)
            qp.drawText(x - fm.width(str(i)) / 2, height - 5, str(i))

        qp.end()

class MainWindow(QMainWindow):
    def __init__(self, tasks, num_cores):
        super().__init__()

        self.tasks = tasks
        self.num_cores = num_cores

        self.scale_spinbox = QDoubleSpinBox()
        self.scale_spinbox.setDecimals(2)
        self.scale_spinbox.setSingleStep(0.25)
        self.scale_spinbox.setMinimum(0.25)
        self.scale_spinbox.setMaximum(10.0)
        self.scale_spinbox.setValue(1.0)
        self.scale_spinbox.valueChanged.connect(self.update_scene)

        self.scene = QGraphicsScene(self)
        self.view = self.scene.views()[0]
        self.view.setRenderHint(QPainter.Antialiasing)
        self.view.setDragMode(QGraphicsView.ScrollHandDrag)

        self.setCentralWidget(self.view)
        self.add_tasks_to_scene()
        self.update_scene()

        self.setWindowTitle("Timeline Visualizer")
        self.setGeometry(100, 100, 800, 600)
        self.show()

    def add_tasks_to_scene(self):
        for i, task in enumerate(self.tasks):
            color = Qt.GlobalColor(i % len(Qt.GlobalColor.__members__))
            text = QGraphicsTextItem(task[0])
            text.setDefaultTextColor(color)
            text.setPos(-120, 25 + i * 50)
            self.scene.addItem(text)
            for j in range(self.num_cores):
                x1 = (task[1] / self.scale_spinbox.value()) + j * 100
                x2 = (task[2] / self.scale_spinbox.value()) + j * 100
                period = task[3] / self.scale_spinbox.value()
                while x1 < self.view.width():
                    line = QGraphicsLineItem(x1, 40 + i * 50, x2, 40 + i * 50)
                    line.setPen(Qt.PenStyle.SolidLine)
                    line.setPen(Qt.black)
                    self.scene.addItem(line)
                    x1 += period
                    x2 += period

    def update_scene(self):
        self.scene.clear()
        self.add_tasks_to_scene()

if __name__ == '__main__':
    
    tasks = [
        ('Task 1', 0, 5, 20, 4),
        ('Task 2', 0, 4, 16, 2),
        ('Task 3', 2, 6, 10, 3),
        ('Task 4', 3, 9, 18, 5),
        ('Task 5', 1, 3, 6, 1)
    ]
    num_cores = 4

    app = QApplication(sys.argv)
    window = MainWindow(tasks, num_cores)
    sys.exit(app.exec_())