﻿import networkx as nx
import matplotlib.pyplot as plt

# Create a graph with vertices and edges
G = nx.Graph()
G.add_nodes_from(['A', 'B', 'C', 'D', 'E'])
G.add_edges_from([('A', 'B'), ('B', 'C'), ('C', 'D'), ('D', 'E'), ('E', 'A')])

# Create a layout for the graph nodes
pos = nx.circular_layout(G)

# Draw the graph nodes and edges using Matplotlib
nx.draw_networkx_nodes(G, pos, node_color='lightblue', node_size=500)
nx.draw_networkx_edges(G, pos, edge_color='grey')
nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')

# Show the plot
plt.axis('off')
plt.show()