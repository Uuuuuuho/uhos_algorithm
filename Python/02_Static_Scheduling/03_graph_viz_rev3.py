﻿from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys

class Graph(QWidget):
    def __init__(self, tasks, predecessors, successors):
        super().__init__()
        self.tasks = tasks
        self.predecessors = predecessors
        self.successors = successors
        self.setGeometry(200, 200, 800, 600)
        self.show()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        qp.setPen(QPen(Qt.black, 2))
        qp.setFont(QFont('Arial', 10))

        # Draw edges
        for task, pred_list in self.predecessors.items():
            x1, y1 = self.tasks[task]
            for pred in pred_list:
                x2, y2 = self.tasks[pred]
                qp.drawLine(x1, y1, x2, y2)

        # Draw vertices
        for task, (x, y) in self.tasks.items():
            qp.setBrush(QBrush(Qt.white))
            qp.drawEllipse(x-20, y-20, 40, 40)
            qp.setBrush(QBrush(Qt.blue))
            qp.drawEllipse(x-15, y-15, 30, 30)
            qp.drawText(QRect(x-20, y-20, 40, 40), Qt.AlignCenter, task)

        qp.end()


if __name__ == '__main__':
    # Sample input data
    tasks = {'A': (50, 50), 'B': (150, 150), 'C': (250, 250), 'D': (350, 350)}
    predecessors = {'B': ['A'], 'C': ['B', 'A'], 'D': ['C', 'B']}
    successors = {'A': ['B', 'C'], 'B': ['C', 'D'], 'C': ['D']}

    app = QApplication(sys.argv)
    graph = Graph(tasks, predecessors, successors)
    sys.exit(app.exec_())