def list_scheduling(num_inst, num_proc, inst_dep, inst_cycles):
    # num_inst: number of instructions
    # num_proc: number of processors
    # inst_dep: instruction dependencies
    # inst_cycles: instruction execution times
    
    # Initialize the ready list with instructions that have no dependencies
    ready_list = [i for i in range(num_inst) if -1 in inst_dep[i]]
    print(ready_list)
    
    # Initialize the schedule
    schedule = [-1] * num_inst
    
    # Initialize the processor list
    processors = [0] * num_proc
    
    # Iterate until all instructions have been scheduled
    while ready_list:
        # Find the instruction with the earliest ready time
        inst_idx = min(ready_list, key=lambda inst_idx: max(processors) + inst_cycles[inst_idx])
        
        # Find the processor with the earliest available time
        proc = processors.index(min(processors))
        
        # Schedule the instruction on the processor
        schedule[inst_idx] = proc
        processors[proc] += inst_cycles[inst_idx]
        
        # Update the ready list
        ready_list.remove(inst_idx)
        for j in range(num_inst):
            if inst_idx in inst_dep[j]:
                inst_dep[j].remove(inst_idx)
                if not inst_dep[j]:
                    ready_list.append(j)
    
    # Return the schedule
    return schedule, processors


if __name__ == '__main__':
    # example set of instructions
    inst_dep = [[1], [-1], [-1], [-1]]

    inst_cycles = [ 1, 3, 1, 1 ]
    
    num_proc = 2
    num_inst = 4
    
    # run list scheduling algorithm
    schedule, processors = list_scheduling(num_inst, num_proc, inst_dep, inst_cycles)
    
    # print results
    print('Schedule:', schedule)
    print('Processors:', processors)