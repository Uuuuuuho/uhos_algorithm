﻿import argparse
import csv
import matplotlib.pyplot as plt

# Define a Task class to store task information
class Task:
    def __init__(self, name, wcet, period, start_time, end_time):
        self.name = name
        self.wcet = wcet
        self.period = period
        self.start_time = start_time
        self.end_time = end_time

# Parse command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('cores', type=int, help='number of cores')
parser.add_argument('input_file', type=str, help='input CSV file')
args = parser.parse_args()

# Load task data from input CSV file
tasks = []
with open(args.input_file, 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        task = Task(*row)
        tasks.append(task)

# Sort tasks by their start times
tasks.sort(key=lambda x: x.start_time)

# Initialize the timeline
timeline = [[] for _ in range(args.cores)]

# Iterate over the tasks and assign them to cores
for task in tasks:
    assigned = False
    for core in range(args.cores):
        # Check if the core is available during the task's execution time
        if all(task.start_time >= t.end_time or task.end_time <= t.start_time for t in timeline[core]):
            # Assign the task to the core
            timeline[core].append(task)
            assigned = True
            break
    # If the task was not assigned to any core, print an error message
    if not assigned:
        print(f'Error: could not assign task {task.name} to any core')

# Generate a timeline plot
fig, ax = plt.subplots(figsize=(10, 5))
ylabels = [f'Core {core}' for core in range(args.cores)]
ax.set_yticks(range(args.cores))
ax.set_yticklabels(ylabels)
ax.set_ylim(-0.5, args.cores - 0.5)
ax.set_xlabel('Time')
ax.grid(True)

# Plot the tasks on the timeline
for core, tasks in enumerate(timeline):
    for task in tasks:
        color = 'C' + str(core)
        duration = task.end_time - task.start_time
        rect = plt.Rectangle((task.start_time, core - 0.25), duration, 0.5, color=color)
        ax.add_patch(rect)
        ax.text(task.start_time + duration / 2, core, task.name, ha='center', va='center')

# Show the plot
plt.show()

""" 
    To use this program, save the code to a file (e.g., **`timeline.py`**) 
    and run it from the command line, passing the number of cores and the input CSV file as arguments:

``` bash
    python 02_timeline.py 4 input.csv
```

    Replace **`4`** with the desired number of cores and **`input.csv`** 
    with the actual path to your input CSV file. The program will generate 
    a timeline plot showing the execution of tasks on each core.
 """