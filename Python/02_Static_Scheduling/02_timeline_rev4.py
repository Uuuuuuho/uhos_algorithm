﻿import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class TimelineVisualizer(QWidget):
    def __init__(self, tasks, num_cores, parent=None):
        super().__init__(parent)
        self.tasks = tasks
        self.num_cores = num_cores
        self.initUI()

    def initUI(self):
        self.setMinimumSize(800, 600)
        self.setWindowTitle("Timeline Visualizer")
        self.layout = QGridLayout()
        self.layout.setSpacing(10)
        self.setLayout(self.layout)

        # Vertical labels for task names
        for i in range(len(self.tasks)):
            label = QLabel(self.tasks[i][0])
            label.setAlignment(Qt.AlignRight)
            self.layout.addWidget(label, i+1, 0)

        # Horizontal labels for time scale
        for i in range(self.tasks[0][4]+1):
            label = QLabel(str(i))
            label.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(label, 0, i+1)

        # Draw task bars
        for i in range(len(self.tasks)):
            task = self.tasks[i]
            row = i+1
            start_time = task[1]
            end_time = task[2]
            period = task[3]
            wcet = task[4]
            period_count = 0

            while start_time <= self.tasks[0][4]:
                core = period_count % self.num_cores
                color = QColor(0, 0, 0)
                color.setHsvF(core/self.num_cores, 1, 1)

                start = max(start_time, period_count * period)
                end = min(end_time, start + wcet)
                duration = end - start
                if duration > 0:
                    bar = QFrame(self)
                    bar.setStyleSheet("background-color: %s" % color.name())
                    bar.setFrameShape(QFrame.StyledPanel)
                    bar.setFixedSize(duration * 40, 20)
                    self.layout.addWidget(bar, row, start+1)

                start_time += period
                period_count += 1

        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    tasks = [("Task A", 0, 4, 4, 1), ("Task B", 2, 6, 6, 1), ("Task C", 4, 8, 8, 2), ("Task D", 3, 7, 7, 2)]
    num_cores = 2
    ex = TimelineVisualizer(tasks, num_cores)
    sys.exit(app.exec_())