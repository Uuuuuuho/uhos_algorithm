﻿import sys
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen
from PyQt5.QtWidgets import QApplication, QGraphicsScene, QGraphicsView, QGraphicsTextItem, QGraphicsRectItem

# Define a Task class to store task information
class Task:
    def __init__(self, name, start_time, end_time, period, wcet):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.period = period
        self.wcet = wcet

# Create a list of tasks
tasks = [Task('Task A', 0, 3, 10, 2),
         Task('Task B', 2, 5, 12, 3),
         Task('Task C', 6, 8, 15, 1),
         Task('Task D', 8, 12, 20, 4)]

class TimelineView(QGraphicsView):
    def __init__(self):
        super().__init__()

        # Set up the scene
        self.scene = QGraphicsScene()
        self.setScene(self.scene)
        
        # Set the default pen and brush
        self.pen = QPen(Qt.black)
        self.brush = QBrush(Qt.blue)
        
        # Add the tasks to the scene
        for task in tasks:
            self.add_task(task)

        # Set the view size and show it
        self.setFixedSize(800, 600)
        self.show()

    def add_task(self, task):
        # Calculate the task dimensions
        x = task.start_time * 20
        y = len(self.scene.items()) * 50
        width = task.end_time * 20 - x
        height = 40
        
        # Create the task rectangle and text
        rect = QGraphicsRectItem(x, y, width, height)
        rect.setPen(self.pen)
        rect.setBrush(self.brush)
        text = QGraphicsTextItem(task.name)
        text.setPos(x + width/2 - text.boundingRect().width()/2, y + height/2 - text.boundingRect().height()/2)
        
        # Add the rectangle and text to the scene
        self.scene.addItem(rect)
        self.scene.addItem(text)

# Create the application and view
app = QApplication(sys.argv)
view = TimelineView()

# Run the application
sys.exit(app.exec_())

""" 
In this example, we use PyQt5 to create a **`QGraphicsView`** that displays a timeline with rectangles representing each task. The **`Task`** class is used to store information about each task, including its name, start time, end time, period, and worst case execution time (WCET). The **`tasks`** list is then defined with four tasks of varying durations and start times.

The **`TimelineView`** class is a subclass of **`QGraphicsView`** that sets up the scene, adds the tasks to the scene, and displays the view. The **`add_task`** method is used to add a single task to the scene. It calculates the dimensions of the task rectangle based on the task's start time, end time, and other properties, and creates a **`QGraphicsRectItem`** and **`QGraphicsTextItem`** to represent the task rectangle and label. These items are then added to the scene using the **`addItem`** method.

Finally, the **`app`** and **`view`** objects are created and the application is run using **`app.exec_()`**. You can modify the list of tasks in the program to display your own timeline. You can also modify the dimensions and colors of the rectangles and labels
"""