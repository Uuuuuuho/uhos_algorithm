﻿import sys
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen, QFontMetrics, QFont
from PyQt5.QtWidgets import QApplication, QGraphicsScene, QGraphicsView, QGraphicsTextItem, QGraphicsRectItem

# Define a Task class to store task information
class Task:
    def __init__(self, name, start_time, end_time, period, wcet):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.period = period
        self.wcet = wcet

# Create a list of tasks
tasks = [Task('Task A', 0, 3, 10, 2),
         Task('Task B', 2, 5, 12, 3),
         Task('Task C', 6, 8, 15, 1),
         Task('Task D', 8, 12, 20, 4)]

class TimelineView(QGraphicsView):
    def __init__(self):
        super().__init__()

        # Set up the scene
        self.scene = QGraphicsScene()
        self.setScene(self.scene)
        
        # Set the default pen and brush
        self.pen = QPen(Qt.black)
        self.brush = QBrush(Qt.blue)
        
        # Add the tasks to the scene
        self.add_tasks()
        self.add_timeline()
        self.add_task_names()

        # Set the view size and show it
        self.setFixedSize(800, 600)
        self.show()

    def add_tasks(self):
        # Add the tasks to the scene
        for task in tasks:
            self.add_task(task)

    def add_task(self, task):
        # Calculate the task dimensions
        x = task.start_time * 20 + 100
        y = len(self.scene.items()) * 50 + 50
        width = task.end_time * 20 - task.start_time * 20
        height = 40
        
        # Create the task rectangle and text
        rect = QGraphicsRectItem(x, y, width, height)
        rect.setPen(self.pen)
        rect.setBrush(self.brush)
        text = QGraphicsTextItem(task.name)
        text.setPos(50, y + height/2 - text.boundingRect().height()/2)
        
        # Add the rectangle and text to the scene
        self.scene.addItem(rect)
        self.scene.addItem(text)

    def add_timeline(self):
        # Add the timeline scale to the scene
        for i in range(0, 41, 5):
            x = i * 20 + 100
            y = 10
            height = 30
            rect = QGraphicsRectItem(x, y, 1, height)
            text = QGraphicsTextItem(str(i))
            text.setPos(x - text.boundingRect().width()/2, y + height + 5)
            self.scene.addItem(rect)
            self.scene.addItem(text)

    def add_task_names(self):
        # Add the task names to the scene
        font = QFont()
        font.setBold(True)
        fm = QFontMetrics(font)
        max_width = max([fm.width(task.name) for task in tasks])
        for i, task in enumerate(tasks):
            text = QGraphicsTextItem(task.name)
            text.setFont(font)
            text.setPos(50 - fm.width(task.name), i * 50 + 70 - fm.height()/2)
            self.scene.addItem(text)

if __name__=="__main__":
    # Create the application and view
    app = QApplication(sys.argv)
    view = TimelineView()
    while (True):
        __name__
    # Run