﻿from PyQt5.QtWidgets import QApplication, QGraphicsScene, QGraphicsView, QGraphicsItem, QGraphicsTextItem, QGraphicsEllipseItem, QGraphicsLineItem
from PyQt5.QtGui import QColor, QPen, QBrush, QFont
from PyQt5.QtCore import Qt, QPointF
import sys
from PyQt5.QtWidgets import QApplication, QGraphicsScene, QGraphicsView, QGraphicsItem, QGraphicsTextItem
from PyQt5.QtGui import QPen, QColor
from PyQt5.QtCore import Qt
# create a directed graph
graph = {
    'A': ['B', 'C'],
    'B': ['D'],
    'C': ['E'],
    'D': [],
    'E': []
}

class GraphVisualizer(QGraphicsView):
    def __init__(self):
        super().__init__()
        self.scene = QGraphicsScene(self)
        self.setScene(self.scene)
        self.setRenderHint(QPainter.Antialiasing)
        self.setWindowTitle("Directed Graph Visualizer")

        self.nodes = {}
        self.edges = {}

        self.draw_graph(graph)

    def draw_graph(self, graph):
        node_size = 50
        node_color = QColor(200, 200, 200)
        node_pen = QPen(QColor(50, 50, 50), 1, Qt.SolidLine)
        node_font = QFont("Helvetica", 12)
        node_brush = QBrush(QColor(50, 50, 50))

        edge_color = QColor(50, 50, 50)
        edge_pen = QPen(edge_color, 2, Qt.SolidLine)

        for node in graph:
            # draw node
            item = QGraphicsEllipseItem(-node_size/2, -node_size/2, node_size, node_size)
            item.setPos(QPointF(0, 0))
            item.setPen(node_pen)
            item.setBrush(node_brush)
            label = QGraphicsTextItem(node)
            label.setFont(node_font)
            label.setDefaultTextColor(node_color)
            label.setPos(QPointF(-node_size/2, -node_size/2))
            item.addToGroup(label)
            self.scene.addItem(item)
            self.nodes[node] = item

            # draw edges
            for neighbor in graph[node]:
                line = QGraphicsLineItem()
                line.setPen(edge_pen)
                line.setPos(QPointF(0, 0))
                line.setLine(QLineF(item.pos(), self.nodes[neighbor].pos()))
                self.scene.addItem(line)
                self.edges[(node, neighbor)] = line

    def resizeEvent(self, event):
        self.fitInView(self.sceneRect(), Qt.KeepAspectRatio)

if __name__ == '__main__':
    app = QApplication([])
    window = GraphVisualizer()
    window.show()
    app.exec_()