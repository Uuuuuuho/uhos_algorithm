﻿import sys
from PyQt5.QtWidgets import QApplication, QGraphicsScene, QGraphicsView, QGraphicsItem, QGraphicsLineItem
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtCore import Qt

class Node(QGraphicsItem):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def boundingRect(self):
        return Qt.QRectF(-20, -20, 40, 40)

    def paint(self, painter, option, widget):
        painter.setPen(QPen(Qt.black, 2))
        painter.drawEllipse(-19, -19, 38, 38)
        painter.drawText(-10, 5, self.name)

class Edge(QGraphicsLineItem):
    def __init__(self, sourceNode, destNode):
        super().__init__()
        self.sourceNode = sourceNode
        self.destNode = destNode
        self.setPen(QPen(Qt.black, 2))

    def boundingRect(self):
        return self.shape().boundingRect()

    def shape(self):
        path = super().shape()
        path.addPolygon(self.arrowHead)
        return path

    def paint(self, painter, option, widget):
        if self.sourceNode.collidesWithItem(self.destNode):
            return
        painter.setPen(QPen(Qt.black, 2))
        painter.setBrush(Qt.black)

        sourcePoint = self.mapFromItem(self.sourceNode, 0, 0)
        destPoint = self.mapFromItem(self.destNode, 0, 0)
        line = Qt.QLineF(sourcePoint, destPoint)

        self.setLine(line)

        angle = Qt.QLineF(line).angle()

        if angle > 180:
            angle -= 180
        else:
            angle += 180

        arrowHeadSize = 10

        arrowP1 = destPoint + Qt.QPointF(
            arrowHeadSize * math.sin(math.radians(angle - 60)),
            arrowHeadSize * math.cos(math.radians(angle - 60)),
        )
        arrowP2 = destPoint + Qt.QPointF(
            arrowHeadSize * math.sin(math.radians(angle + 60)),
            arrowHeadSize * math.cos(math.radians(angle + 60)),
        )

        self.arrowHead = Qt.QPolygonF([destPoint, arrowP1, arrowP2])

        painter.drawLine(line)
        painter.drawPolygon(self.arrowHead)

class Graph(QGraphicsView):
    def __init__(self):
        super().__init__()
        self.scene = QGraphicsScene(self)
        self.setScene(self.scene)

    def addNode(self, node):
        self.scene.addItem(node)

    def addEdge(self, edge):
        self.scene.addItem(edge)

    def addNodes(self, nodes):
        for node in nodes:
            self.scene.addItem(node)

    def addEdges(self, edges):
        for edge in edges:
            self.scene.addItem(edge)

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sys

class GraphVisualizer(QWidget):
    def __init__(self, graph, parent=None):
        super(GraphVisualizer, self).__init__(parent)

        # set the title of the window
        self.setWindowTitle("Directed Graph Visualizer")

        # set the geometry of the window
        self.setGeometry(100, 100, 800, 600)

        # set the graph
        self.graph = graph

        # create a graphics view widget
        self.view = QGraphicsView(self)

        # create a graphics scene for the graphics view
        self.scene = QGraphicsScene(self)

        # set the scene for the graphics view
        self.view.setScene(self.scene)

        # create a brush for the nodes
        self.node_brush = QBrush(QColor(0, 200, 200))

        # create a pen for the nodes
        self.node_pen = QPen(QColor(0, 0, 0))

        # create a pen for the edges
        self.edge_pen = QPen(QColor(0, 0, 0))

        # create a font for the node labels
        self.node_font = QFont("Arial", 10)

        # set the horizontal and vertical spacing between the nodes
        self.h_spacing = 80
        self.v_spacing = 50

        # set the scaling factor for the view
        self.scale_factor = 1.0

        # create a layout for the widget
        layout = QVBoxLayout(self)

        # add the graphics view to the layout
        layout.addWidget(self.view)

        # set the layout for the widget
        self.setLayout(layout)

    def paintEvent(self, event):
        # clear the scene
        self.scene.clear()

        # create a dictionary to hold the QGraphicsItem for each node
        node_items = {}

        # loop over the nodes in the graph and create a QGraphicsItem for each one
        for node in self.graph.nodes:
            # create a QGraphicsEllipseItem for the node
            node_item = QGraphicsEllipseItem(-20, -20, 40, 40)

            # set the brush and pen for the node
            node_item.setBrush(self.node_brush)
            node_item.setPen(self.node_pen)

            # set the position of the node based on its layout position
            node_item.setPos(node.layout_pos[0] * self.h_spacing, node.layout_pos[1] * self.v_spacing)

            # create a QGraphicsTextItem for the label of the node
            label_item = QGraphicsTextItem(node.label)

            # set the font for the label
            label_item.setFont(self.node_font)

            # set the position of the label relative to the node
            label_item.setPos(-label_item.boundingRect().width() / 2, -label_item.boundingRect().height() / 2)

            # add the node and label items to the scene
            self.scene.addItem(node_item)
            self.scene.addItem(label_item)

            # add the node item to the dictionary of node items
            node_items[node] = node_item

        # loop over the edges in the graph and create a QGraphicsItem for each one
        for edge in self.graph.edges:
            # get the source and target nodes for the edge
            source_node = edge.source
            target_node = edge.target

            # create a QGraphicsLineItem for the edge
            edge_item = QGraphicsLineItem()

            # set the pen for the edge
            edge_item.setPen(self.edge_pen)


# if __name__ == '__main__':
#     app = QApplication(sys.argv)

#     graph = Graph()
#     graph.setWindowTitle("Directed Graph Visualizer")

#     nodeA = Node('A')
#     nodeB = Node('B')
#     nodeC = Node('C')
#     nodeD = Node('D')

#     edgeAB = Edge(nodeA, nodeB)
#     edgeBC = Edge(nodeB, nodeC)
#     edgeCD = Edge(nodeC, nodeD)
#     edgeDA = Edge(nodeD, nodeA)

#     graph.addNodes([nodeA, nodeB, nodeC, nodeD])
#     graph.addEdges([edgeAB, edgeBC, edgeCD, edgeDA])
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    graph = DirectedGraph()

    # Add nodes and edges
    graph.add_node("A")
    graph.add_node("B")
    graph.add_node("C")
    graph.add_node("D")
    graph.add_edge("A", "B")
    graph.add_edge("B", "C")
    graph.add_edge("C", "D")
    graph.add_edge("D", "A")

    # Set up the view and show the graph
    view = GraphView(graph)
    view.setWindowTitle("Directed Graph Visualizer")
    view.show()

    sys.exit(app.exec_())