﻿from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QPen, QColor, QBrush
from PyQt5.QtWidgets import QApplication, QGraphicsScene, QGraphicsView, QGraphicsTextItem, QGraphicsLineItem, QGraphicsItem

class GraphViewer(QGraphicsView):
    def __init__(self, predecessor_data, successor_data, parent=None):
        super().__init__(parent)
        
        # Set window properties
        self.setWindowTitle('Directed Graph Visualizer')
        self.setGeometry(100, 100, 800, 600)
        
        # Set up the graphics scene
        self.scene = QGraphicsScene(self)
        self.setScene(self.scene)
        
        # Set up the node and edge data
        self.predecessor_data = predecessor_data
        self.successor_data = successor_data
        
        # Draw the nodes
        node_size = 30
        node_spacing = 100
        node_x_offset = 50
        node_y_offset = 50
        
        node_items = {}
        for node in set(predecessor_data.keys()) | set(successor_data.keys()):
            node_item = QGraphicsTextItem(node, self.scene)
            node_item.setPos(node_x_offset + node_spacing * len(node_items), node_y_offset)
            node_item.setFlag(QGraphicsItem.ItemIsMovable)
            node_item.setFlag(QGraphicsItem.ItemIsSelectable)
            node_items[node] = node_item
            
            # Draw the predecessor edges
            if node in predecessor_data:
                for predecessor in predecessor_data[node]:
                    line = QGraphicsLineItem(self.scene)
                    line.setPen(QPen(QColor(0, 0, 0)))
                    line.setLine(
                        node_item.x() + node_size / 2,
                        node_item.y() + node_size / 2,
                        node_items[predecessor].x() + node_size / 2,
                        node_items[predecessor].y() + node_size / 2
                    )
            
            # Draw the successor edges
            if node in successor_data:
                for successor in successor_data[node]:
                    line = QGraphicsLineItem(self.scene)
                    line.setPen(QPen(QColor(0, 0, 0)))
                    line.setLine(
                        node_item.x() + node_size / 2,
                        node_item.y() + node_size / 2,
                        node_items[successor].x() + node_size / 2,
                        node_items[successor].y() + node_size / 2
                    )

if __name__ == '__main__':
    app = QApplication([])
    predecessor_data = {
        'A': [],
        'B': ['A'],
        'C': ['A'],
        'D': ['B', 'C']
    }
    successor_data = {
        'A': ['B', 'C'],
        'B': ['D'],
        'C': ['D'],
        'D': []
    }
    viewer = GraphViewer(predecessor_data, successor_data)
    viewer.show()
    app.exec_()