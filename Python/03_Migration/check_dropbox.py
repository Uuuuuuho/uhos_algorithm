import dropbox
import os

access_token = ''

# Open the file for reading
with open('E:\Dropbox_key.txt', 'r') as file:
    # Read the entire file content into a string
    # Provide your Dropbox API key
    access_token = file.read()

# Initialize the Dropbox client
dbx = dropbox.Dropbox(access_token)

# Specify the source file (local file path)
source_file = 'local_file.txt'

# Specify the destination file path on Dropbox
destination_file = '/destination_folder/remote_file.txt'

# Check if the file exists locally
if os.path.exists(source_file):
    # Upload the file to Dropbox
    with open(source_file, 'rb') as file:
        dbx.files_upload(file.read(), destination_file)

    print("File uploaded to Dropbox.")
else:
    print(source_file, " does exist.")

# Make sure to replace 'YOUR_ACCESS_TOKEN' with your actual Dropbox access token.
