import os
import shutil
from tqdm import tqdm  # Import tqdm for the progress bar

def migrate(src_dir, dst_dir):
    print("\n\n\n")
    print("migrate...")
    print("\n\n\n")
    for root, dirs, files in os.walk(src_dir):
        relative_path = os.path.relpath(root, src_dir)
        destination_path = os.path.join(dst_dir, relative_path)

        # Ensure the destination directory exists
        os.makedirs(destination_path, exist_ok=True)

        # Copy files
        for file in tqdm(files):
            source_file = os.path.join(root, file)
            destination_file = os.path.join(destination_path, file)

            # Check if the file exists in the destination
            if not os.path.exists(destination_file):
                shutil.copy2(source_file, destination_file)

def check_consistency(src, dst):
    print("\n\n\n")
    print("check_consistency...")
    print("\n\n\n")
    for root, dirs, files in os.walk(src_dir):
        relative_path = os.path.relpath(root, src_dir)
        destination_path = os.path.join(dst_dir, relative_path)

        # Ensure the destination directory exists
        os.makedirs(destination_path, exist_ok=True)

        # Copy files
        for file in tqdm(files):
            source_file = os.path.join(root, file)
            destination_file = os.path.join(destination_path, file)

            # Check if the file exists in the destination
            if not os.path.exists(destination_file):
                try:
                    with open(src_dir, 'rb') as file:
                        # Your file processing code
                        continue
                except OSError as e:
                    if e.errno == 22:  # Errno 22 corresponds to "invalid argument" on many systems
                        print(f"Ignoring file '{src_dir}' due to invalid argument error.")
                    else:
                        print(f"An error occurred: {str(e)}")
    print("\n\n\n")
    print("Done..!")
    print("\n\n\n")


if __name__ == "__main__":
    src_dir = "E:\\OneDrive - 고려대학교\\From_dropbox\\Wedding"
    dst_dir = "C:\\Users\\JB\\iCloudDrive\\From_Dropbox\\Wedding"

    migrate(src_dir=src_dir, dst_dir=dst_dir)
    check_consistency(src=src_dir, dst=dst_dir)
