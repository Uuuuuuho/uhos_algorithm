import sqlite3
import pandas as pd
import tkinter as tk
from tkinter import ttk, messagebox
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
from matplotlib.ticker import FuncFormatter

# 한글 폰트 설정
font_path = "C:\\Windows\\Fonts\\malgun.ttf"  # 윈도우의 '맑은 고딕' 폰트 경로
font_name = font_manager.FontProperties(fname=font_path).get_name()
rc('font', family=font_name)

# SQLite 데이터베이스 생성 및 연결
db_path = 'C:\\Users\\Hyunyeong\\Dropbox\\호호네 경제관리\\가계부.db'
conn = sqlite3.connect(db_path)

# 데이터 삽입 함수
def insert_data():
    data = {
        "date": ["2024-07-01", "2024-07-02", "2024-07-03", "2024-07-05", "2024-07-10",
                 "2024-07-15", "2024-07-20", "2024-07-25",
                 "2024-08-01", "2024-08-02", "2024-08-03", "2024-08-05", "2024-08-10",
                 "2024-08-15", "2024-08-20", "2024-08-25",
                 "2024-09-01", "2024-09-02", "2024-09-03", "2024-09-05", "2024-09-10",
                 "2024-09-15", "2024-09-20", "2024-09-25"],
        "description": ["월급", "[유호]식비", "[현영]생필품", "[유호]교육비", "[현영]외식비", "저축", "고정지출", "저축",
                        "월급", "[유호]식비", "[현영]생필품", "[유호]교육비", "[현영]외식비", "저축", "고정지출", "저축",
                        "월급", "[유호]식비", "[현영]생필품", "[유호]교육비", "[현영]외식비", "저축", "고정지출", "저축"],
        "amount": [500000, 20000, 15000, 30000, 25000, 100000, 50000, 100000,
                   500000, 21000, 16000, 31000, 26000, 100000, 50000, 100000,
                   500000, 22000, 17000, 32000, 27000, 100000, 50000, 100000],
        "category": ["수입", "변동지출", "변동지출", "변동지출", "변동지출", "저축", "고정지출", "저축",
                     "수입", "변동지출", "변동지출", "변동지출", "변동지출", "저축", "고정지출", "저축",
                     "수입", "변동지출", "변동지출", "변동지출", "변동지출", "저축", "고정지출", "저축"],
        "subcategory": ["", "식비", "생필품", "교육비", "외식비", "", "", "",
                        "", "식비", "생필품", "교육비", "외식비", "", "", "",
                        "", "식비", "생필품", "교육비", "외식비", "", "", ""],
        "due_date": ["2024-07-01", "2024-07-02", "2024-07-03", "2024-07-05", "2024-07-10",
                     "2024-07-15", "2024-07-20", "2024-07-25",
                     "2024-08-01", "2024-08-02", "2024-08-03", "2024-08-05", "2024-08-10",
                     "2024-08-15", "2024-08-20", "2024-08-25",
                     "2024-09-01", "2024-09-02", "2024-09-03", "2024-09-05", "2024-09-10",
                     "2024-09-15", "2024-09-20", "2024-09-25"]
    }

    df = pd.DataFrame(data)
    df.to_sql('transactions', conn, if_exists='append', index=False)

# 월별 지출 내역 계산 함수
def calculate_monthly_expenses():
    query = '''
    SELECT
        strftime('%Y-%m', date) as month,
        category,
        subcategory,
        SUM(amount) as total
    FROM transactions
    GROUP BY month, category, subcategory
    ORDER BY month
    '''
    df_summary = pd.read_sql(query, conn)
    return df_summary

# 데이터 추가 함수
def add_transaction(date, description, amount, category, subcategory, due_date):
    cursor = conn.cursor()
    cursor.execute('''
    INSERT INTO transactions (date, description, amount, category, subcategory, due_date)
    VALUES (?, ?, ?, ?, ?, ?)
    ''', (date, description, amount, category, subcategory, due_date))
    conn.commit()

class App:
    def __init__(self, root):
        self.root = root
        self.root.title("가계부 관리 프로그램")
        
        self.tree = ttk.Treeview(root, columns=("month", "category", "subcategory", "total"), show="headings")
        self.tree.heading("month", text="월")
        self.tree.heading("category", text="카테고리")
        self.tree.heading("subcategory", text="하위항목")
        self.tree.heading("total", text="총액")

        self.tree.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.insert_button = tk.Button(root, text="데이터 삽입", command=insert_data)
        self.insert_button.pack(side=tk.TOP, fill=tk.X)

        self.update_button = tk.Button(root, text="업데이트", command=self.update_data)
        self.update_button.pack(side=tk.TOP, fill=tk.X)

        self.add_button = tk.Button(root, text="거래 추가", command=self.open_add_window)
        self.add_button.pack(side=tk.TOP, fill=tk.X)

        self.show_main_chart_button = tk.Button(root, text="월별 내역 차트 보기", command=self.open_main_chart_window)
        self.show_main_chart_button.pack(side=tk.TOP, fill=tk.X)

        self.show_sub_chart_button = tk.Button(root, text="변동지출 차트 보기", command=self.open_sub_chart_window)
        self.show_sub_chart_button.pack(side=tk.TOP, fill=tk.X)

        self.toggle_summary_button = tk.Button(root, text="월별 요약 보기/숨기기", command=self.toggle_summary)
        self.toggle_summary_button.pack(side=tk.TOP, fill=tk.X)

        self.canvas1 = None
        self.canvas2 = None
        self.summary_visible = True
        self.update_data()  # 프로그램 실행 시 데이터를 바로 보여줌

    def update_data(self):
        for row in self.tree.get_children():
            self.tree.delete(row)
        
        df_summary = calculate_monthly_expenses()
        if self.summary_visible:
            df_summary = df_summary.groupby(['month', 'category']).agg({'total': 'sum'}).reset_index()
        else:
            df_summary = df_summary.groupby(['month', 'category', 'subcategory']).agg({'total': 'sum'}).reset_index()

        for index, row in df_summary.iterrows():
            self.tree.insert("", "end", values=(row["month"], row["category"], "" if self.summary_visible else row["subcategory"], row["total"]))
        
        self.clear_data()


    def clear_data(self):
        if self.canvas1:
            self.canvas1.get_tk_widget().pack_forget()
        if self.canvas2:
            self.canvas2.get_tk_widget().pack_forget()

    def plot_data(self, df_summary):
        self.clear_data()
        
        fig1, ax1 = plt.subplots()
        fig2, ax2 = plt.subplots()
        
        # 주요 카테고리별 막대 그래프 생성
        main_categories = df_summary[df_summary['subcategory'] == ''].pivot(index='month', columns='category', values='total').fillna(0)
        if not main_categories.empty:
            main_categories.plot(kind='bar', stacked=True, ax=ax1)
            ax1.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: f'{int(x):,} 원'))
            ax1.set_title('월별 주요 카테고리 내역')
            ax1.set_xlabel('월')
            ax1.set_ylabel('총액')
            ax1.legend(title='카테고리')
            ax1.grid(axis='y')

            self.canvas1 = FigureCanvasTkAgg(fig1, master=self.root)
            self.canvas1.draw()
            self.canvas1.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # 변동지출 하위 항목별 막대 그래프 생성
        subcategories = df_summary[df_summary['category'] == '변동지출'].pivot(index='month', columns='subcategory', values='total').fillna(0)
        if not subcategories.empty:
            subcategories.plot(kind='bar', stacked=True, ax=ax2)
            ax2.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: f'{int(x):,} 원'))
            ax2.set_title('월별 변동지출 하위 항목 내역')
            ax2.set_xlabel('월')
            ax2.set_ylabel('총액')
            ax2.legend(title='하위항목')
            ax2.grid(axis='y')

            self.canvas2 = FigureCanvasTkAgg(fig2, master=self.root)
            self.canvas2.draw()
            self.canvas2.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

    def open_main_chart_window(self):
        main_chart_window = tk.Toplevel(self.root)
        main_chart_window.title("월별 내역 차트 보기")

        fig1, ax1 = plt.subplots()
        
        df_summary = calculate_monthly_expenses()
        
        main_categories = df_summary[df_summary['subcategory'] == ''].pivot(index='month', columns='category', values='total').fillna(0)
        if not main_categories.empty:
            main_categories.plot(kind='bar', stacked=True, ax=ax1)
            ax1.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: f'{int(x):,} 원'))
            ax1.set_title('월별 주요 카테고리 내역')
            ax1.set_xlabel('월')
            ax1.set_ylabel('총액')
            ax1.legend(title='카테고리')
            ax1.grid(axis='y')

        canvas1 = FigureCanvasTkAgg(fig1, master=main_chart_window)
        canvas1.draw()
        canvas1.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    def open_sub_chart_window(self):
        sub_chart_window = tk.Toplevel(self.root)
        sub_chart_window.title("변동지출 차트 보기")

        fig2, ax2 = plt.subplots()
        
        df_summary = calculate_monthly_expenses()

        subcategories = df_summary[df_summary['category'] == '변동지출'].pivot(index='month', columns='subcategory', values='total').fillna(0)
        if not subcategories.empty:
            subcategories.plot(kind='bar', stacked=True, ax=ax2)
            ax2.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: f'{int(x):,} 원'))
            ax2.set_title('월별 변동지출 하위 항목 내역')
            ax2.set_xlabel('월')
            ax2.set_ylabel('총액')
            ax2.legend(title='하위항목')
            ax2.grid(axis='y')

        canvas2 = FigureCanvasTkAgg(fig2, master=sub_chart_window)
        canvas2.draw()
        canvas2.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    def toggle_summary(self):
        self.summary_visible = not self.summary_visible
        self.update_data()

    def open_add_window(self):
        add_window = tk.Toplevel(self.root)
        add_window.title("거래 추가")

        tk.Label(add_window, text="날짜 (YYYY-MM-DD)").grid(row=0, column=0)
        tk.Label(add_window, text="내역").grid(row=1, column=0)
        tk.Label(add_window, text="금액").grid(row=2, column=0)
        tk.Label(add_window, text="카테고리").grid(row=3, column=0)
        tk.Label(add_window, text="하위항목").grid(row=4, column=0)
        tk.Label(add_window, text="출금 예정일 (YYYY-MM-DD)").grid(row=5, column=0)

        date_entry = tk.Entry(add_window)
        description_entry = tk.Entry(add_window)
        amount_entry = tk.Entry(add_window)
        category_entry = tk.Entry(add_window)
        subcategory_entry = tk.Entry(add_window)
        due_date_entry = tk.Entry(add_window)

        date_entry.grid(row=0, column=1)
        description_entry.grid(row=1, column=1)
        amount_entry.grid(row=2, column=1)
        category_entry.grid(row=3, column=1)
        subcategory_entry.grid(row=4, column=1)
        due_date_entry.grid(row=5, column=1)

        def submit_transaction():
            date = date_entry.get()
            description = description_entry.get()
            amount = amount_entry.get()
            category = category_entry.get()
            subcategory = subcategory_entry.get()
            due_date = due_date_entry.get()
            try:
                add_transaction(date, description, float(amount), category, subcategory, due_date)
                messagebox.showinfo("성공", "거래가 추가되었습니다.")
                add_window.destroy()
                self.update_data()
            except Exception as e:
                messagebox.showerror("오류", f"거래를 추가하는 동안 오류가 발생했습니다: {e}")

        submit_button = tk.Button(add_window, text="추가", command=submit_transaction)
        submit_button.grid(row=6, column=0, columnspan=2)


# GUI 창 생성 및 실행
root = tk.Tk()
app = App(root)
root.mainloop()

# 데이터베이스 연결 해제
conn.close()
