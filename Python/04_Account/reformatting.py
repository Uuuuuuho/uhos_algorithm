import pandas as pd
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.styles import Alignment

# 예제 데이터 프레임 생성
data = {
    "날짜": ["2024-07-01", "2024-07-02", "2024-07-03", "2024-07-05", "2024-07-10"],
    "내역": ["월급", "[유호]식비", "[현영]생필품", "[유호]교육비", "[현영]외식비"],
    "금액": [500000, 20000, 15000, 30000, 25000],
    "분류": ["수입", "변동지출", "변동지출", "변동지출", "변동지출"],
    "출금 예정일": ["2024-07-01", "2024-07-02", "2024-07-03", "2024-07-05", "2024-07-10"]
}

df = pd.DataFrame(data)

# 날짜 형식 변경
df['날짜'] = pd.to_datetime(df['날짜']).dt.date
df['출금 예정일'] = pd.to_datetime(df['출금 예정일']).dt.date

# 변동지출 하위항목 열 추가
df['하위항목'] = df['내역'].apply(lambda x: x.split(']')[-1] if ']' in x else '')

# 엑셀 파일 생성 및 기본 시트 추가
wb = Workbook()
ws = wb.active
ws.title = "가계부"

# 데이터 프레임을 엑셀 시트에 추가
for r in dataframe_to_rows(df, index=False, header=True):
    ws.append(r)

# 각 열의 너비 조절
column_widths = {
    "A": 12,  # 날짜
    "B": 20,  # 내역
    "C": 15,  # 금액
    "D": 15,  # 분류
    "E": 15,  # 출금 예정일
    "F": 20   # 하위항목
}
for col, width in column_widths.items():
    ws.column_dimensions[col].width = width

# 모든 셀의 내용 가운데 정렬
for row in ws.iter_rows():
    for cell in row:
        cell.alignment = Alignment(horizontal="center", vertical="center")

# 월별 지출 내역 시트 추가
ws_summary = wb.create_sheet(title="월별 지출 내역")

# 수식 추가 (예시)
ws_summary['A1'] = '월'
ws_summary['B1'] = '수입'
ws_summary['C1'] = '변동지출'
ws_summary['D1'] = '고정지출'
ws_summary['E1'] = '저축'
ws_summary['F1'] = '변동지출 총액'
ws_summary['G1'] = '식비'
ws_summary['H1'] = '생필품'
ws_summary['I1'] = '교육비'
ws_summary['J1'] = '외식비'

for row in range(2, 100):  # 예시로 100개월까지 추가
    ws_summary[f'A{row}'] = f'=TEXT(DATE(2024,ROW()-1,1),"yyyy-mm")'
    ws_summary[f'B{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!D:D, "수입")'
    ws_summary[f'C{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!D:D, "변동지출")'
    ws_summary[f'D{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!D:D, "고정지출")'
    ws_summary[f'E{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!D:D, "저축")'
    ws_summary[f'F{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!D:D, "변동지출")'
    ws_summary[f'G{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!F:F, "식비")'
    ws_summary[f'H{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!F:F, "생필품")'
    ws_summary[f'I{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!F:F, "교육비")'
    ws_summary[f'J{row}'] = f'=SUMIFS(가계부!C:C, 가계부!A:A, ">="&DATE(2024,ROW()-1,1), 가계부!A:A, "<"&DATE(2024,ROW(),1), 가계부!F:F, "외식비")'

# 각 열의 너비 조절 (월별 지출 내역 시트)
for col in ws_summary.columns:
    max_length = 0
    column = col[0].column_letter  # Get the column name
    for cell in col:
        try:
            if len(str(cell.value)) > max_length:
                max_length = len(str(cell.value))
        except:
            pass
    adjusted_width = (max_length + 2)
    ws_summary.column_dimensions[column].width = adjusted_width

# 엑셀 파일 저장
output_file_path = 'C:\\Users\\Hyunyeong\\Dropbox\\호호네 경제관리\\정리된_가계부.xlsx'
wb.save(output_file_path)

print(f"Updated Excel file saved to {output_file_path}")
