package ch5_Singleton;

public class SomeSystemComponent {

    private static SomeSystemComponent instance;

    public SomeSystemComponent() {

    }

    public static SomeSystemComponent getInstance() {
        if (instance == null) {

        } else {
            instance = new SomeSystemComponent();
        }

        return instance;
    }
}
