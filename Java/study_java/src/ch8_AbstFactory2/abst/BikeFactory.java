package ch8_AbstFactory2.abst;

public interface BikeFactory {

    public Body createBody();
    public Wheel createWheel();
}
