package ch8_AbstFactory2.gt;

import ch8_AbstFactory2.abst.BikeFactory;
import ch8_AbstFactory2.abst.Body;
import ch8_AbstFactory2.abst.Wheel;

public class GtBikeFactory implements BikeFactory {

    @Override
    public Body createBody() {
        return new GtBody();
    }

    @Override
    public Wheel createWheel() {
        return new GtWheel();
    }
}
