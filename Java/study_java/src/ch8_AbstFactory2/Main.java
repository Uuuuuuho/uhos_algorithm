package ch8_AbstFactory2;

import ch8_AbstFactory2.abst.BikeFactory;
import ch8_AbstFactory2.abst.Body;
import ch8_AbstFactory2.abst.Wheel;
import ch8_AbstFactory2.gt.GtBikeFactory;

public class Main {

    public static void main(String[] args) {
        BikeFactory factory = new GtBikeFactory();

        Body body = factory.createBody();
        Wheel wheel = factory.createWheel();

        System.out.println(body.getClass());
        System.out.println(wheel.getClass());

    }
}
