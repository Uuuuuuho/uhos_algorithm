package ch8_AbstFactory2.Sam;

import ch8_AbstFactory2.abst.BikeFactory;
import ch8_AbstFactory2.abst.Body;
import ch8_AbstFactory2.abst.Wheel;

public class SamBikeFactory implements BikeFactory {

    @Override
    public Body createBody() {
        return new SamBody();
    }

    @Override
    public Wheel createWheel() {
        return new SamWheel();
    }
}
