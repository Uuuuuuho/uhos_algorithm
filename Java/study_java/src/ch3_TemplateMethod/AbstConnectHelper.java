package ch3_TemplateMethod;

public abstract class AbstConnectHelper {

    abstract protected String doSecurity(String info);

    abstract protected String authentication(String id, String password);

    abstract protected int authorization(String userName);

    abstract protected String connection(String info);

    public String requestConnection(String info) {

        String id, password, userName, decodedInfo, userInfo;

        decodedInfo = doSecurity(info);

        id = "abc";
        password = "abc";

        userInfo = authentication(id, password);

        userName = "abc";

        int result = authorization(userName);

        switch (result) {

            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                break;
        }

        return connection(userInfo);
    }
}
