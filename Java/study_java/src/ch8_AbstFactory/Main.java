package ch8_AbstFactory;

import ch8_AbstFactory.abst.Button;
import ch8_AbstFactory.abst.GuiFac;
import ch8_AbstFactory.abst.TextArea;
import ch8_AbstFactory.concrete.FactoryInstance;

public class Main {
    public static void main(String[] args) {

        GuiFac fac = FactoryInstance.getGuiFac();

        Button button = fac.creatButton();
        TextArea area = fac.createTextArea();

        button.click();
        System.out.println(area.getText());
        System.out.println(System.getProperty("os.name"));
    }
}
