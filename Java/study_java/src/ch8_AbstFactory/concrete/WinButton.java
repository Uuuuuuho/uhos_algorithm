package ch8_AbstFactory.concrete;

import ch8_AbstFactory.abst.Button;

public class WinButton implements Button {

    @Override
    public void click() {
        System.out.println("Win Button");
    }
}
