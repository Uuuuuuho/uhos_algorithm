package ch8_AbstFactory.concrete;

import ch8_AbstFactory.abst.TextArea;

public class WinTextArea implements TextArea {

    @Override
    public String getText() {
        return "Win TextArea";
    }

}
