package ch8_AbstFactory.concrete;


import ch8_AbstFactory.abst.GuiFac;

public class FactoryInstance {

    public static GuiFac getGuiFac() {

        switch (1) {
            case 0:
                return new MacGuiFac();
            case 1:
                return new LinuxGuiFac();
            case 2:
                return new WinGuiFac();
        }
        return null;
    }

    private static int getOsCode() {
        if (System.getProperty("os.name").equals("Mac OS X")) {
            return 0;
        }
        return 1;
    }
}
