package ch8_AbstFactory.concrete;

import ch8_AbstFactory.abst.TextArea;

public class LinuxTextArea implements TextArea {

    @Override
    public String getText() {
        return "Linux TextArea";
    }

}
