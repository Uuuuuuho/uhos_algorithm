package ch8_AbstFactory.concrete;

import ch8_AbstFactory.abst.TextArea;

public class MacTextArea implements TextArea {

    @Override
    public String getText() {
        return "Mac TextArea";
    }

}
