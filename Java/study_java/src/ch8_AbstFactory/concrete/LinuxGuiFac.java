package ch8_AbstFactory.concrete;

import ch8_AbstFactory.abst.Button;
import ch8_AbstFactory.abst.GuiFac;
import ch8_AbstFactory.abst.TextArea;

public class LinuxGuiFac implements GuiFac {

    @Override
    public Button creatButton() {
        return new LinuxButton();
    }

    @Override
    public TextArea createTextArea() {
        return new LinuxTextArea();
    }

}

