package ch8_AbstFactory.concrete;

import ch8_AbstFactory.abst.Button;

public class LinuxButton implements Button {

    @Override
    public void click() {
        System.out.println("Linux Button");
    }
}
