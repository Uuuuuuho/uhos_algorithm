package ch8_AbstFactory.abst;

public interface TextArea {

    public String getText();
}
