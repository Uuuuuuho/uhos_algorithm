package ch8_AbstFactory.abst;

public interface Button {

    public void click();
}
