package ch8_AbstFactory.abst;

public interface GuiFac {

    public Button creatButton();
    public TextArea createTextArea();
}
