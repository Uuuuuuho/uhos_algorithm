package ch4_TemplateFactory;

import ch4_TemplateFactory.fw.Item;

public class DefulatItem extends Item {

    private String itemName;

    public DefulatItem(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public void use() {
        System.out.println(itemName + " was used!");
    }
}
