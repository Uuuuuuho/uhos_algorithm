package ch4_TemplateFactory;

import ch4_TemplateFactory.fw.Creator;
import ch4_TemplateFactory.fw.Item;

public class DefaultItemCreator extends Creator {

    @Override
    protected String end(String itemName) {
        System.out.println("Default job done!");
        return itemName;
    }

    @Override
    protected String init(String itemName) {
        System.out.println("Default init done!");
        return itemName;
    }

    @Override
    protected Item createItem(String itemName) {
        System.out.println("Default createItem done!");
        return new DefulatItem(itemName);
    }
}
