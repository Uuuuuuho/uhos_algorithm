package ch4_TemplateFactory;

import ch4_TemplateFactory.fw.Creator;
import ch4_TemplateFactory.fw.Item;

public class Application {

    public static void main(String[] args) {

        Creator creator = new DefaultItemCreator();

        Item item1 = creator.create("wood sword");
        Item item2 = creator.create("wood shield");
        Item item3 = creator.create("wood armor");

        item1.use();
        item2.use();
        item3.use();
    }
}
