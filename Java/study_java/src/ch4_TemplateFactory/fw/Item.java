package ch4_TemplateFactory.fw;

public abstract class Item {
    public abstract void use();
}
