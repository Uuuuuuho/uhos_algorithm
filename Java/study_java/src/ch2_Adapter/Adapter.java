package ch2_Adapter;

public interface Adapter {

    public Double twiceOf(Float num);

    public Double halfOf(Float num);
}
