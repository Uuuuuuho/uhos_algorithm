package ch2_Adapter;

public class Application {

    public static void main(String[] args) {

        Adapter target = new AdapterImpl();

        System.out.println(target.twiceOf(100.0f));
        System.out.println(target.halfOf(100.3f));
    }
}
