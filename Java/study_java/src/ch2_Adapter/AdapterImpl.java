package ch2_Adapter;

public class AdapterImpl implements Adapter {
    @Override
    public Double twiceOf(Float num) {
        return Math.twoTime(num.doubleValue());
    }

    @Override
    public Double halfOf(Float num) {
        return Math.half(num);
    }
}
