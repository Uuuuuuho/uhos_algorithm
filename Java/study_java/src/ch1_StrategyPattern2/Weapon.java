package ch1_StrategyPattern2;

public interface Weapon {
    public int doAttack();
}
