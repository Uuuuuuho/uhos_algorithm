package ch1_StrategyPattern2;

public class Knife implements Weapon {
    public int doAttack() {
        System.out.println("Knife Attack");
        return 0;
    }
}
