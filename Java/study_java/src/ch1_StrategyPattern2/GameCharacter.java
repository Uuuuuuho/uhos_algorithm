package ch1_StrategyPattern2;

public class GameCharacter {
    private Weapon weapon;

    public int attack() {
        return weapon.doAttack();
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
