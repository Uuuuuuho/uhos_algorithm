package ch1_StrategyPattern2;

public class Ax implements Weapon{
    @Override
    public int doAttack() {
        System.out.println("Ax Attack");
        return 0;
    }
}
