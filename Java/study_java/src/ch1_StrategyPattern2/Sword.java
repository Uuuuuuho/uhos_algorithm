package ch1_StrategyPattern2;

public class Sword implements Weapon {
    public int doAttack() {
        System.out.println("Sword attack");
        return 0;
    }
}
