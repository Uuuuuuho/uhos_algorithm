#include <iostream>

#define MAX 101
#define APH 26
int main()
{
  char s_in[MAX] = "";
  int result_front[APH] = {0,};
  int result_count[APH] = {0,};
  std::cin >> s_in;

  for(int i = 0; i < MAX; i++)
  {
    if((int)s_in[i] != 0)
    {
      if(result_count[(int)s_in[i]-97] == 0)
      {
        result_front[(int)s_in[i]-97] = i;
        result_count[(int)s_in[i]-97]++;
      }
      else
      {
        result_count[(int)s_in[i]-97]++;
      }
    }
  }

  for(int i = 0; i < APH; i++)
  {
    if(result_count[i] == 0)  std::cout << "-1 ";
    else                      std::cout << result_front[i] << " ";
  }
}