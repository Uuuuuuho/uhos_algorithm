#include <iostream>

int main()
{
  int A = 0, B = 0, C = 0;
  
  std::cin >> A >> B >> C;
  
  int event = 0;
  if(B==C) event = -1;
  else event = A/(C-B) + 1;

  if(event > 0) std::cout << event << "\n";
  else std::cout << "-1\n";
}