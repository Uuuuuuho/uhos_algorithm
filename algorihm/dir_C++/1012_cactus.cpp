#include <iostream>
#include <vector>
#include <string.h>
#define MAX 50
bool mat[MAX][MAX];
bool visit[MAX][MAX];
int32_t dx[4] = {1,-1,0,0};
int32_t dy[4] = {0,0,1,-1};
int32_t T = 0, M = 0, N = 0, K = 0;

int32_t find_set(int32_t x, int32_t y)
{
  int res = 1;
  visit[x][y] = true;
  int32_t nx = 0, ny = 0;
  for(int32_t i = 0; i < 4; i++)
  {
    nx = x + dx[i];
    ny = y + dy[i];
    if((nx < 0) || (nx >= M) || (ny < 0) || (ny >= N)) continue;
    if((!visit[nx][ny]) && (mat[nx][ny])) res += find_set(nx, ny);
  }
  return res;
}

int main()
{
  int32_t x = 0, y = 0;
  std::vector<int32_t> tmp, ans;
  std::cin >> T;

  for(int32_t i = 0; i < T; i++)
  {
    tmp.clear();
    std::cin >> M >> N >> K;
    
    memset(mat, false, sizeof(bool)*MAX*MAX);
    memset(visit, false, sizeof(bool)*MAX*MAX);

    for(int32_t j = 0; j < K; j++)
    {
      std::cin >> x >> y;
      mat[x][y] = true;
    }

    for(x = 0; x < M; x++)
    {
      for(y = 0; y < N; y++)
      {
        if((!visit[x][y]) && mat[x][y]) tmp.push_back(find_set(x, y));
      }
    }
    ans.push_back(tmp.size());
  }

  for(int32_t i = 0; i < ans.size(); i++) std::cout << ans[i] << "\n";
}