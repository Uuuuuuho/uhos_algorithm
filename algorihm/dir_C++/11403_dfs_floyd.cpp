#include <iostream>
#define MAX 101

void path(uint8_t adj[][MAX], const int size, const int v)
{
  for(int k = 0; k < size; k++)
    for(int i = 0; i < size; i++)
      for(int j = 0; j < size; j++)
        if((adj[i][j]) || (adj[i][k] && adj[k][j])) adj[i][j] = true;
}

int main()
{
  uint8_t adj_mat[MAX][MAX] = {0};
  int N = 0, tmp = 0, tmp2 = 0;
  scanf("%d", &N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
    {
      scanf("%d", &tmp);
      adj_mat[i][j] = tmp;
    }

  for(int i = 0; i < N; i++)
    path(adj_mat, N, i);

  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      printf("%d ", adj_mat[i][j]);
    }
    printf("\n");
  }
  
}