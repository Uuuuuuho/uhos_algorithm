#include <iostream>

int main()
{
  long long N = 0, ans = 1, curr = 1;
  std::cin >> N;
  while (curr<N)
  {
    curr += 6*ans;
    ans++;
  }
  
  std::cout << ans << std::endl;
}