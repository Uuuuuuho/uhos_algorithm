#include <iostream>

int main()
{
  int64_t a = 0, b = 0;
  std::cin >> a >> b;
  std::cout << a+b << std::endl
            << a-b << std::endl
            << a*b << std::endl
            << a/b << std::endl
            << a%b << std::endl;
}