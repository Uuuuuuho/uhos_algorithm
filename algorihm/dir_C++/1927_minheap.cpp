#include <iostream>
#include <algorithm>
#define MAX 100001
uint32_t heap_cnt = 0;
uint64_t min_heap[MAX] = {0};

void push(uint64_t data)
{
  min_heap[++heap_cnt] = data; // insert data to the last index
  uint32_t child = heap_cnt;
  uint32_t parent = child>>1;

  // heapify
  while(child>1 && min_heap[parent]>min_heap[child]){
    std::swap(min_heap[parent], min_heap[child]);
    child = parent;
    parent = child>>1;
  }
}

void pop()
{
  if(heap_cnt==0){
    printf("0\n");
    return;
  } 
  uint64_t result = min_heap[1];
  uint32_t parent = 1;
  uint32_t child = parent<<1;

  std::swap(min_heap[1],min_heap[heap_cnt--]);
  if(child+1<=heap_cnt) child = (min_heap[child]>min_heap[child+1])?child+1:child;

  // heapify
  while(child<=heap_cnt && min_heap[parent]>min_heap[child]){
    std::swap(min_heap[parent], min_heap[child]);
    parent = child;
    child = parent<<1;
    if(child+1<=heap_cnt) child = (min_heap[child]>min_heap[child+1])?child+1:child;
  }
  printf("%d\n", result);
}

int main()
{
  int n_case = 0, cmd = 0;
  scanf("%d", &n_case);

  for(int i = 0; i < n_case; i++){
    scanf("%d", &cmd);
    if(cmd) push(cmd);
    else    pop();
    // for(int i = 1; i < heap_cnt+1; i++) printf("%d ", min_heap[i]);
    // printf("\n");
  }

}