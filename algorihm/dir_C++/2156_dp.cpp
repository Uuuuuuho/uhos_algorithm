#include <iostream>
#include <vector>
std::vector<uint32_t> weight;

int main()
{
  uint32_t n_glass = 0, tmp = 0, max = 0;
  std::cin >> n_glass;
  uint32_t dp[n_glass+1] = {};
  weight.push_back(0);

  for(uint32_t iter = 0; iter < n_glass; iter++)
  {
    std::cin >> tmp;
    weight.push_back(tmp);
  } 
  
  dp[0] = 0;
  dp[1] = weight[1];

  if(n_glass > 1) 
  {
    dp[2] = weight[1] + weight[2];
    for(uint32_t iter = 3; iter < n_glass+1; iter++)
    {
      max = 0;
      max = std::max(weight[iter]+dp[iter-2],weight[iter]+weight[iter-1]+dp[iter-3]);
      max = std::max(max, dp[iter-1]);
      // std::cout << max << std::endl;
      dp[iter] = max;
    }
  }

  // for(uint32_t iter = 0; iter < weight.size(); iter++) std::cout << weight[iter] << std::endl;
  // for(uint32_t iter = 0; iter < weight.size(); iter++) std::cout << dp[iter] << std::endl;
  std::cout << dp[n_glass] << std::endl;
  
}