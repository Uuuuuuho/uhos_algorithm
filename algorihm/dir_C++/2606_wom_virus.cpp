#include <iostream>
#include <vector>

#define MAX 101
bool visit[MAX] = {0, };
std::vector<int32_t> edges[MAX];
uint32_t answer = 0;

void find_wom(int32_t node)
{
  visit[node] = true;
  for(auto iter: edges[node])
  {
    if(!visit[iter])
    {
      answer++;
      find_wom(iter);
    }
  }
}

int main()
{
  int32_t num_ver = 0, num_edge = 0;
  std::cin >> num_ver;
  std::cin >> num_edge;

  int32_t fst = 0, snd = 0;

  for(int i = 0; i < num_edge; i++)
  {
    std::cin >> fst >> snd;
    edges[fst].push_back(snd);
    edges[snd].push_back(fst);
  }

  find_wom(1);

  std::cout << answer << "\n";
}