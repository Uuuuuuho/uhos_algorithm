#include <iostream>
#define MAX 46
int main()
{
  uint64_t dp[MAX] = {0};
  uint64_t n = 0;
  std::cin >> n;
  dp[0] = 0;
  dp[1] = 1;
  for(int i = 2; i < n+1; i++) dp[i] = dp[i-1]+dp[i-2];
  std::cout << dp[n] << std::endl;
}