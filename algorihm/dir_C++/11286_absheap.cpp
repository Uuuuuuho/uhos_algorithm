#include <iostream>
#include <queue>
#include <functional>
#include <vector>

struct cmp{
  bool operator()(int a, int b){
    if (abs(a)==abs(b)) return a > b;
    else                return abs(a)>abs(b);
  }
};
std::priority_queue<int,std::vector<int>,cmp> pque;
void push(int data)
{
  pque.push(data);
}
void pop()
{
  if(pque.empty()){
    printf("0\n");
    return;
  }
  int res = pque.top();
  pque.pop();
  printf("%d\n", res);
}
int main()
{
  int n_case = 0, tmp_cmd = 0;
  scanf("%d", &n_case);
  std::vector<int> cmd;

  for(int i = 0; i < n_case; i++){
    scanf("%d", &tmp_cmd);
    cmd.push_back(tmp_cmd);
  }
  for(int i = 0; i < n_case; i++){
    if(cmd[i]) push(cmd[i]);
    else    pop();
  }
}