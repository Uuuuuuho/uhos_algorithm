#include <iostream>
#include <vector>

int main()
{
  std::vector<int> A, B;
  int N = 0, tmp = 0, ptr = 0;

  std::cin >> N;
  A.resize(N,0);
  B.resize(N,0);
  for(int i = 0; i < N; i++) std::cin >> A[i];
  for(int i = 0; i < N; i++) std::cin >> B[i];

  // bubble sort
  for(int last = N; last > 2; last--){
    for(int i = 0; i < last-1; i++){
      if(A[i] > A[i+1]){
        tmp = A[i];
        A[i] = A[i+1];
        A[i+1] = tmp;
      }
      if(A[i]==B[i]) ptr++;
    }
  }

  for(int i = 0; i < N; i++) std::cout << A[i] << " ";
  std::cout << std::endl;
}