#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <algorithm>
int main()
{
  std::map<std::string, int> workers;
  std::vector<std::string> ans;
  int N = 0;
  std::string name, enter;
  std::cin >> N;
  for(int i = 0; i < N; i++){
    std::cin >> name >> enter;
    if(enter=="enter") workers.insert(std::make_pair(name, 1));
    else               workers.erase(name);
  }
  for(auto person: workers) ans.push_back(person.first);
  std::sort(ans.begin(), ans.end());
  std::reverse(ans.begin(), ans.end());
  for(auto person: ans) std::cout << person << "\n";
}