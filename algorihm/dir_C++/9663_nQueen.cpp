#include <iostream>
#include <string.h>

#define MAX 16

int32_t N, cnt;
int32_t row[MAX];


bool check_overlap(int level)
{
    for(int i = 0; i < level; i++)
        if(row[i] == row[level] || abs(row[level] - row[i]) == level - i) return false;
    return true;
}

void n_Queen_Search(int32_t row_idx)
{
  static int32_t i = 0;
  if(row_idx == N)
  {
    cnt++;
    return;
  }
  for(int32_t col_idx = 0; col_idx < N; col_idx++)
  {
    row[row_idx] = col_idx;
    if(check_overlap(row_idx)) n_Queen_Search(row_idx + 1);
  }
}

int main()
{
  cnt = 0;
  std::cin >> N;
  memset(row, 0, sizeof(int32_t)*MAX);

  n_Queen_Search(0);

  std::cout << cnt << "\n";  
}