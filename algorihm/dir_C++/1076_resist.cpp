#include <iostream>
#include <vector>
#include <string>
#include <map>

int main()
{
  std::map<std::string, int> val = {
     std::make_pair<std::string, int>("black",0)
    ,std::make_pair<std::string, int>("brown",1)  
    ,std::make_pair<std::string, int>("red",2)    
    ,std::make_pair<std::string, int>("orange",3) 
    ,std::make_pair<std::string, int>("yellow",4) 
    ,std::make_pair<std::string, int>("green",5)  
    ,std::make_pair<std::string, int>("blue",6)   
    ,std::make_pair<std::string, int>("violet",7) 
    ,std::make_pair<std::string, int>("grey",8)   
    ,std::make_pair<std::string, int>("white",9)  
  };
  std::map<std::string, long long> mul = {
     std::make_pair<std::string, long long>("black",1)
    ,std::make_pair<std::string, long long>("brown",10)  
    ,std::make_pair<std::string, long long>("red",100)    
    ,std::make_pair<std::string, long long>("orange",1000) 
    ,std::make_pair<std::string, long long>("yellow",10000) 
    ,std::make_pair<std::string, long long>("green",100000)  
    ,std::make_pair<std::string, long long>("blue",1000000)   
    ,std::make_pair<std::string, long long>("violet",10000000) 
    ,std::make_pair<std::string, long long>("grey",100000000)   
    ,std::make_pair<std::string, long long>("white",1000000000)  
  };
  std::vector<std::string> inp(3);
  long long ans = 0;
  for(int i = 0; i < 3; i++) std::cin >> inp[i];
  
  ans = (val.find(inp[0])->second*10 + val.find(inp[1])->second)*mul.find(inp[2])->second;
  std::cout << ans << "\n";
}