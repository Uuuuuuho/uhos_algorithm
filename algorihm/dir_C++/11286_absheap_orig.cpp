#include <iostream>
#include <algorithm>
#define MAX 100001
int heap_cnt = 0;
int min_heap[MAX] = {0,};

void heapify()
{
  int parent = 1;
  int child = parent<<1;
  if(child+1<=heap_cnt){
    if(abs(min_heap[child])==abs(min_heap[child+1])){
      if(min_heap[child]>min_heap[child+1]){
        child = child+1;
      }
    }
    else if(abs(min_heap[child])>abs(min_heap[child+1])){
      child = child+1;
    }
  }

  while(child<=heap_cnt){
    if(abs(min_heap[parent])==abs(min_heap[child])){
      if(min_heap[parent]>min_heap[child]) 
        std::swap(min_heap[parent], min_heap[child]);
    }
    else if(abs(min_heap[parent])>abs(min_heap[child])){
      std::swap(min_heap[parent], min_heap[child]);
    }
    parent = child;
    child = child<<1;
  }
}
void push(int data)
{
  min_heap[++heap_cnt] = data; // insert data to the last index
  heapify();
}
void pop()
{
  if(heap_cnt==0){
    printf("0\n");
    return;
  } 
  
  int result = min_heap[1];
  printf("%d\n", result);
  std::swap(min_heap[1],min_heap[heap_cnt--]);

  heapify();
}

int main()
{
  int n_case = 0;
  scanf("%d", &n_case);
  int cmds[n_case+1] = {0};

  for(int i = 0; i < n_case; i++) scanf("%d", &cmds[i]);
  for(int i = 0; i < n_case; i++){
    if(cmds[i]) push(cmds[i]);
    else        pop();
  }

}