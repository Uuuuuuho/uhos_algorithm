#include <iostream>
#include <list>
#include <map>
#include <algorithm>
#define MAX_NODEs 51
int ans = 0, num_ele = 0;

void del_node(std::map<int,std::list<int>> &tree, int node)
{
  if(!tree[node].empty()){
    std::list<int> del_list;
    for(auto child: tree[node]){
      del_node(tree, child);
      del_list.push_back(child);
    } 
    for(auto child: del_list) tree[node].remove(child);
    num_ele -= del_list.size();
  }
}
void del(std::map<int,std::list<int>> &tree, int root, int node)
{
  if(root==node){
    del_node(tree,node);
    num_ele--;
  }
  if(!tree[root].empty()){
    std::list<int>::iterator iter;
    iter = std::find(tree[root].begin(), tree[root].end(), node);
    if(iter != tree[root].end()){
      num_ele--;
      tree[root].remove(node);
      del_node(tree, node);
    } 
    else
      for(auto child: tree[root])
        del(tree, child, node);
  }
}
void count(std::map<int,std::list<int>> &tree, int node)
{
  if(!tree[node].empty()){
    for(auto child: tree[node]){
      count(tree, child);
    }
  }
  else if(num_ele!=0)
    ans++;
}

int main()
{
  std::map<int,std::list<int>>tree;
  int N = 0;
  int parent = 0, del_node = 0, root = 0;

  std::cin >> N;

  // insert
  for(int node = 0; node < N; node++){
    std::cin >> parent;
    if(parent==-1) root = node;
    else tree[parent].push_back(node); // assuming only 2 children allowed
    num_ele++;
  }
  std::cin >> del_node;
  
  del(tree, root, del_node);
  count(tree, root);
  // std::cout << ans << " " << num_ele<< "\n";
  std::cout << ans << "\n";

  // for(int node = 0; node < N; node++){
  //   std::cout << node << ": ";
  //   for(auto child: tree[node]){
  //     std::cout << child << " ";
  //   }
  //   std::cout << "\n";
  // }

}