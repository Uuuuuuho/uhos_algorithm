#include <iostream>
#define N 10

int main()
{
  int num[10] = {0, }, in_N = 0;
  std::cin >> in_N;
  int digit_cnt = 0;

  for(int i = 0; i < 10; i++)
  {
    if(in_N != 0) digit_cnt++;
    num[i] = in_N % 10;
    in_N /= 10;
  }

  int tmp = 0;

  for(int j = N-1; j > 0; j--)
  {
    for(int i = 0; i < j; i++)
    {
      if(num[i] > num[i+1]) 
      {
        tmp = num[i];
        num[i] = num[i+1];
        num[i+1] = tmp;
      }
    }
  }

  for(int i = 0; i < digit_cnt; i++)
    std::cout << num[N-1-i];
  std::cout << "\n";
}