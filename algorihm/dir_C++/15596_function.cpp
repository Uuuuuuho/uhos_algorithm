#include <iostream>
#include <vector>

long long sum(std::vector<int> &a)
{
  long long ret = 0;
  for(int iter = 0; iter < a.size(); iter++) ret += a[iter];
  return ret;
}
int main()
{
  int n_in = 0;
  std::cin >> n_in;
  std::vector<int> arr(n_in);
  for(int iter = 0; iter < n_in; iter++)
  {
    int tmp = 0;
    std::cin >> tmp;
    arr[iter] = tmp;
  }
  // std::cout << sum(arr) << std::endl;
}