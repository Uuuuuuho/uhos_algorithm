#include <iostream>
#include <vector>
#include <map>
#define MAX_NODEs 51
int ans = 0;
int num_ele = 0;

void del_node(std::map<int,std::pair<int,int>> &tree, int node)
{
  num_ele--;
  if(tree[node].first!=-1){
    del_node(tree,tree[node].first);
    tree[node].first = -1;
  }
  if(tree[node].second!=-1){
    del_node(tree,tree[node].second);
    tree[node].second = -1;
  }  
}
void del(std::map<int,std::pair<int,int>> &tree, int root, int node)
{
  if(root==node) del_node(tree,node);
  if(tree[root].first!=-1){
    if(tree[root].first==node){
      tree[root].first = -1;
      del_node(tree,node);
      return;
    }
    else del(tree,tree[root].first, node);
  }
  if(tree[root].second!=-1){
    if(tree[root].second==node){
      tree[root].second = -1;
      del_node(tree,node);
      return;
    }
    else del(tree,tree[root].second, node);
  }
}
void count(std::map<int,std::pair<int,int>> &tree, int node)
{
  // std::cout << node << " " << tree[node].first << " " << tree[node].second << "\n";
  if(tree[node].first!=-1)  count(tree,tree[node].first);
  if(tree[node].second!=-1) count(tree,tree[node].second);
  if(tree[node].first==-1 && tree[node].second==-1 && num_ele!=0) ans++;
}

int main()
{
  std::map<int,std::pair<int,int>>tree;
  int N = 0;
  int parent = 0, left = 0, right = 0, del_node = 0, root = 0;

  std::cin >> N;
  // initialization
  for(int node = 0; node < MAX_NODEs; node++){
    tree[node].first = -1;
    tree[node].second = -1;
  }
  // insert
  for(int node = 0; node < N; node++){
    std::cin >> parent;
    if(parent==-1){
      tree[node].first = -1;
      tree[node].second = -1;
      num_ele++;
      root = node;
    }
    else{ // assuming only 2 children allowed
      if(tree[parent].first==-1) tree[parent].first = node;
      else                       tree[parent].second = node;
      num_ele++;
    }
  }
  std::cin >> del_node;
  del(tree, root, del_node);
  count(tree, root);
  std::cout << ans << "\n";
  // std::cout << ans << " " << num_ele << "\n";

  // for(int node = 0; node < N; node++){
  //   std::cout << node << " " << tree[node].first << " " << tree[node].second << "\n";
  // }

}