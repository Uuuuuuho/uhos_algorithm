#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>
#include <string.h>
#include <cmath>

bool comp(std::pair<int32_t,int32_t> ref1, std::pair<int32_t,int32_t> ref2)
{
  return (ref1.second > ref2.second) ? true : false;
}

int main()
{
  int32_t N = 0, tmp = 0;
  int32_t max = -4000, min = 4000;
  int32_t tot = 0;

  std::cin >> N;
  std::vector<int32_t> arr(N);

  for(int32_t i = 0; i < N; i++)
  {
    std::cin >> tmp;
    if(tmp > max) max = tmp;
    if(tmp < min) min = tmp;
    arr[i] = tmp;
    tot += tmp;
  }

  std::sort(arr.begin(), arr.end());

  int32_t range = max-min+1;
  std::vector<std::pair<int32_t,int32_t>> cnt(range);
  
  for(int32_t i = 0; i < range; i++) cnt[i] = std::pair<int32_t,int32_t>(i,0); 

  for(int32_t i = 0; i < N; i++) cnt[arr[i]-min].second++;
  
  std::stable_sort(cnt.begin(), cnt.end(), comp);

  int32_t it = 0;
  if(range > 1)
    if(cnt[0].second == cnt[1].second) 
      it = 1;

  /* print out the solution */
  int32_t avg = std::round((tot/(double)N));
  std::cout << avg << "\n";      /* arithematic avg */
  std::cout << arr[N/2] << "\n";              /* median */
  std::cout << cnt[it].first+min << "\n";             /* second most frequent */
  std::cout << (max-min) << "\n";             /* range */
}