#include <iostream>
#include <utility>
#include <algorithm>
#include <vector>

bool compare(const std::pair<int,int> &a, const std::pair<int,int> &b)
{
  return a.first < b.first;
}

int main()
{
  std::vector<std::pair<int,int>> cols;
  int N = 0;
  std::cin >> N;
  cols.resize(N);
  for(int i = 0; i < N; i++)
    std::cin >> cols[i].first >> cols[i].second;
  std::sort(cols.begin(), cols.end(), compare);
  // for(int i = 0; i < N; i++)
  //   std::cout << cols[i].first << " " << cols[i].second << std::endl;
  std::vector<int> cols_b, dp;
  dp.resize(N,0);
  for(int i = 0; i < N; i++) cols_b.push_back(cols[i].second);
  for(int i = 0; i < N; i++){
    for(int j = 0; j < i; j++){
      if(cols_b[i] > cols_b[j] && dp[i] < dp[j]) dp[i] = dp[j];
    }
    dp[i]++;
  }
  std::cout << (N-*std::max_element(dp.begin(),dp.end())) << std::endl;
}