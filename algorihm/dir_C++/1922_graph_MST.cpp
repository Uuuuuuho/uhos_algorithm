#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int getRoot(vector<int>& parent, int x)  // 인수로 넘긴 정점의 부모 정점을 알려줌
{
    if (parent[x] == x) return x;
    return parent[x] = getRoot(parent, parent[x]);
}

void unionParent(vector<int>& parent, int a, int b)  // 두 정점을 병합함. 부모가 같은, 같은 그룹으로.
{
    int par_a = getRoot(parent, a);
    int par_b = getRoot(parent, b);
    if(par_a < par_b) parent[par_b] = par_a;
    else parent[par_a] = par_b;
}

bool find(vector<int>& parent, int a, int b)  // 두 정점이 같은 부모를 가졌는지 확인
{
    int par_a = getRoot(parent, a);
    int par_b = getRoot(parent, b);
    if(par_a == par_b) return true;
    else return false;
}

bool compare(vector<int> a, vector<int> b)
{
    return a[2] < b[2];
}

int solution(int n, vector<vector<int>> costs) {	
  int answer = 0;
    
	sort(costs.begin(), costs.end(), compare);
    
  vector<int> parents(n+1);
  
  for(int i = 1; i <= parents.size(); i++)
      parents[i] = i;
  
  for(int i = 0; i < costs.size(); i++)
  {
    // for(int i = 0; i < parents.size(); i++)
    //   std::cout << parents[i] << " ";
    //   std::cout << std::endl;

    if(!find(parents, costs[i][0], costs[i][1]))
    {
        unionParent(parents, costs[i][0], costs[i][1]);
        answer += costs[i][2];
    }
  }

	return answer;
}

int main()
{
  int N = 0, M = 0, a = 0, b = 0, c = 0;
  std::vector<std::vector<int>> graph;
  std::cin >> N;
  std::cin >> M;
  graph.resize(M);  
  for(int i = 0; i < M; i++){
    graph[i].resize(3,0);
    std::cin >> a >> b >> c;
    graph[i][0] = a;
    graph[i][1] = b;
    graph[i][2] = c;
  }
  // for(int i = 0; i < M; i++) std::cout << graph[i][0] << " " << graph[i][1] << " " << graph[i][2] << std::endl;
  std::cout << solution(N, graph) << std::endl;
}