#include <iostream>
#include <vector>
#include <string>

int main()
{
  std::string a, b;
  std::vector<char> vec_a, vec_b, ans;
  int carry = 0;
  std::cin >> a >> b;
  for(auto c: a) vec_a.push_back(c);
  for(auto c: b) vec_b.push_back(c);
  if(vec_a.size()>vec_b.size()){
    while(vec_b.size()<vec_a.size())vec_b.insert(vec_b.begin(),'0');
    ans.resize(vec_a.size(),'0');
  }
  else{
    while(vec_a.size()<vec_b.size())vec_a.insert(vec_a.begin(),'0');
    ans.resize(vec_b.size(),'0');
  }

  for(int i = vec_a.size()-1; i >= 0 ; i--){
    if      (vec_a[i]=='1' && vec_b[i]=='1' && carry==0){
      ans[i] = '0';
      carry = 1;
    }
    else if      (vec_a[i]=='1' && vec_b[i]=='1' && carry==1){
      ans[i] = '1';
      carry = 1;
    }
    else if      (vec_a[i]=='0' && vec_b[i]=='1' && carry==0){
      ans[i] = '1';
      carry = 0;
    }
    else if      (vec_a[i]=='0' && vec_b[i]=='1' && carry==1){
      ans[i] = '0';
      carry = 1;
    }
    else if      (vec_a[i]=='1' && vec_b[i]=='0' && carry==0){
      ans[i] = '1';
      carry = 0;
    }
    else if      (vec_a[i]=='1' && vec_b[i]=='0' && carry==1){
      ans[i] = '0';
      carry = 1;
    }
    else if      (vec_a[i]=='0' && vec_b[i]=='0' && carry==0){
      ans[i] = '0';
      carry = 0;
    }
    else if      (vec_a[i]=='0' && vec_b[i]=='0' && carry==1){
      ans[i] = '1';
      carry = 0;
    }
  }
  if(carry==1) ans.insert(ans.begin(),'1');
  while(ans[0]=='0' && ans.size()>1) ans.erase(ans.begin());
  for(auto c: ans) std::cout << c;
}