#include <iostream>
#include <vector>

#ifndef MAX
#define MAX 8
#endif

int n_in, m_in;
std::vector<int> vec;
std::vector<int> visit(MAX);


void permute(int cnt)
{
  if(cnt == m_in)
  {
    for(auto it: vec) std::cout << it << " ";
    std::cout << "\n";
  }
  else
  {
    for(int i = 1; i <= n_in; i++)
    {
      vec.push_back(i);
      permute(cnt + 1);
      vec.pop_back();
    }
  }

}

int main()
{
  std::cin >> n_in >> m_in;

  permute(0);
}