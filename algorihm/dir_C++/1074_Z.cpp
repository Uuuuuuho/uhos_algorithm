#include <iostream>
#include <vector>
#include <cmath>

int sol(int N, int r, int c)
{
  if(N==1) return (2*r+c);
  if     ((r<pow(2,N-1)&&(c<pow(2,N-1))))  return sol(N-1,r,c);
  else if((r<pow(2,N-1)&&(c>=pow(2,N-1)))) return sol(N-1,r,c-pow(2,N-1))+pow(2,N-1)*pow(2,N-1);
  else if((r>=pow(2,N-1)&&(c<pow(2,N-1)))) return sol(N-1,r-pow(2,N-1),c)+pow(2,N-1)*pow(2,N-1)*2;
  else                                     return sol(N-1,r-pow(2,N-1),c-pow(2,N-1))+pow(2,N-1)*pow(2,N-1)*3;  
}
int main()
{
  std::ios::sync_with_stdio(false);
  std::cin.tie(NULL);
  std::cout.tie(NULL);

  int N = 0, r = 0, c = 0;
  std::cin >> N >> r >> c;
  std::cout << sol(N,r,c) << "\n";
}