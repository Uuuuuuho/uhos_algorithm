#include <iostream>
#include <string>
#include <stack>

int main()
{
  int N = 0, M = 0;
  std::string ans = "";
  char cmd = {}, in_char = {};
  std::stack<char> l_stack, r_stack;

  std::cin >> ans;
  for(int i = 0; i < ans.size(); i++) l_stack.push(ans[i]);
  std::cin >> M;

  for(int i = 0; i < M; i++){
    std::cin >> cmd;
    if(cmd=='P') {
      std::cin >> in_char;
      l_stack.push(in_char);
    }
    else if(cmd=='L') {
      if(l_stack.empty()) continue;
      r_stack.push(l_stack.top());
      l_stack.pop();
    }
    else if(cmd=='D') {
      if(r_stack.empty()) continue;
      l_stack.push(r_stack.top());
      r_stack.pop();
    }
    else if(cmd=='B') {
      if(l_stack.empty()) continue;
      l_stack.pop();
    }
  }
  while(!l_stack.empty()) {
    r_stack.push(l_stack.top());
    l_stack.pop();
  }
  while (!r_stack.empty()) {
    std::cout << r_stack.top();
    r_stack.pop();
  }

#if 0  
  int N = 0, M = 0, cursor = 0;
  std::string ans = "";
  char cmd = {}, in_char = {};

  std::cin >> ans;
  cursor = ans.size();
  std::cin >> M;

  for(int i = 0; i < M; i++){
    std::cin >> cmd;
    if(cmd=='P') {
      std::cin >> in_char;
      if(cursor==0){
        ans.insert(0,1,in_char);
      }
      else{
        ans.insert(cursor,1,in_char);
        cursor++;
      }
    }
    else if(cmd=='L') (cursor==0)?:cursor--;
    else if(cmd=='D') (cursor==(ans.size()))?:cursor++;
    else if(cmd=='B') {
      if(cursor==0);
      else
      {
        ans.erase(cursor-1,1);
        cursor--;
      }
    }
  }
  std::cout << ans << std::endl;
#endif  
}