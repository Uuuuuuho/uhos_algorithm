#include <iostream>

int main()
{
  uint32_t n_in = 0;
  std::cin >> n_in;
  uint32_t dp[1001] = {0};
  dp[0] = 0;
  dp[1] = 1;
  dp[2] = 3;
  for(uint32_t iter = 3; iter < n_in+1; iter++) dp[iter] = (dp[iter-1]+(dp[iter-2]<<1))%10007;
  std::cout << dp[n_in] << std::endl;
}