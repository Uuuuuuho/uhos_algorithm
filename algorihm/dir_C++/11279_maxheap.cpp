#include <iostream>
#include <algorithm>
#define MAX 100001
uint32_t heap_cnt = 0;
uint64_t max_heap[MAX] = {0};

void push(uint64_t data)
{
  max_heap[++heap_cnt] = data; // insert data to the last index
  uint32_t child = heap_cnt;
  uint32_t parent = child>>1;

  // heapify
  while(child>1 && max_heap[parent]<max_heap[child]){
    std::swap(max_heap[parent], max_heap[child]);
    child = parent;
    parent = child>>1;
  }
}

void pop()
{
  if(heap_cnt==0){
    printf("0\n");
    return;
  } 
  uint64_t result = max_heap[1];
  uint32_t parent = 1;
  uint32_t child = parent<<1;

  std::swap(max_heap[1],max_heap[heap_cnt--]);
  if(child+1<=heap_cnt) child = (max_heap[child]<max_heap[child+1])?child+1:child;

  // heapify
  while(child<=heap_cnt && max_heap[parent]<max_heap[child]){
    std::swap(max_heap[parent], max_heap[child]);
    parent = child;
    child = parent<<1;
    if(child+1<=heap_cnt) child = (max_heap[child]<max_heap[child+1])?child+1:child;
  }
  printf("%d\n", result);
}

int main()
{
  int n_case = 0, cmd = 0;
  scanf("%d", &n_case);

  for(int i = 0; i < n_case; i++){
    scanf("%d", &cmd);
    if(cmd) push(cmd);
    else    pop();
  }

}