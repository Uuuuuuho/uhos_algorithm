#include <iostream>

int main()
{
  uint64_t dp[91] = {0};
  uint32_t n_in = 0;
  std::cin >> n_in;

  dp[0] = 0;
  dp[1] = 1;
  dp[2] = 1;
  for(int iter = 3; iter < n_in+1; iter++) dp[iter] = dp[iter-1] + dp[iter-2];
  std::cout << dp[n_in] << std::endl;
}
