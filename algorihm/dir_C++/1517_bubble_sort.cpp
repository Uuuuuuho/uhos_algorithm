#include <iostream>
#include <vector>
#define MAX 500001

template <typename T>
void merge(std::vector<T>& vec, int start, int mid, int end);
template <typename T>
void merge_sort(std::vector<T>& vec, int start, int end);
long long ans = 0;

int main()
{
  long long N = 0;
  std::vector<long long> arr;  
  std::cin >> N;

  arr.resize(N,0);
  for(int i = 0; i < N; i++) std::cin >> arr[i];
  merge_sort(arr, 0, arr.size()-1);
  std::cout << ans << "\n";
}

template <typename T>
void merge(std::vector<T>& vec, int start, int mid, int end)
{
  static std::vector<T> sorted(MAX);
  int left = start;
  int right = mid + 1;
  int sort_idx = start;

  while(left <= mid && right <=end)
  {
    if(vec[left] <= vec[right])
    {
      sorted[sort_idx] = vec[left];
      left++;
    }
    else
    {
      sorted[sort_idx] = vec[right];
      ans += (mid - left + 1);
      right++;
    }
    sort_idx++;
  }

  if(left > mid)
  {
    for(int t = right; t <= end; t++)
    {
      sorted[sort_idx] = vec[t];
      sort_idx++;
    }
  }
  else
  {
    for(int t = left; t <= mid; t++)
    {
      sorted[sort_idx] = vec[t];
      sort_idx++;
    }
  }

  for(int t = start; t <= end; t++)
    vec[t] = sorted[t];

}

template <typename T>
void merge_sort(std::vector<T>& vec, int start, int end)
{
  if(start < end)
  {
    int mid = (start + end) / 2;
    merge_sort(vec, start, mid);
    merge_sort(vec, mid+1, end);
    merge(vec, start, mid, end);
  }
}

//===== over time =====
// int main()
// {
//   long long N = 0, ans = 0;
//   std::vector<long long> arr;

//   std::cin >> N;
//   arr.resize(N,0);
//   for(int i = 0; i < N; i++){
//     std::cin >> arr[i];
//   }

//   for(int i = 0; i < N; i++){
//     for(int j = i+1; j < N; j++){
//       if(arr[i]>arr[j]) ans++;
//     }
//   }

//   std::cout << ans << "\n";
// }