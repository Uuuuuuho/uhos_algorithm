#include <iostream>

int main()
{
  uint64_t n_in = 0;
  std::cin >> n_in;
  uint64_t dp[91][2] = {0}; // 0: zero, 1: ans
  dp[0][0] = 0;
  dp[0][1] = 0;
  dp[1][0] = 0;
  dp[1][1] = 1;

  for(uint32_t iter = 2; iter < n_in+1; iter++) 
  {
    dp[iter][1] = dp[iter-1][1]+dp[iter-1][0];
    dp[iter][0] = dp[iter-1][1];
  }
  std::cout << dp[n_in][1] << std::endl;
}