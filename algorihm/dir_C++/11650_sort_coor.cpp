#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

bool cmp_fst(const std::pair<int,int> &a, const std::pair<int,int> &b)
{
  return (a.first < b.first);
}

void swap_pair(std::pair<int,int> &a, std::pair<int,int> &b)
{
  std::pair<int,int> tmp = a;
  a = b;
  b = tmp;
}

int part_snd(std::vector<std::pair<int,int>> &arr, int left, int right)
{
  int x = arr[right].second;
  int i = left-1;
  for(int j = left; j < right; j++)
  {
    if(arr[j].second <= x)
    {
      i++;
      swap_pair(arr[i], arr[j]);
    }
  }

  swap_pair(arr[right],arr[i+1]);
  return i+1;  
}

void quick_snd(std::vector<std::pair<int,int>> &arr, int left, int right)
{
  if(left<right)
  {
    int pivot = part_snd(arr, left, right);
    quick_snd(arr,left,pivot-1);
    quick_snd(arr,pivot+1,right);
  }
}

void cmp_snd(std::vector<std::pair<int,int>> &arr)
{
  int left = 0, right = 0;
  int prev_fst = arr[0].first;
  int cnt_sort = 0;
  for(int i = 1; i < arr.size(); i++)
  {
    if(prev_fst == arr[i].first) 
    {
      cnt_sort++;
      while(prev_fst == arr[i++].first) cnt_sort++;
    }

    if(cnt_sort != 0) 
    {
      // std::cout << "quick_snd is called!\n";
      // std::cout << i-1-cnt_sort << " " << i-1 << "\n";
      quick_snd(arr,i-1-cnt_sort,i-1);
      cnt_sort = 0;
    }
    prev_fst = arr[i].first;
    // std::cout << "prev_fst: " << prev_fst << "\n";
  }
}

int main()
{
  int N = 0;
  std::cin >> N;
  std::vector<std::pair<int,int>> coor(N);
  for(int i = 0; i < N; i++) std::cin >> coor[i].first >> coor[i].second;
  // std::sort(coor.begin(),coor.end(),cmp_fst);
  // cmp_snd(coor);
  std::sort(coor.begin(), coor.end());
  for(int i = 0; i < N; i++) std::cout << coor[i].first << " " << coor[i].second << "\n";
}


/* ==== irrelavant code below ===== */
void swap(int *a, int *b)
{
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

int partition(int *arr, int left, int right)
{
  int x = arr[right];
  int i = left-1;
  for(int j = left; j < right; j++)
  {
    if(arr[j] <= x)
    {
      i++;
      swap(&arr[i], &arr[j]);
    }
  }

  swap(&arr[right],&arr[i+1]);
  return i+1;
}

void quick_sort(int *arr, int left, int right)
{
  if(left<right)
  {
    int pivot = partition(arr, left, right);
    quick_sort(arr,left,pivot-1);
    quick_sort(arr,pivot+1,right);
  }
}
