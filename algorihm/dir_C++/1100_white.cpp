#include <iostream>
#include <vector>
#include <string>
int main()
{
  int board[8][8] = {
    {0,1,0,1,0,1,0,1}
    ,{1,0,1,0,1,0,1,0}
    ,{0,1,0,1,0,1,0,1}
    ,{1,0,1,0,1,0,1,0}
    ,{0,1,0,1,0,1,0,1}
    ,{1,0,1,0,1,0,1,0}
    ,{0,1,0,1,0,1,0,1}
    ,{1,0,1,0,1,0,1,0}
  };
  char horse[8][8] = {'.',};
  std::string tmp;
  int ans = 0;

  for(int i = 0; i < 8; i++){
    std::cin >> tmp;
    for(int j = 0; j < 8; j++){
      horse[i][j] = tmp[j];
    }
  }

  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      if(!board[i][j] && horse[i][j]=='F') ans++;
    }
  }

  std::cout << ans;
}