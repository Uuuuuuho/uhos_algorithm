#include <iostream>
#include <vector>
#include <string.h>
#define MAX 20001
#define RED 1
#define BLUE -1
int color=RED;
bool res = true;

void dfs(std::vector<int> adj_list[], int visit[], const int v)
{
  visit[v] = color;
  for(std::vector<int>::iterator e = adj_list[v].begin(); e != adj_list[v].end(); e++){
    std::cout << v << " " << e[0] << " " << visit[v] << " " << visit[e[0]] << std::endl;
    if(visit[e[0]]==0){
      color = -(visit[v]);
      dfs(adj_list, visit, e[0]);
    }
  }
}

bool bipartitie(std::vector<int> adj_list[], int visit[], const int v)
{
  for(std::vector<int>::iterator e = adj_list[v].begin(); e != adj_list[v].end(); e++){
    if(visit[v]==visit[e[0]]) return false;
  }
  return true;
}

int main()
{
  int num_test = 0, num_v = 0, num_e = 0, u = 0, v = 0;
  scanf("%d", &num_test);
  for(int i = 0; i < num_test; i++){
    bool flag = true;
    std::vector<int> adj_list[MAX];
    int visit[MAX] = {0};
    res = true;
    memset(adj_list, 0, sizeof(adj_list));

    scanf("%d %d", &num_v, &num_e);
    for(int j = 0; j < num_e; j++){
      scanf("%d %d", &u, &v);
      adj_list[u].push_back(v);
      adj_list[v].push_back(u);
    }

    // for(int j = 1; j <= num_v; j++){
    //   for(std::vector<int>::iterator iter = adj_list[j].begin(); iter != adj_list[j].end(); iter++){
    //     std::cout << iter[0] << " ";
    //   }
    //   std::cout << std::endl;
    // }

    for(v = 0; v < num_v; v++) dfs(adj_list, visit, v);
    for(v = 0; v < num_v; v++){
      flag = bipartitie(adj_list,visit,v);
      if(flag==false){
        break;
      } 
    }

    if(flag==true) std::cout << "YES" << std::endl;
    else           std::cout << "NO"  << std::endl;

    for(int j = 1; j <= num_v; j++) printf("%d ", visit[j]);
    printf("\n");
  }

}