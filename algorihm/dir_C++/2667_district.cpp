#include <iostream>
#include <vector>
#include <string.h>
#include <algorithm>

#define MAX 26

int32_t dx[4] = {1,-1,0,0};
int32_t dy[4] = {0,0,1,-1};
bool mat[MAX][MAX];
bool visit[MAX][MAX];
int32_t N;

int32_t find_district(int32_t x, int32_t y)
{
  int32_t cnt_element = 1;
  visit[x][y] = true;
  for(int i = 0; i < 4; i++)
  {
    int nx = x + dx[i];
    int ny = y + dy[i];
    if((nx < 0) || (nx >= N) || (ny < 0) || (ny >= N)) continue;
    if((mat[nx][ny]) && (!visit[nx][ny])) cnt_element += find_district(nx, ny);
  }
  return cnt_element;
}

int main()
{
  std::cin >> N;
  std::string tmp;
  std::vector<int32_t> answer;
  for(int32_t i = 0; i < N; i++)
  {
    std::cin >> tmp;
    for(int32_t j = 0; j < N; j++)
    {
      mat[i][j] = tmp[j] - '0';     /* what does '0' mean??? */
    }
  }

  for(int32_t i = 0; i < N; i++)
  {
    for(int32_t j = 0; j < N; j++)
    {
      if((mat[i][j]) && (!visit[i][j])) 
      {
        answer.push_back(find_district(i,j));
      }
    }
  }

  std::sort(answer.begin(), answer.end());
  std::cout << answer.size() << "\n";
  for(int32_t i = 0; i < answer.size(); i++) std::cout << answer[i] << "\n";
}