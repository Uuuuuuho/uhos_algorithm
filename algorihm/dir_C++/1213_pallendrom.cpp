#include <iostream>
#include <vector>

int main()
{
  std::ios::sync_with_stdio(false);
  std::cin.tie(NULL);
  std::cout.tie(NULL);
  std::vector<int> alpha_arr(26);
  std::fill(alpha_arr.begin(), alpha_arr.end(), 0);
  std::vector<char> inp;
  std::vector<char> ans;
  std::fill(ans.begin(), ans.end(), 0);
  std::string tmp;
  int num_odd = 0;
  char odd_alpha = 0;

  std::cin >> tmp;
  for(char c: tmp) inp.push_back(c);
  ans.resize(inp.size()+1);

  // count the alphabets
  for(char alpha = 65; alpha < 91; alpha++){
    for(int j = 0; j < inp.size(); j++){
      if(alpha==inp[j]) {
        alpha_arr[alpha-65]++;
      }
    }
  }

  // odd case
  for(int i = 0; i < 26; i++){
    if(alpha_arr[i]%2) {
      num_odd++;
      odd_alpha = i + 65;
    }
    if(num_odd>1) {
      std::cout << "I'm Sorry Hansoo" << "\n";
      return 0;
    }
  }

  // assign alphabets
  int start = 0, end = ans.size()-1;
  for(int alpha = 0; alpha < 26; alpha++){
    for(int num_ele = alpha_arr[alpha]; num_ele > 1; num_ele-=2){
      ans[start++] = alpha + 65;
      ans[end--] = alpha + 65;
    }
  }
  
  if(num_odd) ans[ans.size()>>1] = odd_alpha;

  for(char c:ans) std::cout << c;
  std::cout << "\n";
}