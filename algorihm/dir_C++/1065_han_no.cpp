#include <iostream>

int isHan(int n)
{
  int first = n / 100;
  int second = (n % 100)/10;
  int third = n % 10;
  int diff = second - first;

  std::cout << first << " " << second << " " << third
            << " " << diff << std::endl;

  if((third - second) == diff) std::cout << "same!" << std::endl;
}

int main()
{
  int N = 0;
  int first  = 0;
  int second = 0;
  int third  = 0;
  int i = 0;
  int cnt = 0;

  std::cin >> N;

  if(N < 100) std::cout << N << std::endl;
  else
  {
    for(i = 100; i <= N; i++)
    {
      first = i / 100;
      second = (i % 100)/10;
      third = i % 10;
      
      // std::cout << first << " " << second << " " << third << std::endl;

      if((third - second) == (second - first)) cnt++;
    }

    // std::cout << cnt << std::endl;
    std::cout << cnt + 99 << std::endl;
  }
}