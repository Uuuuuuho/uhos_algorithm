#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

bool cmp(std::pair<int,int> a, std::pair<int,int> b)
{
  if(a.second == b.second) return (a.first < b.first);
  else return (a.second < b.second);
}

int main()
{
  int N = 0;
  std::cin >> N;
  std::vector<std::pair<int,int>> coor(N);
  for(int i = 0; i < N; i++) std::cin >> coor[i].first >> coor[i].second;
  std::sort(coor.begin(),coor.end(),cmp);
  for(int i = 0; i < N; i++) std::cout << coor[i].first << " " << coor[i].second << "\n";
}