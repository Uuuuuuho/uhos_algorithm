#include <iostream>
#include <cstring>

int main()
{
  std::string seq_1, seq_2;
  std::cin >> seq_1 >> seq_2;
  int dp[seq_1.length()+1][seq_2.length()+1];
  std::memset(dp,0,sizeof(dp));

  for(int fst_iter = 1; fst_iter < seq_1.length()+1; fst_iter++)
    for(int snd_iter = 1; snd_iter < seq_2.length()+1; snd_iter++)
    {
      if(seq_1[fst_iter-1]==seq_2[snd_iter-1]) dp[fst_iter][snd_iter] = dp[fst_iter-1][snd_iter-1]+1;
      else dp[fst_iter][snd_iter] = std::max(dp[fst_iter-1][snd_iter],dp[fst_iter][snd_iter-1]);
    }

  // for(int fst_iter = 0; fst_iter < seq_1.length()+1; fst_iter++)
  // {
  //   for(int snd_iter = 0; snd_iter < seq_2.length()+1; snd_iter++)
  //     std::cout << dp[fst_iter][snd_iter] << " ";
  //   std::cout << std::endl;
  // }

  std::cout << dp[seq_1.length()][seq_2.length()] << std::endl;
}