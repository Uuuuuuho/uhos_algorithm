#include <iostream>
#include <vector>
#include <string.h>
#include <utility>

#define MAX 100001

/* figure this out! */

std::vector<std::pair<int32_t,int32_t>> tree[MAX];
bool visit[MAX];
uint32_t radius = 0;
uint32_t prev_radius = 0;
uint32_t curr_radius = 0;

inline int max(const int x, const int y) { return (x > y) ? x : y; }

void find_radius(int node)
{
  visit[node] = true;
  for(auto iter: tree[node])
  {
    if(!visit[iter.first])
    {
      if(iter != tree[node].back())
      {
        radius += max(radius + iter.second, radius + tree->back().second);
      }
      find_radius(iter.first);
    }
  }
}

int main()
{
  int V = 0;
  std::cin >> V;

  memset(visit, false, sizeof(bool)*MAX);
  memset(tree, 0, sizeof(int32_t) * 2 * MAX);

  /* node & edge_weight */
  std::pair<int32_t,int32_t> tmp;
  int in_node = 0;
  for(int iter_i = 1; iter_i <= V; iter_i++)
  {
    std::cin >> in_node;
    do
    {
      std::cin >> tmp.first;
      if(tmp.first == -1) break;
      std::cin >> tmp.second;

      tree[in_node].push_back(tmp);             /* save the weight */
    } while (1);
  }


  find_radius(1);

  std::cout << radius << "\n";
}