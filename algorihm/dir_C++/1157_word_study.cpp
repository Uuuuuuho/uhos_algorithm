#include <iostream>
#include <cstdio>
#define MAX 1000000
#define APH 26
int main()
{
  char s_in[MAX] = "";
  int result_front[APH] = {0,};
  int result_count[APH] = {0,};
  std::cin >> s_in;
  int tmp_c = 0;
  int tmp_idx = 0;

  for(int i = 0; i < MAX; i++)
  {
    tmp_c = (int)s_in[i];
    if(tmp_c != 0)
    {
      tmp_idx = tmp_c - 65;
      if((tmp_idx - 32) >= 0)  result_count[tmp_idx-32]++;
      else                    result_count[tmp_idx]++;
    }
  }

  int max = -999, max_apl = -999;
  unsigned int f_same = 0;

  for(int i = 0; i < APH; i++)
  {
    // std::cout << result_count[i] << " ";
    if(max == result_count[i]) f_same = 1;
    if(max < result_count[i]) 
    {
      max = result_count[i];
      f_same = 0;
      max_apl = i;
    }
  }

  if(f_same == 0) std::cout << (char)(max_apl+65);
  else            std::cout << "?\n";
}