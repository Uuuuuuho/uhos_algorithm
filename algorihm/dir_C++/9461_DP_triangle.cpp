#include <stdio.h>

int main()
{
  long long tri[101] = {0, };

  unsigned int i = 0;
  for(i = 0; i <= 100; i++)
  {
    if(i < 3) tri[i] = 1;
    else if(i < 5) tri[i] = 2;
    else tri[i] = tri[i-1] + tri[i-5];
  }

  // printf("%d\n", tri[101]);
  unsigned int N = 0;
  scanf("%d", &N);
  long long inp_N[101] = {0, };

  for(i = 0; i < N; i++)
  {
    scanf("%d", &inp_N[i]);
    printf("%lld\n", tri[inp_N[i]-1]);
  } 

}
