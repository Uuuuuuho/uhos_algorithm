#include <stdio.h>
#include <stdlib.h>
#define M 100
#define N 100
#define Q 5
#define P 5

int main()
{
  int m = M, n = N, p = P, q = Q, sum = 0;
  int out_x = 0, out_y = 0, out_ch = 0;
  int output_x = m-p+1, output_y = n-q+1;

  int **input;
  int **kernel;
  int **output;

  input = (int**)malloc(sizeof(int*)*m);
  kernel = (int**)malloc(sizeof(int*)*p);
  output = (int**)malloc(sizeof(int*)*output_x);
  
  int i = 0, j = 0, k = 0;
  
  // input, kernel & output allocation
  for(i = 0; i < m; i++)
    input[i] = (int*)malloc(sizeof(int)*n);

  for(i = 0; i < p; i++)
    kernel[i] = (int*)malloc(sizeof(int)*q);

  for(i = 0; i < m; i++)
    output[i] = (int*)malloc(sizeof(int)*output_y);

  // input & kernel initialization
  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      input[i][j] = 3;

  for(i = 0; i < p; i++)
    for(j = 0; j < q; j++)
    {
      kernel[i][j] = 1;
      output[i][j] = 0;

    }


  for(out_y = 0; out_y < output_y; out_y++){
    for(out_x = 0; out_x < output_x; out_x++){
      for (i = 0; i < m; i++) {
        for (j = 0; j < q; j++) {
          for (k = 0; k < p; k++) {
            sum += input[i][k]*kernel[k][j];
          }
        }
      }
      output[out_x][out_y] = sum;
      sum = 0;
    }
  }
 
  printf("Product of the matrices:\n");

  for (out_x = 0; out_x < output_x; out_x++) {
    for (out_y = 0; out_y < output_y; out_y++)
      printf("%d\t", output[out_x][out_y]);

    printf("\n");
  }
  
 
  return 0;
}