#include <iostream>
#include <vector>

int main()
{
  std::vector<std::vector<long long>> dp(1002, std::vector<long long>(10, 0));
  std::vector<int> N;
  int T = 0;
  int max_N = 0;
  
  std::cin >> T;
  N.resize(T);

  for(int i = 0; i < 10; i++) dp[1][i] = 1;
  for(int i = 0; i < T; i++) std::cin >> N[i];
  for(int i = 0; i < T; i++) if(N[i]>max_N) max_N = N[i];
  
  for(int i = 2; i <= max_N; i++){
    dp[i][0] = dp[i-1][7];
    dp[i][1] = dp[i-1][2] + dp[i-1][4];
    dp[i][3] = dp[i-1][2] + dp[i-1][6];
    dp[i][9] = dp[i-1][6] + dp[i-1][8];
    dp[i][2] = dp[i-1][1] + dp[i-1][3] + dp[i-1][5];
    dp[i][4] = dp[i-1][1] + dp[i-1][5] + dp[i-1][7];
    dp[i][6] = dp[i-1][3] + dp[i-1][5] + dp[i-1][9];
    dp[i][7] = dp[i-1][0] + dp[i-1][4] + dp[i-1][8];
    dp[i][8] = dp[i-1][5] + dp[i-1][7] + dp[i-1][9];
    dp[i][5] = dp[i-1][2] + dp[i-1][4] + dp[i-1][6] + dp[i-1][8];

    dp[i][0] = dp[i][0]%1234567;
    dp[i][1] = dp[i][1]%1234567;
    dp[i][3] = dp[i][3]%1234567;
    dp[i][9] = dp[i][9]%1234567;
    dp[i][2] = dp[i][2]%1234567;
    dp[i][4] = dp[i][4]%1234567;
    dp[i][6] = dp[i][6]%1234567;
    dp[i][7] = dp[i][7]%1234567;
    dp[i][8] = dp[i][8]%1234567;
    dp[i][5] = dp[i][5]%1234567;
  }

  for(int i = 0; i < T; i++){
    unsigned long long sum = 0;
    for(int j = 0; j < 10; j++) sum = (sum + dp[N[i]][j])%1234567;
    std::cout << sum%1234567 << "\n";
  }

}