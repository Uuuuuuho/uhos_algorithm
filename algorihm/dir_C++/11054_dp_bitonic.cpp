#include <iostream>
#include <algorithm>
#include <iterator>
#include <string.h>

int main()
{
  int N = 0; 
  std::cin >> N;
  int dp[N]={1,}, dp2[N]={1,}, arr[N]={0,}, sub_len[N]={0,};
  for(int i = 0; i < N; i++){
    dp[i] = 1;
    dp2[i] = 1;
    arr[i] = 0;
    sub_len[i] = 0;
  } 

  for(int i = 0; i < N; i++) std::cin >> arr[i];
  for(int i = 0; i < N; i++){
    for(int j = 0; j < i; j++){
      if(arr[i] > arr[j]) dp[i] = std::max(dp[i],dp[j]+1);
    }
  }
  std::reverse(arr, arr+N);
  for(int i = 0; i < N; i++){
    for(int j = 0; j < i; j++){
      if(arr[i] > arr[j]) dp2[i] = std::max(dp2[i],dp2[j]+1);
    }
  }
  std::reverse(dp2, dp2+N);
  for(int i = 0; i < N; i++) sub_len[i] = dp[i] + dp2[i];

  // // debug
  // for(int i = 0; i < N; i++) std::cout << dp[i] << " ";
  // std::cout << std::endl;
  // for(int i = 0; i < N; i++) std::cout << dp2[i] << " ";
  // std::cout << std::endl;
  // for(int i = 0; i < N; i++) std::cout << sub_len[i] << " ";
  // std::cout << std::endl;

  std::cout << *std::max_element(sub_len,sub_len+N)-1 << std::endl;

}