#include <iostream>
#include <vector>

int max_idx(const std::vector<long long> &vec)
{
  int res = 4;
  long long max = 0;
  for(int i = 0; i < 3; i++)
    if(vec[i] > max) {
      max = vec[i];
      res = i;
    }
  return res;
}
void cut(std::vector <long long> &vec, std::vector<bool> &bit_idx)
{
  for(int i = 0; i < 3; i++) if(bit_idx[i]) vec[i]--;
}

int main()
{
  std::ios::sync_with_stdio(false);
  std::cin.tie(NULL);
  std::vector<long long>  inp(4);
  std::vector<bool>       bit_idx = {false,false,false};
  int idx = 4;
  long long ans = 0;
  int tot = 0;
  
  // std::cin >> tot;
  scanf("%d", &tot);
  while (tot != 0)
  {
    // for(auto i = 0; i < 4; i++) std::cin >> inp[i];
    scanf("%d %d %d %d", &inp[0], &inp[1], &inp[2], &inp[3]);
    for(int i = 0; i < inp[3]; i++) 
    {
      idx = max_idx(inp);
      bit_idx[idx] = true;
      // cut(inp, bit_idx);
      for(int j = 0; j < 3; j++) 
      {
        if(bit_idx[j]) inp[j]--;
        bit_idx[j] = false;
      }
      // std::fill(bit_idx.begin(), bit_idx.end(), false);
    }
    ans = inp[0]*inp[1]*inp[2];
    // std::cout << ans << " " << inp[0] << " " << inp[1] << " " << inp[2] << " " << inp[3] << "\n";
    // std::cout << ans << "\n";
    printf("%d\n", ans);
    tot--;
  }
  
}