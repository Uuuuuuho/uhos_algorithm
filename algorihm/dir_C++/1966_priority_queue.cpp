#include <iostream>
#include <queue>
#include <utility>

struct comp{
  bool operator()(std::pair<int,int> a, std::pair<int,int> b)
  {
    return a.second < b.second;
  }
};

int main()
{
  int num_test = 0, num_doc = 0, ans = 0, priority = 0, cnt = 0;
  std::cin >> num_test;
  for(int i = 0; i < num_test; i++){
    std::queue<std::pair<int,int>> que;
    std::priority_queue<int> p_que;
    cnt = 0;
    std::cin >> num_doc >> ans;
    for(int j = 0; j < num_doc; j++){
      std::cin >> priority;
      que.push(std::make_pair(j, priority));
      p_que.push(priority);
    }
    while(!que.empty()){
      int idx = que.front().first;
      int value = que.front().second;
      que.pop();
      if(p_que.top()==value){
        p_que.pop();
        ++cnt;
        if(idx == ans){
          std::cout << cnt << std::endl;
          break;
        }
      }
      else que.push({idx,value});
    }
  }
}