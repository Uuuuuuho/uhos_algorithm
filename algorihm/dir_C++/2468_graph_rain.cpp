#include <iostream>
#include <utility>
#include <string.h>
#include <queue>

int max = 0, N = 0;
int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
std::pair<int,int> graph[101][101] = {std::make_pair(0,0)};
int visited[101][101] = {0};

void find_safety(int x, int y)
{
  visited[x][y] = true;
  std::queue<std::pair<int,int>> que;
  que.push({x,y});
  
  while(!que.empty()){
    std::pair<int,int> curr = que.front(); // (x,y)
    que.pop();
    for(int i = 0; i < 4; i++){
      int nx = curr.first + dx[i];
      int ny = curr.second + dy[i];
      if(0<= nx && nx < N && 0 <= ny && ny < N)
        if(!graph[nx][ny].second && !visited[nx][ny]){
          que.push(std::make_pair(nx,ny));
          visited[nx][ny] = true;
        }
    }
  }

}

int main()
{
  int Max_safety = 0;
  std::cin >> N;
  int tmp_arr[101][101] = {0};
  int tmp = 0;
  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++)
    {
      std::cin >> tmp_arr[i][j];                        // first: height
      graph[i][j] = std::make_pair(tmp_arr[i][j],0);    // second: flag-swallowed
      max = (max < graph[i][j].first) ? graph[i][j].first : max;
    }
  }

  for(int rain = 0; rain < max; rain++) 
  {
    int cnt = 0;
    memset(visited,0,sizeof(visited));
    for(int j = 0; j < N; j++){
      for(int k = 0; k < N; k++){
        graph[j][k].second = (graph[j][k].first <= rain) ? true : false;
      }
    }

    for(int x = 0; x < N; x++)
      for(int y = 0; y < N; y++)
        if(!graph[x][y].second && !visited[x][y])
        {
          // this is BFS actually...
          find_safety(x, y);
          cnt++;
        }
    Max_safety = (Max_safety < cnt) ? cnt : Max_safety;
  }
  std::cout << Max_safety << std::endl;
}