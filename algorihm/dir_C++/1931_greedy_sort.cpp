#include <vector>
#include <algorithm>
#include <stdio.h>

int main()
{
  int N;
  std::vector<std::vector<int>> conf;
  
  scanf("%d\n", &N);
  conf.resize(N);

  /* input */
  for(int i = 0; i < N; i++)
  {
    conf[i].resize(2);
    scanf("%d %d", &conf[i][0], &conf[i][1]);
  }
  std::sort(conf.rbegin(), conf.rend());

  int last = conf[0][0];
  int cnt = 1;

  for(int i = 1; i < N; i++)
  {
    if((last >= conf[i][1]))
    {
      last = conf[i][0];
      cnt++;
    }
  }

  printf("%d", cnt);

}