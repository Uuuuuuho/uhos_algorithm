#include <iostream>

int main()
{
  int N = 0;
  int decomp = 2;
  std::cin >> N;
  while(N!=1){
    if(N%decomp==0){
      N /= decomp;
      std::cout << decomp << std::endl;
    }
    else decomp++;
  }
}