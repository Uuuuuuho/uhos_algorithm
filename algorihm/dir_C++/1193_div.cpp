#include <iostream>

int main()
{
  long long X = 0;
  std::cin >> X;
  long long curr = 1, layer = 1;
  while (curr<X)
  {
    layer++;
    curr += layer;
  }
  curr -= layer;
  
  long long step = 0;
  while(curr<X)
  {
    step++;
    curr++;
  }

  if(layer%2==0){
    std::cout << step << "/" << layer + 1 - step << std::endl;
  }  
  else{
    std::cout << layer + 1 - step << "/" << step << std::endl;
  }
}