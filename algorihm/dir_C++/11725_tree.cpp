#include <stdio.h>
#include <utility>
#include <string.h>
#include <vector>
#define MAX 100001

std::vector<int> tree[MAX];
bool visit[MAX];
int parent[MAX];

void find_parent(int node)
{
  visit[node] = true;

  for(auto iter: tree[node])
  {
    if(!visit[iter])
    {
      parent[iter] = node;
      find_parent(iter);
    }
  }
}

int main()
{
    int N = 0;
    scanf("%d", &N);
    memset(tree,0,sizeof(int)*100001);
    memset(visit,false,sizeof(bool)*100001);
    
    /* initialization */

    parent[1] = 1;
    visit[1] = 1;
    
    std::pair<int, int> tmp;

    for(int i = 2; i <= N; i++)
    {
      scanf("%d %d", &tmp.first, &tmp.second);
      tree[tmp.first].push_back(tmp.second);
      tree[tmp.second].push_back(tmp.first);
    }

    find_parent(1);

    for(int i = 2; i <= N; i++) printf("%d\n", parent[i]);

}