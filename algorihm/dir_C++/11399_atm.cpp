#include <stdio.h>
#include <vector>
#include <algorithm>

int main()
{
  int N = 0;
  scanf("%d", &N);
  std::vector<int> atm(N);
  for(int i = 0; i < N; i++) scanf("%d", &atm[i]);
  std::sort(atm.begin(), atm.end());
  // for(int i = 0; i < N; i++) printf("%d  ", atm[i]);
  int ans = 0;
  for(int i = 0; i < N; i++)
    for(int j = 0; j <= i; j++)
      ans += atm[j];
  printf("%d\n", ans);
}