#include <iostream>
#include <vector>
#define MAX 9

std::vector<bool> visit(MAX);
std::vector<int> vec;
int n_in = 0, m_in = 0;

void permute(int cnt)
{

  if(cnt == m_in)
  {
    for(auto it: vec) std::cout << it << " ";
    std::cout << "\n";
    return;
  }
  else
  {
    for(int i = 1; i <= n_in; i++)
    {
      if(visit[i] == true) continue;
      visit[i] = true;
      vec.push_back(i);
      permute(cnt + 1);
      visit[i] = false;
      vec.pop_back();
    }
  }
}

int main()
{
  std::cin >> n_in >> m_in;

  permute(0);
}
