#include <iostream>
#include <utility>
#include <string.h>

#define MAX(x, y) ((x) > (y)) ? (x) : (y)

int main()
{
  int N = 0, K = 0;
  std::cin >> N >> K;
  /* pair
    first: weight 
  & second: value 
  */
  std::pair<int,int> knapsack[N+1];
  int i = 0, j = 0;
  for(i = 1; i <= N; i++) 
  {
    std::cin >> knapsack[i].first >> knapsack[i].second;
  }


  int sol[N+1][K+1]; /* element represents value with the index paramter */
  int wgt = 0, val = 0;

  memset(sol, 0, (sizeof(int)*(N+1)*(K+1)));


  for(i = 1; i <= N; i++) 
  {
    wgt = knapsack[i].first;
    val = knapsack[i].second;
    for(j = 1; j <= K; j++)
    {

      if(wgt > j) 
      {
        sol[i][j] = sol[i-1][j];
      }
      else sol[i][j] = MAX((val + sol[i-1][j-wgt]), (sol[i-1][j]));
    }
  }
  std::cout << sol[N][K] << "\n";
}