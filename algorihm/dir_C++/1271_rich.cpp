#include <iostream>

int main()
{
  long long m = 0, n = 0, ans = 0, rem = 0;
  std::cin >> m >> n;
  ans = (long long)(m / n);
  rem = m % n;
  std::cout << ans << std::endl << rem << std::endl;
}