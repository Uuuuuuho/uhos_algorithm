#include <iostream>
#include <vector>
#include <algorithm>
// void get_rank(const std::vector<int> &b, std::vector<int> &rnk)
// {
//   int max = 0, prev_max = 0;
//   for(int i = 0; i < b.size(); i++)
//     if(prev_max < b[i]) prev_max = b[i];
//   prev_max++;

//   for(int i = 0; i < rnk.size(); i++){
//     for(int j = 0; j < b.size(); j++){
//       if((max < b[j]) && (prev_max > b[j])){
//         max = b[j];
//         rnk[j] = i;
//       }
//     }
//     prev_max = max;
//     max = 0;
//   }
// }
// void mapping(std::vector<int> &a, const std::vector<int> &b, std::vector<int> &re_a)
// {
//   std::vector<bool> visit(a.size());
//   std::fill(visit.begin(), visit.end(), false);
//   int min = 101, idx = 0;
//   for(int i = 0; i < a.size(); i++){
//     for(int j = 0; j < a.size(); j++){
//       if(a[j] < min && !visit[j]){
//         min = a[j];
//         idx = j;
//       }
//     }
//     visit[idx] = true;
//     re_a[idx] = i;
//     min = 101;
//   }
// }
int main()
{
  int N = 0, ans = 0;
  std::vector<int> a, b;

  std::cin >> N;
  a.resize(N);
  b.resize(N);
  for(int32_t i = 0; i < N; i++) std::cin >> a[i];
  for(int32_t i = 0; i < N; i++) std::cin >> b[i];

  std::sort(a.begin(), a.end());
  std::sort(b.begin(), b.end(), std::greater<>());
  for(int i = 0; i < N; i++) 
    ans += a[i]*b[i];
  std::cout << ans << "\n";
}