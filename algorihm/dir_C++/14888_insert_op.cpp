#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> op_seq;
std::vector<int> num_seq;
std::vector<bool> op_visit;
std::vector<bool> num_visit;

std::vector<int> v_num;
std::vector<int> v_op;
std::vector<int> result;


int N = 0, max = 0, min = 0, k = 0;

int ops[4] = {1,2,3,4};
/*   
  plus  = 1,
  minus = 2,
  mul   = 3,
  div   = 4 
*/

int dfs(int k)
{
  if(k == (N))
  {
    int ret = num_seq.front();
    for(int n_i = 1; n_i < num_seq.size(); n_i++)
    {
      switch (v_op[n_i-1])
      {
        case 1:   /* plus */
          ret += num_seq[n_i];
          break;
        case 2:   /* minus */
          ret -= num_seq[n_i];
          break;
        case 3:   /* mul */
          ret *= num_seq[n_i];
          break;
        case 4:   /* div */
          ret /= num_seq[n_i];
          break;
        default:
          break;
      }
    }
    return ret;
  }
  else
  {
    for(int j = 0; j < N-1; j++)
    {
      if(op_visit[j] == true) continue;
      op_visit[j] = true;
      v_op.push_back(op_seq[j]);
      dfs(k+1);
      v_op.pop_back();
      op_visit[j] = false;
    }
    if(k == (N-1)) result.push_back(dfs(N));
    return 0;
  }
}

int main()
{
  int tmp = 0;
  
  std::cin >> N;

  num_seq.resize(N);
  num_visit.resize(N);
  op_visit.resize(N-1);

  for(int i = 0; i < N; i++) std::cin >> num_seq[i];
  for(int i = 0; i < 4; i++) 
  {
    std::cin >> tmp;
    for(int j = 0; j < tmp; j++) op_seq.push_back(ops[i]);
  }

  dfs(0);


  std::sort(result.begin(), result.end());

  std::cout << result.back() << "\n" << result.front() << "\n";
}