#include <iostream>
#include <cmath>
#define MAX 1000001

int main()
{
  int start = 0, end = 0;

  std::cin >> start >> end;

  int i = 0, j = 0;
  bool isPrime[MAX];
  std::fill_n(isPrime, end+1, true);

  isPrime[0] = false;
  isPrime[1] = false;

  int cnt = 0;

  for(i = 2; i <= sqrt(end); i++)
  {
    for(j = 2 * i; j <= end; j += i)
    {
      isPrime[j] = false;
    }
  }


  for(i = start; i <= end; i++)
    if(isPrime[i]) std::cout << i << "\n";
}