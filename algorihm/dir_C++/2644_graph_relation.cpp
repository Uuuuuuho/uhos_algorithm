#include <iostream>
#define MAX 101
int adj_mat[MAX][MAX] = {0};
int visited[MAX] = {0};
int size = 0;


void floyd()
{
  for(int k = 1; k <= size; k++)
    for(int i = 1; i <= size; i++)
      for(int j = 1; j <= size; j++)
        if(adj_mat[i][j]) continue;
        else if(adj_mat[i][k] && adj_mat[k][j]) adj_mat[i][j] = adj_mat[i][k] + adj_mat[k][j];
}

int main()
{
  int tmp = 0, tar1 = 0, tar2 = 0, s = 0, d = 0;
  scanf("%d", &size);
  scanf("%d %d", &tar1, &tar2);
  scanf("%d", &tmp);
  for(int i = 0; i < tmp; i++){
    scanf("%d %d", &s, &d);
    adj_mat[s][d] = 1;
    adj_mat[d][s] = 1;
  }

  floyd();

  if(adj_mat[tar1][tar2]) printf("%d\n", adj_mat[tar1][tar2]);
  else                    printf("-1\n");
}