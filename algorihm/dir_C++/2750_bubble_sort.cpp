#include <iostream>

int main()  // bubble sort
{
  int arr[1001] = {0,};
  int N = 0, i = 0, j = 0;
  int tmp = 0;
  std::cin >> N;

  for(i = 0; i < N; i++)
    std::cin >> arr[i];
  
  // std::cout << std::endl;
  
  for(j = N-1; j > 0; j--)
  {
    for(i = 0; i < j; i++)
    {
      if(arr[i] > arr[i+1]) 
      {
        tmp = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = tmp;
      }
    }
  }

  for(i = 0; i < N; i++)
    std::cout << arr[i] << std::endl;
}