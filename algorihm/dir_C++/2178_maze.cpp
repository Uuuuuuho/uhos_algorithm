#include <iostream>
#include <list>
#include <string.h>
#include <utility>

#define MAX 101

int32_t mat[MAX][MAX];
bool visit[MAX][MAX];
int32_t dx[4] = {1,-1,0,0};
int32_t dy[4] = {0,0,1,-1};
int32_t N = 0, M = 0;

void shortest_path(int32_t x, int32_t y)
{
  std::list<std::pair<int32_t,int32_t>> q_v;
  std::pair<int32_t, int32_t> q_front;
  int32_t nx = 0, ny = 0;

  visit[x][y] = true;
  q_v.push_back(std::pair<int32_t,int32_t>(x,y));
  
  while(!q_v.empty())
  {
    q_front = q_v.front();
    q_v.pop_front();

    for(int32_t i = 0; i < 4; i++)
    {
      nx = q_front.first  + dx[i];
      ny = q_front.second + dy[i];
      if((nx < 0) || (nx >= N) || (ny < 0) || (ny >= M)) continue;
      if((!visit[nx][ny]) && (mat[nx][ny] == 1))
      {
        q_v.push_back(std::pair<int32_t,int32_t>(nx,ny));
        mat[nx][ny] += mat[q_front.first][q_front.second];
        visit[nx][ny] = true;
      } 
    }
  }
}

int main()
{
  int32_t x = 0, y = 0;
  std::cin >> N >> M;
  std::string tmp;

  memset(mat, 0, sizeof(int32_t)*MAX*MAX);
  memset(visit, false, sizeof(bool)*MAX*MAX);

  for(y = 0; y < N; y++)
  {
    std::cin >> tmp;
    for(x = 0; x < M; x++)
    {
      mat[y][x] = tmp[x] - '0';
    }
  }

  shortest_path(0, 0);
  std::cout << mat[N-1][M-1] << "\n";
}