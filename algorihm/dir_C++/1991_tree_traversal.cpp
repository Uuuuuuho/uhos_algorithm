#include <iostream>
#include <vector>
#include <map>

void preorder(std::map<char,std::pair<char,char>>&tree, char node){
  if(node!='.') std::cout << node;
  if(tree[node].first) preorder(tree, tree[node].first);
  if(tree[node].second) preorder(tree, tree[node].second);
}
void inorder(std::map<char,std::pair<char,char>>&tree, char node){
  if(tree[node].first) inorder(tree, tree[node].first);
  if(node!='.') std::cout << node;
  if(tree[node].second) inorder(tree, tree[node].second);
}
void postorder(std::map<char,std::pair<char,char>>&tree, char node){
  if(tree[node].first) postorder(tree, tree[node].first);
  if(tree[node].second) postorder(tree, tree[node].second);
  if(node!='.') std::cout << node;
}

int main()
{
  std::map<char,std::pair<char,char>>tree;
  int N = 0;
  char node, left, right;

  std::cin >> N;
  for(int i = 0; i < N; i++){
    std::cin >> node >> left >> right;
    tree[node] = std::make_pair(left,right);
  }
  preorder(tree, 'A');
  std::cout << "\n";
  inorder(tree, 'A');
  std::cout << "\n";
  postorder(tree, 'A');
}