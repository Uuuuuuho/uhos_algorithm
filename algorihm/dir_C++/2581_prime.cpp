#include <iostream>
#include <vector>

int main()
{
  int start = 0, end = 0;
  int cnt = 0;
  int sum = 0;
  std::vector<int> v;

  std::cin >> start;
  std::cin >> end;

  if(start == 2)
  {
    sum += 2;
    v.push_back(2);
  }

  int i = 0, j = 0;
  if(start % 2 == 0) start++;

  for(i = start; i <= end; i++)
  {
    for(j = 1; j <= i; j++)
    {
      if(!(i % j)) cnt++;
    }
    if(cnt == 2)
    {
      // std::cout << "isNotPrime: " << i << std::endl;
      cnt++;
      sum += i;
      v.push_back(i);
    }
    cnt = 0;
  }
  if(v.empty()) std::cout << "-1" << "\n";
  else 
  {
    std::cout << sum << "\n";
    std::cout << v.front() << "\n";
  }
}
