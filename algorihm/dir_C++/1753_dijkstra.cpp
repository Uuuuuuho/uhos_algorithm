#include <iostream>
#include <vector>
#include <queue>
#define INF 1e9

void dijkstra(std::vector<std::pair<int,int>> adj[], uint32_t dist[], const int n_v, const int src)
{
  std::fill(dist, dist+n_v+1, INF);
  std::priority_queue<std::pair<uint32_t,uint32_t>> p_que;

  p_que.push({0,src});
  dist[src] = 0;

  while(!p_que.empty()){
    int cost = -p_que.top().first;
    int curr = p_que.top().second;
    p_que.pop();

    for(int i = 0; i < adj[curr].size(); i++)
    {
      int next = adj[curr][i].first;
      int nextcost = adj[curr][i].second;

      // relaxation
      if(dist[next] > dist[curr] + nextcost)
      {
        dist[next] = dist[curr] + nextcost;
        p_que.push({-dist[next],next});
      }
    }
  }
}

int main()
{
  uint32_t n_v = 0, n_e = 0, src = 0, u = 0, v = 0, w = 0;
  scanf("%d %d", &n_v, &n_e);
  scanf("%d", &src);
  // for small size of graph
  std::vector<std::pair<int,int>> adj_list[n_v+1];
  uint32_t dist[n_v+1];

  for(int iter = 0; iter < n_e; iter++)
  {
    scanf("%d %d %d",&u, &v, &w);
    adj_list[u].push_back({v,w});
  }

  dijkstra(adj_list, dist, n_v, src);

  for(int i = 1; i < n_v+1; i++)
  {
    if(dist[i] == INF)  printf("INF\n");
    else                printf("%d\n", dist[i]);
  }
}

