#include <iostream>

int main()
{
  uint32_t n_case = 0;
  std::cin >> n_case;
  uint32_t sums[n_case] = {0};
  for(int iter = 0; iter < n_case; iter++) std::cin >> sums[iter];
  
  uint32_t dp[12] = {0};
  dp[0] = 0;
  dp[1] = 1;
  dp[2] = 2;
  dp[3] = 4;
  for(int iter = 4; iter < 13; iter++) dp[iter] = dp[iter-1] + dp[iter-2] + dp[iter-3];
  
  for(int iter = 0; iter < n_case; iter++) std::cout << dp[sums[iter]] << std::endl;
  

}