#include <iostream>
#define MAX 101
int adj_mat[MAX][MAX] = {0};
int size = 0;


void floyd()
{
  for(int k = 1; k <= size; k++)
    for(int i = 1; i <= size; i++)
      for(int j = 1; j <= size; j++)
        if(i==j) continue;
        else if(adj_mat[i][k] && adj_mat[k][j]){
          if(adj_mat[i][j]) adj_mat[i][j] = std::min(adj_mat[i][k] + adj_mat[k][j], adj_mat[i][j]);
          else              adj_mat[i][j] = adj_mat[i][k] + adj_mat[k][j];
        } 
}

int main()
{
  int tmp = 0, s = 0, d = 0, cost = 0;
  scanf("%d", &size);
  scanf("%d", &tmp);
  for(int i = 0; i < tmp; i++){
    scanf("%d %d %d", &s, &d, &cost);
    if(adj_mat[s][d]==0) adj_mat[s][d] = cost;
    else
      if(adj_mat[s][d]>cost) adj_mat[s][d]=cost;    
  }

  // printf("\n");
  // for(int i = 1; i <= size; i++){
  //   for(int j = 1; j <= size; j++){
  //     printf("%d ", adj_mat[i][j]);
  //   }
  //   printf("\n");
  // }
  // printf("\n");

  floyd();

  for(int i = 1; i <= size; i++){
    for(int j = 1; j <= size; j++){
      if(adj_mat[i][j]) printf("%d ", adj_mat[i][j]);
      else              printf("0 ");
    }
    printf("\n");
  }
}