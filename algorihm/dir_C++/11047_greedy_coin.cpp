#include <iostream>
#include <vector>

int main()
{
  int N = 0, answer = 0;
  int cnt_coin = 0;

  std::cin >> N >> answer;
  std::vector<int> coins(N);
  
  for(int i = 0; i < N; i++)
    std::cin >> coins[i];

  int max_idx = N - 1;
  do
  {
    if(answer >= coins[max_idx])
    {
      answer -= coins[max_idx];
      cnt_coin++;
    }
    else
    {
      max_idx--;
    }
    
  } while (answer > 0);
  
  std::cout << cnt_coin << "\n";
}