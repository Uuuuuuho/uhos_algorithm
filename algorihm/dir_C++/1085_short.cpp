#include <iostream>

int main()
{
  int x = 0, y = 0, w = 0, h = 0;
  int x_dis = 0, y_dis = 0;
  int min = 0;

  std::cin >> x >> y >> w >> h;

  x_dis = (x > (w-x))?(w-x):x;
  y_dis = (y > (h-y))?(h-y):y;
  min = (x_dis > y_dis)?y_dis:x_dis;
  std::cout << min << "\n";
}