#include <iostream>
#include <vector>
#define MAX 9

std::vector<int> q;
int n_in = 0, m_in = 0;

void permute(int start, int cnt)
{

  if(cnt == m_in)
  {
    for(auto it: q) std::cout << it << " ";
    std::cout << "\n";
    return;
  }
  else
  {
    for(int i = start; i <= n_in; i++)
    {
      if(!q.empty() && (q.back() > i)) continue;
      q.push_back(i);
      permute(start, cnt + 1);
      q.pop_back();
    }
  }
}

int main()
{
  std::cin >> n_in >> m_in;

  permute(1, 0);
}
