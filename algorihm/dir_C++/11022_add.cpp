#include <iostream>

int main()
{
  int a = 0, b = 0, num_test = 0;
  std::cin >> num_test;
  for(int i = 0; i < num_test; i++){
    std::cin >> a >> b;
    std::cout << "Case #" << i+1 << ": " << a << " + " << b << " = " << a+b << std::endl;
  }
}