
def determine_leap_year(y):
  if((y%4==0) and (y%100!=0 or y%400==0)):
    return True
  else:
    return False

if(__name__=="__main__"):
  year = int(input())
  if(determine_leap_year(year)):
    print(1)
  else:
    print(0)