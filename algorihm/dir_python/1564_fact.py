if __name__=="__main__":
  N = int(input())
  ans = 1
  for i in range(2,N+1):
    ans *= i
    while True:
      # print(str(ans)[-1])
      if str(ans)[-1]=="0":
        ans//=10
      else:
        break
    ans%=100000000000000000
  print(str(ans)[-5:])
