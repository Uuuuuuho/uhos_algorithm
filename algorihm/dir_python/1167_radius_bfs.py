import sys
from collections import deque

def bfs(start):
  global num_vert, edge
  check_bfs = [-1]*(num_vert+1)
  check_bfs[start] = 0

  que = deque([])
  que.append(start)
  max = [0,0]

  while que:
    front_v = que.popleft()
    for v,e in edge[front_v]:
      if(check_bfs[v]==-1):
        check_bfs[v]=check_bfs[front_v]+e
        que.append(v)
        if(max[1]<check_bfs[v]):
          max = [v,check_bfs[v]]

  return max

if(__name__=="__main__"):
  num_vert = int(sys.stdin.readline())
  edge = [[] for _ in range(num_vert+1)]

  for _ in range(num_vert):
    tmp = list(map(int, sys.stdin.readline().split()))
    for e in range(1,len(tmp)-2,2):
      edge[tmp[0]].append((tmp[e], tmp[e+1]))

  node,dis = bfs(1)
  node,dis = bfs(node)

  print(dis)