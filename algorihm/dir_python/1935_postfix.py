if __name__=="__main__":
  N = int(input())
  post = list(input())
  post = list(reversed(post))
  eng = [-1]*26
  
  for i in range(N):
    eng[i] = int(input())

  st = []
  
  while post:
    l_most = post[-1]
    if l_most.isalpha():
      st.append(eng[ord(l_most)-65])
    elif l_most=="*":
      fst = st.pop()
      snd = st.pop()
      st.append(fst*snd)
    elif l_most=="+":
      fst = st.pop()
      snd = st.pop()
      st.append(fst+snd)
    elif l_most=="/":
      snd = st.pop()
      fst = st.pop()
      st.append(fst/snd)
    elif l_most=="-":
      snd = st.pop()
      fst = st.pop()
      st.append(fst-snd)
    post.pop()
  
  print(format(st[-1],".2f"))