if __name__=="__main__":
  N,S = map(int, input().split())
  seq = list(map(int,input().split()))
  ans = 0
  ans_seq = []
  for start in range(N+1):
    for end in range(start,N+1):
      sum = 0
      for i in range(start,end):
        sum += seq[i]
        if sum==S:
          tmp = [start,i]
          if tmp in ans_seq:
            continue
          else:
            print(start,i)
            ans_seq.append(tmp)
            ans += 1
            continue
      sum = 0
      for i in range(end-1,start-1,-1):
        sum += seq[i]
        if sum==S:
          tmp = [end,i]
          if tmp in ans_seq:
            continue
          else:
            print(end,i)
            ans_seq.append(tmp)
            ans += 1
            continue
  print(ans)

