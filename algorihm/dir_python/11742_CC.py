import sys
sys.setrecursionlimit(100000)

# this dfs works fine for undirectional graphs.
def dfs(v):
  global check_dfs, stack, edge
  check_dfs[v] = True
  # print(v, end=' ')
  stack.append(v)
  for e in edge[v]:
    if check_dfs[e] == False:
      dfs(e)

def dfs_Inv(v):
  global check_dfs_T, scc, edge_T
  check_dfs_T[v] = True
  scc.append(v)
  for e in edge_T[v]:
    if check_dfs_T[e] == False:
      dfs_Inv(e)

if __name__=="__main__":
  num_v, num_e = map(int, sys.stdin.readline().split())

  edge = [[] for _ in range(num_v+1)]

  for v in range(1,num_v+1):
    edge[v].append(v)

  for _ in range(num_e):
    v1, v2 = map(int, sys.stdin.readline().split())
    edge[v1].append(v2)
    edge[v2].append(v1)

  for v in range(num_v+1):
    edge[v].sort()

  edge_T = [[] for _ in range(num_v+1)]

  for i in range (num_v+1):
    for j in range (len(edge[i])):
      edge_T[edge[i][j]].append(i)

  stack = []
  check_dfs = [False for _ in range(1001)]

  check_dfs_T = [False for _ in range(1001)]
  scc = []

  result = []
    
  for start in range(num_v+1):
    if((check_dfs[start] == False) and (start != 0)):
      dfs(start)

  while(stack):
    start = stack.pop()
    # print(start)
    if check_dfs_T[start] == False:
      dfs_Inv(start)
      result.append(sorted(scc))
      scc.clear()
      # print(result)

  result = sorted(result)
  ans = 0
  for i in result:
    ans += 1
    # print(*i, -1)

  print(ans)