if __name__=="__main__":
  num_test = int(input())
  for _ in range(num_test):
    num_cand = int(input())
    cand = []
    for i in range(num_cand):
      cand.append(list(map(int,input().split())))
    cand.sort()
    ans = 1
    std = cand[0][1]
    for i in range(1,num_cand):
      if std>cand[i][1]:
        ans += 1
        std = cand[i][1]
    print(ans)

