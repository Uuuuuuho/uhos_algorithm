if __name__=="__main__":
  mid = list(input())
  mid = list(reversed(mid))
  ans = []
  st = []
  while mid:
    top = mid[-1]
    if top=="(":
      st.append(top)
    elif top=="*" or top=="/":
      while st and (st[-1]=="*" or st[-1]=="/"):
        ans.append(st.pop())
      st.append(top)
    elif top=="+" or top=="-":
      while st and st[-1]!="(":
        ans.append(st.pop())
      st.append(top)
    elif top==")":
      while st[-1]!="(":
        ans.append(st.pop())
      st.pop()
    else:
      ans.append(top)
    mid.pop()

  while st:
    ans.append(st.pop())

  ans = list(reversed(ans))
  while ans:
    print(ans[-1],end="")
    ans.pop()