import itertools

if __name__=="__main__":
  N, M = map(int, input().split())
  nums = map(int, input().split())
  nums = list(sorted(nums))
  nCr = list(itertools.combinations(nums,M))
  for it in nCr:
    for indiv in it:
      print(indiv, end=" ")
    print()