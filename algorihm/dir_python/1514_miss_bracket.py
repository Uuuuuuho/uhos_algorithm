if __name__=="__main__":
  eq = list(input().split("-"))
  # print(eq)
  ans_list = []
  for it in eq:
    add = 0
    sub_list = it.split("+")
    for i in sub_list:
      add += int(i)
    ans_list.append(add)
  # print(ans_list)
  ans = ans_list[0]
  for i in range(1,len(ans_list)):
    ans -= ans_list[i]
  print(ans)
