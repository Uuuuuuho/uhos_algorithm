if __name__=="__main__":
  n, m = map(int, input().split())
  board = [[]*m for _ in range(n)]
  change = [[0]*m for _ in range(n)]

  dir_x = [-1,1,0,0]
  dir_y = [0,0,-1,1]

  for i in range(n):
    board[i] = list(input())

  for y in range(n):
    for x in range(m):
      for k in range(4):
        nx = x+dir_x[k]
        ny = y+dir_y[k]
        if 0<=ny<n and 0<=nx<m and board[y][x]==board[ny][nx]:
          change[y][x] +=1
  
  for i in range(n):
    print(change[i])
