from operator import truediv
import sys

stack = []
flag = True

def push(x):
    stack.append(x)
def pop():
    if(len(stack) != 0):
        stack.pop() 
    else:
        global flag
        flag = False
def is_empty():
    if((len(stack) == 0) and (flag == True)):  print("YES")
    else: print("NO")

if(__name__ == "__main__"):
    num_test = int(sys.stdin.readline())
    bracket = [0 for _ in range(num_test)]
    for j in range(num_test):
        bracket[j] = list(input())

    for j in range(num_test):
        flag = True
        while(len(stack) != 0):
            pop()
        for i in range(len(bracket[j])):
            if(bracket[j][i]=="("):
                # this is an arbitrary value
                push(1) 
            else:
                pop()
        is_empty()            
