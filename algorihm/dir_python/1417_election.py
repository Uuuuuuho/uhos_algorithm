if __name__=="__main__":
  n = int(input())
  candidates = [0]*n

  for i in range(n):
    candidates[i] = int(input())

  if n==1:
    print(0)
    exit()
  max_can = max(candidates[1:])
  ans = 0

  while candidates[0] <= max_can:
    idx = candidates[1:].index(max_can) + 1
    candidates[idx] = candidates[idx] - 1
    candidates[0] = candidates[0] + 1
    max_can = max(candidates[1:])
    ans = ans + 1
  print(ans)
