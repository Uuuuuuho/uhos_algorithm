def sol(n):
  ans = 0
  for i in range(n+1,n**2,n+1):
    ans += i
  return ans

if __name__=="__main__":
  N = int(input())
  ans = sol(N)
  print(ans)