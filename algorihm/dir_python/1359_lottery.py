dp_comb = [[0]*31 for _ in range(31)]

def combination(n, r):
  if n==r or r==0:
    return 1
  else:
    comb1 = 0
    comb2 = 0
    if dp_comb[n-1][r-1]!=0:
      comb1 = dp_comb[n-1][r-1]
    else:
      dp_comb[n-1][r-1] = combination(n-1,r-1)
      comb1 = dp_comb[n-1][r-1]
    if dp_comb[n-1][r]!=0:
      comb2 = dp_comb[n-1][r]
    else:
      dp_comb[n-1][r] = combination(n-1,r)
      comb2 = dp_comb[n-1][r]
    return comb1+comb2

if __name__=="__main__":
  n, m, k = map(int, input().split())
  num = 0
  while k<=m:
    if n-m<m-k:
      k += 1
      continue
    num += combination(m,k)*combination(n-m,m-k)
    k += 1
  div = combination(n,m)
  if num >= div:
    print(1.0)
  else:
    print(num/div)
