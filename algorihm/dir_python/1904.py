import sys

dp = [0 for _ in range(1000001)]
dp[1] = 1
dp[2] = 2

if __name__ == "__main__":
  N = int(sys.stdin.readline())
  for j in range(3, N+1):
    dp[j] = (dp[j-1] + dp[j-2]) % 15746
  print(dp[N])


