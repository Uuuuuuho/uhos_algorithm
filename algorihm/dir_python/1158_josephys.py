import sys
from collections import deque

if(__name__=="__main__"):
  N, K = map(int, sys.stdin.readline().split())
  cQ = deque([])
  ans = deque([])

  for i in range(1,N+1):
    cQ.append(i)
  
  while(cQ):
    cnt = 0
    for i in range(K-1):
      cnt += 1
      cQ.append(cQ.popleft())
    ans.append(cQ[0])    
    cQ.popleft()

  print("<",end='')
  for i in range(N-1):
    print(ans[i],end=', ')
  print(ans[N-1],end='')
  print(">")
    