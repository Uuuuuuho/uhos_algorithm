dp = [0]*252

if __name__=="__main__":
  dp[0] = 1
  dp[1] = 1
  dp[2] = 3
  for i in range(3,252):
    dp[i] = dp[i-1] + dp[i-2]*2
  while(1):
    try:
      n = int(input())
      print(dp[n])
    except:
      break
    