import sys
from collections import deque
que = deque([])

if(__name__=="__main__"):
  T = int(sys.stdin.readline())
  
  while(T!=0):
    A, B = map(int, sys.stdin.readline().split())
    que.append(A+B)
    T -= 1
  
  while(que):
    print(que[0])
    que.popleft()