if __name__=="__main__":
  n,m = map(int, input().split())
  guard = [[0]*60 for _ in range(60)]
  for i in range(n):
    guard[i] = str(input().split())
  
  rows = n
  cols = m
  # for i in range(n):
  #   for j in range(2,m+2):
  #     print(guard[i][j],end="")
  #   print()
  for i in range(n):
    if "X" in guard[i]:
      rows = rows - 1
  for i in range(2,m+2):
    for j in range(n):
      if guard[j][i]=="X":
        # print(j, i)
        cols = cols -1
        break
    # print(i,guard[:][i])
    # if "X" in guard[:][i]:
    #   cols = cols - 1
  # print(rows, cols)
  print(max(rows, cols))