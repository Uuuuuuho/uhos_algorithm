if __name__=="__main__":
  n = list(map(int, input()))
  while n!=[0]:
    n_rev = list(reversed(n))
    if n==n_rev:
      print("yes")
    else:
      print("no")
    n = list(map(int, input()))
