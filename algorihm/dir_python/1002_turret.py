if __name__=="__main__":
  num_test = int(input())
  for _ in range(num_test):
    pt = list(map(int,input().split()))
    diff = ((pt[0]-pt[3])**2 + (pt[1]-pt[4])**2)**0.5
    if diff==0 and pt[2]==pt[5]:
      print(-1)
    elif pt[2]+pt[5]==diff or abs(pt[2]-pt[5])==diff:
      print(1)
    elif abs(pt[2]-pt[5]) < diff < pt[2]+pt[5]:
      print(2)
    else:
      print(0)
