import sys
from collections import deque

def gcd(a,b):
  while(b>0):
    a,b = b, a%b
  return a

def lcm(a,b):
  return a*b

if(__name__=="__main__"):
  a, b = map(int, sys.stdin.readline().split())
  g = gcd(a,b)
  print(g)
  print(lcm(a,b)//g)