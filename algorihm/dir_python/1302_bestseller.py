if __name__=="__main__":
  n = int(input())
  sellers = {}

  for _ in range(n):
    tmp = input()
    if tmp not in sellers:
      sellers[tmp] = 1
    else:
      sellers[tmp] += 1

  best = max(sellers.values())

  ans = []
  for book, cnt in sellers.items():
    if cnt==best:
      ans.append(book)

  print(sorted(ans)[0])