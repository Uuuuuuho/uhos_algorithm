if __name__=="__main__":
  n,m = map(int,input().split())
  A = [0]*n
  for i in range(n):
    A[i] = list(map(int,input().split()))
  m,k = map(int,input().split())
  B = [0]*m
  for i in range(m):
    B[i] = list(map(int,input().split()))
  ans = [[0]*k for _ in range(n)]

  for i in range(n):
    for it in range(k):
      for j in range(m):
        ans[i][it] += A[i][j]*B[j][it]

  for i in range(n):
    for it in range(k):
      print(ans[i][it], end=" ")
    print()