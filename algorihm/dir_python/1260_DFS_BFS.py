
n, m, start_v = map(int, input().split())

edge = [[] for _ in range(10001)]

for _ in range(m):
    v1, v2 = map(int, input().split())
    edge[v1].append(v2)
    edge[v2].append(v1)

for e in range(m):
    edge[e].sort()
    # print(edge[e])

check_dfs = [False for _ in range(1001)]

def dfs(v):
    check_dfs[v] = True
    print(v, end=' ')
    for e in edge[v]:
        if check_dfs[e] == False:
            dfs(e)

from collections import deque
q_v = deque([])
check_bfs = [False for _ in range(1001)]

def bfs(v):
    check_bfs[v] = True
    q_v.append(v)

    while q_v:
        front_v = q_v.popleft()
        print(front_v, end=' ')
        
        for e in edge[front_v]:
            if (check_bfs[e] == False): 
                check_bfs[e] = True
                q_v.append(e)

dfs(start_v)
print()

bfs(start_v)
print()