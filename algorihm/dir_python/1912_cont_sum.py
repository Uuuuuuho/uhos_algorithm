import sys

if __name__=='__main__':
  N = int(sys.stdin.readline())
  in_list = list(map(int, sys.stdin.readline().split()))

  acc = 0
  for start in range(1,N):
    in_list[start] = max(in_list[start], in_list[start]+in_list[start-1])
    # print(in_list[start])
  print(max(in_list))