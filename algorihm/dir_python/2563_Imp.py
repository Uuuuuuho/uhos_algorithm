num_rec = int(input())

rec = [[0 for _ in range(101)] for _ in range(101)]

area = 0

for _ in range(num_rec):
    x, y = map(int, input().split())
    for i in range(x, x+10):
        for j in range(y, y+10):
            rec[i][j] = 1

for i in range(101):
    for j in range(101):
        area += (rec[i][j] == 1)
        
print(area)
