import sys

if(__name__=="__main__"):
  N, K = map(int, sys.stdin.readline().split())

  primes = [1]*(N+1)
  primes[0] = 0
  primes[1] = 0
  ans = 0

  for i in range(2, N+1):
    for j in range(i,N+1,i):
      if(primes[j]==1):
        primes[j] = 0
        K -= 1
        # print(j, K)
        if(K==0):
          ans = j
          break
    if(K==0):
      break

  print(ans)