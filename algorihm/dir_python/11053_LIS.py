import sys

if __name__=="__main__":
  arr_size = int(sys.stdin.readline())
  in_arr = list(map(int, sys.stdin.readline().split()))
  dp = [1]*(arr_size+1)
  
  for snd in range(arr_size):
    for fst in range(snd):
      if in_arr[fst] < in_arr[snd]:
        # in_arr[snd] = in_arr[fst]
        dp[snd] = max(dp[snd],dp[fst]+1)
    #   print(in_arr)
    # print(dp)
  
  print(max(dp))