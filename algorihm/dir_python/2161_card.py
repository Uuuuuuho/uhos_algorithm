if __name__=="__main__":
  N = int(input())
  cards = [0]*N
  for i in range(N):
    cards[i] = i+1

  while cards:
    print(cards[0], end=" ")
    cards.pop(0)
    if len(cards)==0:
      break
    cards.append(cards[0])
    cards.pop(0)