from collections import deque

que = deque([])

def push(x):
  que.append(x)

def pop():
  if not que:
    print("-1")
  else:
    print(que.popleft())

def size():
  print(len(que))

def empty():
  if not que:
    print("1")
  else:
      print("0")

def front():
  if not que:
    print("-1")
  else:
    print(que[0])

def back():
  if not que:
    print("-1")
  else:
    length = len(que)-1
    print(que[length])
  

import sys

if __name__ == "__main__":
  no_cmd = int(sys.stdin.readline())
  cmd = []
  for _ in range(no_cmd):
    cmd.append(sys.stdin.readline().split())
  
  for i in range(no_cmd):
    if(cmd[i][0]=="push"):
      push(int(cmd[i][1]))
    elif(cmd[i][0]=="pop"):
      pop()
    elif(cmd[i][0]=="size"):
      size()
    elif(cmd[i][0]=="empty"):
      empty()
    elif(cmd[i][0]=="front"):
      front()
    elif(cmd[i][0]=="back"):
      back()
  