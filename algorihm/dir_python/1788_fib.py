if __name__=="__main__":
  n = int(input())
  dp = [0]*1000005
  dp[0] = 0
  dp[1] = 1
  # print(len(dp))
  
  if n==0:
    print(0)
    print(0)
    exit()
  elif n<0:
    if abs(n)%2==0:
      print(-1)
    else:
      print(1)
  else:
    print(1)
  
  abs_n = abs(n)

  for i in range(2,abs_n+1):
    dp[i] = (dp[i-1] + dp[i-2])%1000000000
  print(dp[abs_n])