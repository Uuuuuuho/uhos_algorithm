

def calc(hour, min):
  if(min<45):
    if(hour==0):
      return (23, 60-(45-min))
    else:
      return (hour-1, 60-(45-min))
  else:
    return (hour, min-45)


if(__name__=="__main__"):
  H, M = map(int, input().split())
  res_h, res_m = calc(H, M)
  print(res_h, res_m)