if __name__=="__main__":
  N = int(input())
  arr = [[] for _ in range(N)]
  for i in range(N):
    arr[i] = list(input())
  # print(arr)

  ans_row = 0
  ans_col = 0
  """ sol """
  for i in range(N):
    row, col = 0, 0
    for j in range(N):
      """ row """
      if arr[i][j]=='.':
        row += 1
        if row==2:
          ans_row+=1
      else:
        row = 0
      
      """ col """
      if arr[j][i]=='.':
        col += 1
        if col==2:
          ans_col+=1
      else:
        col = 0

  print(ans_row, ans_col)