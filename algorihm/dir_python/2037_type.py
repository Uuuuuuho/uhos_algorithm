if __name__=="__main__":
  pad = [
    [" "],
    ["A", "B", "C"],
    ["D", "E", "F"],
    ["G", "H", "I"],
    ["J", "K", "L"],
    ["M", "N", "O"],
    ["P", "Q", "R", "S"],
    ["T", "U", "V"],
    ["W", "X", "Y", "Z"]
  ]
  p, w = map(int, input().split())
  # in_str = list(map(str, input().split()))
  in_str = list(input())
  # print(in_str)

  ans = 0
  str_r = ""

  for i in range(len(in_str)):
    for j in range(9):
      if in_str[i] in pad[j]:
        # print(pad[j].index(in_str[i]))
        if str_r!=" " and str_r in pad[j]:
          ans = ans + p*(pad[j].index(in_str[i])+1) + w
        else:
          ans = ans + p*(pad[j].index(in_str[i])+1)
        str_r = in_str[i]
  print(ans)