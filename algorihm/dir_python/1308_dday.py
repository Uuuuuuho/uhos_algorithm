import datetime
if __name__=="__main__":
  curr_y,curr_m,curr_d = map(int,input().split())
  dday_y,dday_m,dday_d = map(int,input().split())
  # print(str(datetime.date(dday_y,dday_m,dday_d)-datetime.date(curr_y,curr_m,curr_d)))
  last_days = int(str(datetime.date(dday_y,dday_m,dday_d)-datetime.date(curr_y,curr_m,curr_d)).split()[0])
  
  days = 0
  for i in range(curr_y, curr_y + 1000):
    if i%400==0:
      days+=366
    elif i%100==0:
      days+=365
    elif i%4==0:
      days+=366
    else:
      days+=365
  
  # print(last_days,days)
  if last_days>=days:
    print("gg")
  else:
    print(f'D-{last_days}')
