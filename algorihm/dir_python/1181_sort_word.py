if __name__=="__main__":
  N = int(input())
  words = []
  for i in range(N):
    words.append(input())
  words = list(dict.fromkeys(words))

  max_len = 0
  for i in range(len(words)):
    max_len = max(max_len,len(words[i]))
  sep_words = [[""] for _ in range(max_len)]
  # append words to list regarding to characters...
  for i in range(len(words)):
    sep_words[len(words[i])-1].append(words[i])
  # sorting...
  for i in range(max_len):
    sep_words[i].sort() 
  # print out...
  for i in range(max_len):
    for j in range(1,len(sep_words[i])):
      print(sep_words[i][j])