import sys
dp_arr = [[[0]*21 for _ in range(21)] for _ in range(21)]

def dp_fn(a,b,c):
  if a<=0 or b<=0 or c<=0:
    return 1
  if a>20 or b>20 or c>20:
    return dp_fn(20,20,20)
  if dp_arr[a][b][c]:
    return dp_arr[a][b][c]
  if a<b<c:
    dp_arr[a][b][c] = dp_fn(a,b,c-1) + dp_fn(a,b-1,c-1) - dp_fn(a,b-1,c)
    return dp_arr[a][b][c]
  dp_arr[a][b][c] = dp_fn(a-1,b,c)+dp_fn(a-1,b-1,c)+dp_fn(a-1,b,c-1)-dp_fn(a-1,b-1,c-1)
  return dp_arr[a][b][c]

if __name__=='__main__':
  while(1):
    a,b,c = map(int, sys.stdin.readline().split())
    if a==-1 and b==-1 and c==-1:
      exit()
    else:
      print(f'w({a}, {b}, {c}) = {dp_fn(a,b,c)}')