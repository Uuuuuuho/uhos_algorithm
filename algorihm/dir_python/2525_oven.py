
def calc(hour, min, dur):
  add_h = int(dur/60)
  add_m = int(dur%60)
  add_h_m = int((min+add_m)/60)

  if(add_h_m>0):
    if(hour+add_h+add_h_m>=24):
      return ((hour+add_h+add_h_m)-24, (min+add_m)-60)
    else:
      return (hour+add_h+add_h_m, (min+add_m)-60)
  else:
    if(hour+add_h+add_h_m>=24):
      return ((hour+add_h+add_h_m)-24, min+add_m)
    else:
      return (hour+add_h+add_h_m, min+add_m)


if(__name__=="__main__"):
  H, M = map(int, input().split())
  D = int(input())
  res_h, res_m = calc(H, M, D)
  print(res_h, res_m)