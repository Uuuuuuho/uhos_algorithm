if __name__=="__main__":
  word = list(input())
  ans = [0]*26
  for i in range(26):
    for ch in word:
      if chr(i+97)==ch:
        ans[i] += 1
  
  for ch in ans:
    print(ch, end=" ")
