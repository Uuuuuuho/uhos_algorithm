import sys

def min_cost(np):
  dp_arr = [[0]*(np+1) for _ in range(np+1)]
  cost = [0] + list(sys.stdin.readline().split())
  acc_sum = [0 for _ in range(np+1)]

  for it in range(1, np+1):
    acc_sum[it] = acc_sum[it-1] + int(cost[it])
  # print(acc_sum)

  for leng in range(1, np):
    for start in range(1, np):
      if start + leng <= np:
        # dp_arr[start][start+leng] = (acc_sum[start+leng] - acc_sum[start-1]) + (min(dp_arr[start][start+leng-1],dp_arr[start+1][start+leng]))
        dp_arr[start][start+leng] = (acc_sum[start+leng] - acc_sum[start-1]) + (min([dp_arr[start][start+k]+dp_arr[start+k+1][start+leng] for k in range(leng)]))

  # print(dp_arr)

  return dp_arr[1][np]

if __name__=="__main__":
  num_test = int(sys.stdin.readline())
  for _ in range(num_test):
    num_pages = int(sys.stdin.readline())
    print(min_cost(num_pages))