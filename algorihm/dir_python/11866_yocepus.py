from collections import deque

que = deque([])
ans = deque([])

if(__name__=="__main__"):
  N, K = map(int, input().split())

  for i in range(1, N+1):
    que.append(i)

  mod = 1

  while(len(que)!=1):
    if((mod % K) == 0):
      ans.append(que[0])
      que.popleft()
      mod = 1
    else:
      que.append(que[0])
      que.popleft()
      mod += 1

  ans.append(que[0])
  print("<", end="")
  for i in range(len(ans)-1):
    print(ans[i], end=", ")
  print(ans[len(ans)-1], end="")
  print(">")
  