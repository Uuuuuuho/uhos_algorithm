import sys

if(__name__=="__main__"):
  arr = [0 for _ in range(9)]
  for i in range(9):
    arr[i] = int(sys.stdin.readline())

  tmp = max(arr)
  index = arr.index(tmp)

  print(tmp)
  print(index+1)