import sys

if __name__=="__main__":
  n_in = int(sys.stdin.readline())
  dp = [[0]*10 for _ in range(101)]
  # print(dp)
  for it in range(1,10):
    dp[1][it] = 1
  
  for snd in range(2,101):
    dp[snd][0] = dp[snd-1][1]
    dp[snd][9] = dp[snd-1][8]
    for fst in range(1,9):
      dp[snd][fst] = (dp[snd-1][fst-1] + dp[snd-1][fst+1]) % 1000000000

  # print(dp)
  sum = 0
  for it in range(0,10):
    sum = (sum + dp[n_in][it]) % 1000000000
  
  print(sum)
