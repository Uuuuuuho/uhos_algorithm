# def pre_trav(tree, visit, v):
#   visit[ord(v)-65] = True
#   print(v,end="")
#   if tree[v][0]==".":
#     None
#   elif visit[ord(tree[v][0])-65]==False:
#     pre_trav(tree,visit,tree[v][0])
#   if tree[v][1]==".":
#     None
#   elif visit[ord(tree[v][1])-65]==False:
#     pre_trav(tree,visit,tree[v][1])

# def in_trav(tree, visit, v):
#   if tree[v][0]==".":
#     visit[ord(v)-65] = True
#     print(v,end="")
#   elif visit[ord(tree[v][0])-65]==False:
#     in_trav(tree,visit,tree[v][0])
#     print(v,end="")
#   if tree[v][1]==".":
#     None
#   elif visit[ord(tree[v][1])-65]==False:
#     in_trav(tree,visit,tree[v][1])

# def post_trav(tree, visit, v):
#   if tree[v][0]==".":
#     None
#   elif visit[ord(tree[v][0])-65]==False:
#     post_trav(tree,visit,tree[v][0])
#   if tree[v][1]==".":
#     print(v,end="")
#   elif visit[ord(tree[v][1])-65]==False:
#     post_trav(tree,visit,tree[v][1])
#     print(v,end="")

# if __name__=="__main__":
#   num_node = int(input())
#   tree={}

#   for _ in range(num_node):
#     root, left, right = input().split()
#     tree[root] = [left,right]
#   visit = [False]*num_node
#   pre_trav(tree,visit,"A")
#   print()
#   visit = [False]*num_node
#   in_trav(tree,visit,"A")
#   print()
#   visit = [False]*num_node
#   post_trav(tree,visit,"A")
#   print()

""" easier way """
def pre_trav(tree, root):
  if root!=".":
    print(root,end="")
    pre_trav(tree, tree[root][0])
    pre_trav(tree, tree[root][1])
def in_trav(tree, root):
  if root!=".":
    in_trav(tree, tree[root][0])
    print(root,end="")
    in_trav(tree, tree[root][1])
def post_trav(tree, root):
  if root!=".":
    post_trav(tree, tree[root][0])
    post_trav(tree, tree[root][1])
    print(root,end="")
    
if __name__=="__main__":
  num_node = int(input())
  tree={}

  for _ in range(num_node):
    root, left, right = input().split()
    tree[root] = [left,right]
  pre_trav(tree,"A")
  print()
  in_trav(tree,"A")
  print()
  post_trav(tree,"A")
  print()
  