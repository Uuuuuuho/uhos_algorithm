v, e = map(int, input().split())

edges = []

for i in range(e):
    a,b,w = map(int, input().split())
    edges.append((a,b,w))

edges.sort(key = lambda x: x[2])

root = list(range(v+1))

# return the root node recursively
def find(node):
    if(node == root[node]):
        return node
    root[node] = find(root[node])
    return root[node]

def union(a,b):
    root_a = find(a)
    root_b = find(b)
    
    if(root_a > root_b):
        root[root_a] = root_b
    else:
        root[root_b] = root_a

output = 0
for a,b,w in edges:
    if find(a) != find(b):
        union(a,b)
        output += w

print(output)