import sys
from collections import deque

deck = deque([])

def push_front(x):
  deck.insert(0,x)

def push_back(x):
  deck.append(x)

def pop_front():
  if(deck):
    print(deck[0])
    deck.popleft()
  else:
    print(-1)

def pop_back():
  if(deck):
    print(deck[len(deck)-1])
    deck.pop()
  else:
    print(-1)

def size():
  print(len(deck))

def empty():
  if(deck):
    print(0)
  else:
    print(1)
  
def front():
  if(deck):
    print(deck[0])
  else:
    print(-1)

def back():
  if(deck):
    print(deck[len(deck)-1])
  else:
    print(-1)

if(__name__=="__main__"):
  num_test = int(sys.stdin.readline())
  for _ in range(num_test):
    cmd = sys.stdin.readline().split()
    if(cmd[0]=="push_front"):
      push_front(cmd[1])
    elif(cmd[0]=="push_back"):
      push_back(cmd[1])
    elif(cmd[0]=="pop_front"):
      pop_front()
    elif(cmd[0]=="pop_back"):
      pop_back()
    elif(cmd[0]=="size"):
      size()
    elif(cmd[0]=="empty"):
      empty()
    elif(cmd[0]=="front"):
      front()
    elif(cmd[0]=="back"):
      back()
