import sys

cnt_code_1 = 0
cnt_code_2 = 0
dp = [0 for _ in range(41)]

def code_1_fib(n):
  global cnt_code_1, dp
  dp[1] = 1
  dp[2] = 1

  for it in range(3,n+1):
    dp[it] = dp[it-1] + dp[it-2]

  cnt_code_1 = dp[n]

def code_2_fib(n):
  global cnt_code_2
  if n>=3:
    for it in range(3,n+1):
      cnt_code_2 += 1


if __name__=='__main__':
  N = int(sys.stdin.readline())
  code_1_fib(N)
  code_2_fib(N)
  print(cnt_code_1, cnt_code_2)
