def min(a,b):
    if (a < b):
        return a
    else:
        return b


house_num = int(input())

# assume red = 0, green = 1, blue = 2
color_cost = [[0]*3 for i in range(house_num)]

for i in range(0, house_num):
    color_cost[i][0], color_cost[i][1], color_cost[i][2] = map(int, input().split())

result_cost = [[0]*3 for i in range(house_num)]
for i in range(0, house_num):
    if (i == 0):
        result_cost[i] = color_cost[i]
    else:
        result_cost[i][0] = color_cost[i][0] + min(result_cost[i-1][1], result_cost[i-1][2])
        result_cost[i][1] = color_cost[i][1] + min(result_cost[i-1][0], result_cost[i-1][2])
        result_cost[i][2] = color_cost[i][2] + min(result_cost[i-1][0], result_cost[i-1][1])

print(min(min(result_cost[house_num-1][0], result_cost[house_num-1][1]),result_cost[house_num-1][2]))