from operator import le
from os import TMP_MAX
import sys

if __name__=='__main__':
  lev = int(sys.stdin.readline())
  dp_arr = [[0]*(lev+1) for _ in range(lev+1)]
  for it in range(1,lev+1):
    tmp = [0] + list(map(int, sys.stdin.readline().split()))
    for tmp_it in range(1, len(tmp)):
      dp_arr[it][tmp_it] = tmp[tmp_it]

  # print(dp_arr)
  
  for row in range(2, lev+1):
    for col in range(1, row+1):
      dp_arr[row][col] = dp_arr[row][col] + max(dp_arr[row-1][col-1], dp_arr[row-1][col])
  
  # print(dp_arr)
  print(max(dp_arr[lev]))
