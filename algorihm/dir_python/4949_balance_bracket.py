from operator import is_


stack = []
flag = True

def push(x):
    stack.append(x)

def pop(x):
  global flag
  # check pairness
  if(x == ")"):
    if("(" != top()): flag = False
  elif(x == "]"):
    if("[" != top()): flag = False
  # check emptiness
  if(len(stack) != 0):
    # not empty
    stack.pop() 
  else:
    flag = False

def top():
  if(len(stack) != 0): 
    idx = len(stack)-1
    return stack[idx]

def is_empty():
  if(len(stack) == 0): return True
  else: return False

if(__name__=="__main__"):
  while(1):
    sentence = list(input())
    # check if done
    if((len(sentence)==1) and (sentence[0]==".")):
      break

    # initialization
    flag = True
    while(len(stack) != 0):
      pop(1)

    for i in range(len(sentence)):
      if(sentence[i]=="("):
        push("(")
      elif(sentence[i]=="["):
        push("[")
      elif((sentence[i]==")") or (sentence[i]=="]")):
        pop(sentence[i])

    # printing
    if((flag == True) and is_empty()): print("yes")
    else: print("no")
