def gcd(a,b):
  while(b>0):
    a,b = b, a%b
  return a

def lcm(a,b):
  return a*b

if __name__=="__main__":
  T = int(input())
  for _ in range(T):
    A, B = map(int, input().split())
    print(lcm(A,B)//gcd(A,B))