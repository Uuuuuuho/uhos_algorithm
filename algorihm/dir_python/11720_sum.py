import sys

if __name__=='__main__':
  case_num = int(sys.stdin.readline())
  in_num = int(sys.stdin.readline())
  # print(in_num)
  ans = 0
  for _ in range(case_num):
    # print(ans, in_num)
    ans += (in_num%10)
    in_num = (in_num//10)
  print(ans)