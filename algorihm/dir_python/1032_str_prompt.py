from ctypes import sizeof


N = int(input())

text = []
for it in range(N):
  text.append(list(input()))

for row in range(1,len(text)):
  for ele in range(len(text[0])):
    if text[0][ele] != text[row][ele]:
      text[0][ele] = "?"

for ele in text[0]:
  print(ele, end='')
