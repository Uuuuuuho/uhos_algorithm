if __name__=="__main__":
  n, m = map(int, input().split())
  cost = [[0]*2 for _ in range(m)]
  for i in range(m):
    cost[i][0], cost[i][1] = map(int, input().split())
  pack = min(x[0] for x in cost)
  indiv = min(x[1] for x in cost)
  pack = min(pack, indiv*6)
  print(min(n//6*pack + n%6*indiv, (n//6+1)*pack))