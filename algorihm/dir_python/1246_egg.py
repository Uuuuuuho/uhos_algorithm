from importlib.machinery import BYTECODE_SUFFIXES


if __name__=="__main__":
  N,M = map(int,input().split())
  buyer = [0]*M
  for i in range(M):
    buyer[i] = int(input())
  
  prices = list(sorted(buyer))

  ans = 0
  ans_cost = 0
  for i in range(M):
    cost = prices[i]
    cnt = min(M-i,N)
    tmp = cost*cnt
    if ans < tmp:
      ans_cost = cost
      ans = tmp
  print(ans_cost, ans)