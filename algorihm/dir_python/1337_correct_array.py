if __name__=="__main__":
  n = int(input())
  arr = []
  for _ in range(n):
    arr.append(int(input()))
  arr_srt = sorted(arr)
  ans = [4]*len(arr_srt)
  idx = 0
  

  for i in range(len(arr_srt)):
    for j in range(1,5):
      if arr_srt.count(arr_srt[i]+j):
        ans[i] -= 1

  # print(arr_srt)
  # print(ans)
  print(min(ans))