import sys
sys.setrecursionlimit(100000)

no_V, no_E = map(int, input().split())

edge = [[] for _ in range(no_V+1)]

for _ in range(no_E):
  v1, v2 = map(int, input().split())
  edge[v1].append(v2)

for e in range(no_V+1):
  edge[e].sort()
  # print(edge[e])

edge_T = [[] for _ in range(no_V+1)]

# print(edge)

for i in range (no_V+1):
  for j in range (len(edge[i])):
    edge_T[edge[i][j]].append(i)

# print(edge_T)

stack = []
check_dfs = [False for _ in range(10001)]

# this dfs works fine for undirectional graphs.
def dfs(v):
  check_dfs[v] = True
  # print(v, end=' ')
  for e in edge[v]:
    if check_dfs[e] == False:
      dfs(e)
  stack.append(v)

check_dfs_T = [False for _ in range(10001)]
scc = []

def dfs_Inv(v):
  check_dfs_T[v] = True
  for e in edge_T[v]:
    if check_dfs_T[e] == False:
      dfs_Inv(e)
  scc.append(v)
  # stack.pop()

for start in range(no_V):
  if((check_dfs[start] == False) and (start != 0)):
    dfs(start)

result = []

while(stack):
  start = stack.pop()
  # print(start)
  if check_dfs_T[start] == False:
    dfs_Inv(start)
    result.append(sorted(scc))
    scc.clear()
    # print(result)

print(len(result))
result = sorted(result)
for i in result:
  print(*i, -1)

