if __name__=="__main__":
  mon = {"January":31, "February":28, "March":31, "April":30, "May":31, "June":30, "July":31, "August":31, "September":30, "October":31, "November":30, "December":31}
  curr = input().split()
  curr[1] = curr[1].replace(",","")
  curr[3] = curr[3].split(":")
  days = 365
  curr_year = int(curr[2])
  if curr_year%4==0:
    if curr_year%100==0:
      if curr_year%400==0:
        days = 366
        mon["February"] = 29
    else:
      days = 366
      mon["February"] = 29
  hour = 24
  min = 60
  nom = days*hour*min

  curr_days = 0
  for key, val in mon.items():
    if key==curr[0]:
      curr_days += int(curr[1])
      break
    else:
      curr_days += val
  
  curr_hour = (curr_days-1)*24 + int(curr[3][0])
  curr_min = curr_hour*60 + int(curr[3][1])
  print(curr_min/nom*100)