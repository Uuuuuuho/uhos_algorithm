def max(a,b):
    if (a<b):
        return b
    else:
        return a

steps = int(input())

score_step = [0 for i in range(301)]
score_result = [0 for i in range (301)]

for i in range (0, steps):
    score_step[i] = int(input())

score_result[0] = score_step[0]
score_result[1] = score_step[1] + score_step[0]
score_result[2] = max(score_step[2] + score_step[1], score_step[2] + score_step[0])

for i in range (3, steps):
    score_result[i] = max(score_step[i-1] + score_step[i] + score_result[i-3], score_step[i] + score_result[i-2])

print(score_result[steps-1])