import sys
from collections import deque

if(__name__=="__main__"):
  test = int(sys.stdin.readline())
  que = deque([])
  while(test!=0):
    A,B = map(int, sys.stdin.readline().split())
    que.append(A+B)
    test -= 1
  
  while(que):
    test += 1
    conc = "Case #" + str(test) + ": " + str(que[0])
    print(conc)
    que.popleft()