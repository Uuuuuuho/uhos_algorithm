dyn_fib = [-1 for _ in range(41)]
dyn_fib_0 = [0 for _ in range(41)]
dyn_fib_1 = [0 for _ in range(41)]

def fib(n):
  if(n == 0):
    dyn_fib[0] = 0
    dyn_fib_0[0] += 1
    return 0
  elif(n == 1):
    dyn_fib[1] = 1
    dyn_fib_1[1] += 1
    return 1
  else:
    if(dyn_fib[n-1] == -1):
        dyn_fib[n-1] = fib(n-1)
    if(dyn_fib[n-2] == -1):
        dyn_fib[n-2] = fib(n-2)
    dyn_fib_0[n] = dyn_fib_0[n-1] + dyn_fib_0[n-2]
    dyn_fib_1[n] = dyn_fib_1[n-1] + dyn_fib_1[n-2]
    return dyn_fib[n-1] + dyn_fib[n-2]

T = int(input())


test_c = []

for i in range(T):
  test_c.append(int(input()))

for i in range(T):
  fib(test_c[i])
  print(dyn_fib_0[test_c[i]], dyn_fib_1[test_c[i]])
  dyn_fib = [-1 for _ in range(41)]
  dyn_fib_0 = [0 for _ in range(41)]
  dyn_fib_1 = [0 for _ in range(41)]
