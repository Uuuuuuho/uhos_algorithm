in_n, in_l = map(int, input().split())

answer = 0
curr_pos = 0
mod = 0
array_n = [0 for _ in range(3)]

for i in range(0, in_n):
    array_n = list(map(int, input().split()))
    answer += array_n[0] - curr_pos
    curr_pos = array_n[0]
    mod = answer % (array_n[1] + array_n[2])
    if (mod <= array_n[1]):
        answer += array_n[1] - mod


answer += in_l - curr_pos
print(answer)