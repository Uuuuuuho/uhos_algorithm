from collections import deque

card_q = deque([])

def pop():
  if not card_q:
    return
  else:
    card_q.popleft()

def move():
  if not card_q:
    return
  else:
    card_q.append(card_q[0])
    pop()

if(__name__=="__main__"):
  N = int(input())

  for i in range(1,N+1):
    card_q.append(i)

  while(len(card_q) != 1):
    pop()
    move()

  print(card_q[0])