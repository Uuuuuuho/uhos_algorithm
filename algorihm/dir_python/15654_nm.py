import itertools

if __name__=="__main__":
  N, M = map(int, input().split())
  nums = map(int, input().split())
  nums = list(sorted(nums))
  nPr = list(itertools.permutations(nums,M))
  for it in nPr:
    for indiv in it:
      print(indiv, end=" ")
    print()