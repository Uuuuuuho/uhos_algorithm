from collections import deque
que = deque([])
min_day = 0

dx = list([-1,1,0,0])
dy = list([0,0,-1,1])

def search():
  global box
  while(que):
    x, y = que.popleft()

    for i in range(4):
      nx = x + dx[i]
      ny = y + dy[i]

      if((0 <= nx < N) and (0 <= ny < M) and (box[nx][ny] == 0)):
        que.append([nx,ny])
        box[nx][ny] = box[x][y] + 1


if(__name__=="__main__"):
  M, N = map(int, input().split())

  # remember this initialization for 2D list
  box = [list(map(int, input().split())) for _ in range(N)]

  for i in range(N):
    for j in range(M):
      if(box[i][j]==1):
        que.append([i,j])
  
  search()

  # debugging purpose
  # print()
  # print()
  # for i in range(N):
  #   for j in range(M):
  #     print(box[i][j], end=' ')
  #   print()

  flag = True
  for i in range(N):
    for j in range(M):
      if(box[i][j] == 0):
        flag = False

  if(flag==False):
    print("-1")
    exit
  else:  
    max_val = -1
    for i in range(N):
      for j in range(M):
        max_val = max(max_val, box[i][j])
    print(max_val-1)