stream = list(input())
pattern = list(input())

def getPi(p):
    m = len(p)
    pi = [0 for _ in range(1000001)]
    j = 0
    for i in range(1,m):
        while((j > 0) and (p[i] != p[j])):
            j = pi[j-1]
        if (p[i] == p[j]):
            j += 1
            pi[i] = j
    return pi

def kmp(s, p):
    n = len(stream)
    m = len(pattern)
    pi = getPi(pattern)
    # print(pi)
    j = 0
    ans_cnt = 0
    ans_idx = [0 for _ in range(1000001)]
    idx = 0
    for i in range(0,n):
        while((j > 0) and (s[i] != p[j])):
            j = pi[j-1]
        if(s[i] == p[j]):
            if(j == (m-1)):
                # print("j: ", j, ", i: ", i)
                # print(idx)
                ans_cnt += 1 
                ans_idx[idx] = i - (m-2)
                idx += 1
                j = pi[j]
            else:
                j += 1
    return ans_cnt, ans_idx

cnt, idx = kmp(stream, pattern)
print(cnt)
for i in range(len(idx)):
    if(idx[i] != 0):
        print(idx[i], end=' ')
print()