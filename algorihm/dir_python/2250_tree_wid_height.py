from glob import glob


depth = 1
def pre_trav(tree, root, curr):
  global depth
  if root!="-1":
    print(root,end=" ")
    depth = max(depth, curr)
    print(depth)
    pre_trav(tree, tree[root][0], curr+1)
    pre_trav(tree, tree[root][1], curr+1)

if __name__=="__main__":
  num_node = int(input())
  tree={}

  for _ in range(num_node):
    root, left, right = input().split()
    tree[root] = [left,right]

  pre_trav(tree,'1', 1)
  print(depth, num_node-1)