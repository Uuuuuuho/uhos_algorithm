
dp_comb = [[0]*31 for _ in range(31)]

def combination(n, r):
  if n==r or r==0:
    return 1
  else:
    comb1 = 0
    comb2 = 0
    if dp_comb[n-1][r-1]!=0:
      comb1 = dp_comb[n-1][r-1]
    else:
      dp_comb[n-1][r-1] = combination(n-1,r-1)
      comb1 = dp_comb[n-1][r-1]
    if dp_comb[n-1][r]!=0:
      comb2 = dp_comb[n-1][r]
    else:
      dp_comb[n-1][r] = combination(n-1,r)
      comb2 = dp_comb[n-1][r]
    return comb1+comb2

if __name__=="__main__":
  num_test = int(input())
  for _ in range(num_test):
    a, b = map(int, input().split())
    dp_comb = [[0]*31 for _ in range(31)]
    if a < b:
      print(combination(b,a))
    else:
      print(combination(a,b))
    # print(dp_comb)

