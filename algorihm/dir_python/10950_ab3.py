from collections import deque

if(__name__=="__main__"):
  test = int(input())
  ans_q = deque([])

  while(test!=0):
    test -= 1
    A, B = map(int, input().split())
    ans_q.append(A+B)
  
  while(ans_q):
    print(ans_q[0])
    ans_q.popleft()