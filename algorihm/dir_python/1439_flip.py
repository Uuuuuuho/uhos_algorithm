if __name__=="__main__":
  bits = list(input())
  zeros = 0
  ones = 0
  bit_r = ""
  if bits[0]=="0":
    zeros = 1
  else:
    ones = 1
  
  for i in range(len(bits)):
    if bits[i]=="0" and bit_r=="1":
      zeros = zeros + 1
    if bits[i]=="1" and bit_r=="0":
      ones = ones + 1

    bit_r = bits[i]

  print(min(ones,zeros))