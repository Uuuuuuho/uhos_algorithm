import itertools

if __name__=="__main__":
  N, M = map(int, input().split())
  nums = map(int, input().split())
  nums = list(sorted(nums))
  nPIr = list(itertools.product(nums,repeat=M))
  for it in nPIr:
    for indiv in it:
      print(indiv, end=" ")
    print()