import sys

if(__name__=="__main__"):
  rest = [0 for _ in range(42)]
  
  for _ in range(10):
    tmp = int(sys.stdin.readline())
    rest[tmp%42] += 1
  
  ans = 0

  for i in range(42):
    if(rest[i]!=0):
      ans += 1
  
  print(ans)