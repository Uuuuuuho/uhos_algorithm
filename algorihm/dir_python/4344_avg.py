import sys

if __name__=="__main__":
  test_case = int(sys.stdin.readline())
  for _ in range(test_case):
    in_case = list(map(int, sys.stdin.readline().split()))
    sum = 0
    ans = 0
    for stds in range(1, in_case[0]+1):
      sum += in_case[stds]
    mean = sum/in_case[0]
    over_mean_cnt = 0
    for stds in range(1, in_case[0]+1):
      if in_case[stds] > mean:
        over_mean_cnt += 1
    ans = round((over_mean_cnt*100)/in_case[0],3)
    print(f'{ans:.3f}%')

