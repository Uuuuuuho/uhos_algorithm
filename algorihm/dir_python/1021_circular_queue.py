import sys
from collections import deque

circular_que = deque([])

def fst_ele():
  circular_que.popleft()
  # print(circular_que.popleft())

def move_left():
  tail = circular_que.popleft()
  circular_que.append(tail)

def move_right():
  head = circular_que.pop()
  circular_que.insert(0,head)

if __name__=="__main__":
  que_size, num_ele = map(int, sys.stdin.readline().split())
  
  for it in range(1, que_size+1):
    circular_que.append(it)

  acq_ele = list(map(int, sys.stdin.readline().split()))
  ans = 0

  for ele in acq_ele:
    idx = circular_que.index(ele)
    if idx == 0:
      fst_ele()
    elif idx < len(circular_que) - idx:
      while circular_que[0] != ele:
        ans += 1
        move_left()
      fst_ele()
    else:
      while circular_que[0] != ele:
        ans += 1
        move_right()
      fst_ele()
  
  print(ans)