import sys

if __name__=='__main__':
  test_case = int(sys.stdin.readline())
  for _ in range(test_case):
    in_ox = list(sys.stdin.readline())
    cnt_o = False
    curr_score = 0
    ans = 0
    for ele in in_ox:
      if ele == "O":
        if not cnt_o:
          cnt_o = True
          curr_score = 1
        else:
          curr_score += 1
        ans += curr_score
      else:
        cnt_o = False
        curr_score = 0
    print(ans)