if __name__=="__main__":
  x,y,z = map(int,input().split())
  tri = [x,y,z]
  while x!=0 and y!=0 and z!=0:
    longest = max(x,y,z)
    tri.remove(longest)
    if longest**2==(tri[0]**2+tri[1]**2):
      print("right")
    else:
      print("wrong")
    x,y,z = map(int,input().split())
    tri = [x,y,z]
    
