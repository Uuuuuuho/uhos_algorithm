import sys

def binary_search(target):
  global arr
  low = 0
  high = len(arr)-1

  while(low<=high):
    mid = int((high+low)/2)
    # print(target, arr[mid], low, mid, high)
    if(arr[mid]==target):
      return True
    elif(arr[mid]>target):
      high = mid-1
    else:
      low = mid+1
  return False

if(__name__=="__main__"):
  N = sys.stdin.readline()
  arr = list(sorted(map(int, sys.stdin.readline().split(' '))))

  M = sys.stdin.readline()
  tmp = list(map(int, sys.stdin.readline().split(' ')))

  for it in range(len(tmp)):
    if(binary_search(tmp[it])):
      print(1)
    else:
      print(0)