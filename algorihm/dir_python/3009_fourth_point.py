if __name__=="__main__":
  pt = [[0]*2 for _ in range(3)]
  pt[0] = list(map(int,input().split()))
  pt[1] = list(map(int,input().split()))
  pt[2] = list(map(int,input().split()))

  x = 0
  y = 0
  if pt[0][0]==pt[1][0]:
    x = pt[2][0]
  elif pt[0][0]==pt[2][0]:
    x = pt[1][0]
  else:
    x = pt[0][0]
    
  if pt[0][1]==pt[1][1]:
    y = pt[2][1]
  elif pt[0][1]==pt[2][1]:
    y = pt[1][1]
  else:
    y = pt[0][1]

  print(x,y)
