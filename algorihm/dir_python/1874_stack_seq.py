import sys
from collections import deque

class Stack:
  def __init__(self):
    self.items = []
  def push(self, item):
    self.items.append(item)
  def pop(self):
    return self.items.pop()
  def peek(self):
    return self[0]
  def isEmpty(self):
    return not self.items

def operation():
  st = Stack()
  que = deque([])
  line_num = int(sys.stdin.readline())
  seq = 0

  for _ in range(line_num):
    inp = int(sys.stdin.readline())

    if(seq<inp):
      while(seq<inp):
        seq += 1
        if(seq==line_num+1):
          print("NO")
          return
        else:
          st.push(seq)
          que.append(1) # 1 equals to '+'(push)
      st.pop()
      que.append(0)  # 0 equals to '-'(pop)
    else:
      top = st.pop()
      if(top!=inp):
        print("NO")
        return
      else:
        que.append(0)
      

  while(que):
    if(que[0]==1):
      print("+")
    else:
      print("-")
    que.popleft()


if(__name__=="__main__"):
  operation()