import sys

if(__name__=="__main__"):
  num_test = int(sys.stdin.readline())
  ans = 0
  for _ in range(num_test):
    word = list(sys.stdin.readline())
    err = False
    for idx in range(len(word)-1):
      if word[idx] != word[idx+1]:
        new_word = word[idx+1:]
        if new_word.count(word[idx]) > 0:
          err = True
          break
    if not err:
      ans += 1
  print(ans)
    