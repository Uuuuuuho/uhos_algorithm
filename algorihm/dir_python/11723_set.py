import sys
lst = []

def add(num):
  if num in lst:
    return
  else:
    lst.append(num)

def remove(num):
  if num not in lst:
    return
  else:
    lst.remove(num)

def check(num):
  if num in lst:
    sys.stdout.write(str(1)+"\n")
  else:
    sys.stdout.write(str(0)+"\n")

def toggle(num):
  if num in lst:
    lst.remove(num)
  else:
    lst.append(num)

def all():
  while lst:
    lst.pop()
  for i in range(20):
    lst.append(i+1)

def empty():
  while lst:
    lst.pop()

if __name__=="__main__":
  ops = int(sys.stdin.readline())
  for _ in range(ops):
    cmd = list(sys.stdin.readline().split())
    # print(cmd)
    if cmd[0]=="add":
      add(int(cmd[1]))
    elif cmd[0]=="remove":
      remove(int(cmd[1]))
    elif cmd[0]=="check":
      check(int(cmd[1]))
    elif cmd[0]=="toggle":
      toggle(int(cmd[1]))
    elif cmd[0]=="all":
      all()
    elif cmd[0]=="empty":
      empty()