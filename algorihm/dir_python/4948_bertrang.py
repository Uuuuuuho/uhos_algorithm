import sys
import math
from collections import deque


if(__name__=="__main__"):
  ans = deque([])
  while(1):
    N = int(sys.stdin.readline())
    if(N==0):
      break

    primes = [1]*(2*N+1)
    primes[0] = 0
    primes[1] = 0
    cnt = 0

    for i in range(2, int(math.sqrt(2*N))+1):
      for j in range(int(N/i)*i,2*N+1,i):
        if(primes[j]==1):
          primes[j] = 0

    for i in range(N+1, 2*N+1):
      if(primes[i]==1):
        cnt += 1
    
    ans.append(cnt)

  for i in ans:
    print(i)
