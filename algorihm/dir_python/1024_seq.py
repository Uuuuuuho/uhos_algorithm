if __name__=="__main__":
  N,L = map(int,input().split())
  fst = N//L
  while True:
    if L>100 or fst<0:
      print(-1)
      exit()
    sum = L*(2*fst+(L-1))//2
    if sum==N:
      break
    fst -= 1  
    if sum<N:
      L += 1
      fst = N//L
  
  for i in range(fst,fst+L):
    print(i,end=" ")