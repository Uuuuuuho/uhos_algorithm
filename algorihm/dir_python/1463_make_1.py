import sys

if __name__=="__main__":
  X = int(sys.stdin.readline())
  LUT = [0 for _ in range(X+1)]

  for it in range(2,X+1):
    LUT[it] = LUT[it-1] + 1

    if it % 2 == 0:
      LUT[it] = min(LUT[it], LUT[it>>1] + 1)
    if it % 3 == 0:
      LUT[it] = min(LUT[it], LUT[int(it/3)] + 1)

  print(LUT[X])