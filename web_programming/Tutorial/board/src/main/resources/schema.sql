CREATE TABLE tbl_board(
    boardID Long auto_increment,
    title varchar (30) not null,
    content varchar (30) not null,
    name varchar (30) not null,
    primary key (boardID)
);

INSERT INTO tbl_board(boardID, title, content, name) VALUES ('title', 'content', 'name')