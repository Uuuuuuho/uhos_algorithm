package com.example.hellospring.service;

import com.example.hellospring.domain.Member;
import com.example.hellospring.repository.MemoryMemberRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MemberserviceTest {
    MemberService memberService;
    MemoryMemberRepository memberRepository;

    @BeforeEach
    public void beforeEach(){
        memberRepository = new MemoryMemberRepository();
        memberService = new MemberService(memberRepository);
    }

    @AfterEach
    public void afterEach(){
        memberRepository.clearStore();
    }

    @Test
    public void CreateAccount() throws Exception {

//        given
        Member member = new Member();
        member.setName("hello");

//        when
        Long saveId = memberService.join(member);

//        then
        Member findMember = memberRepository.findById(saveId).get();
        Assertions.assertEquals(member.getName(), findMember.getName());

//        redundant
//        Member member2 = new Member();
//        member2.setName("hello");
//        Long saveId2 = memberService.join(member2);
//        Member findMember2 = memberRepository.findById(saveId2).get();
//        Assertions.assertEquals(member.getName(), findMember2.getName());

    }

    @Test
    public void Redundant() throws Exception{
//        given
        Member member1 = new Member();
        member1.setName("spring");
        Member member2 = new Member();
        member2.setName("spring");

//        when
        memberService.join(member1);
        IllegalStateException e = Assertions.assertThrows(IllegalStateException.class, ()-> memberService.join(member2));
        assertThat(e.getMessage()).isEqualTo("Already Exists");
    }
}
