package hello.core.singleton;

public class StatefulService {
    private int price;

    public void order(String name, int price) {
        System.out.println("name = " + name + " price = " + price);
        /* this is the problem with singleton design */
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
