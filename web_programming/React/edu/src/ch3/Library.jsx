import react from "react";
import Book from "./Book";

function Library(props) {
  return (
    <div>
      <Book name="First Python" numOfPage={300}/>
    </div>
  );
}

export default Library;