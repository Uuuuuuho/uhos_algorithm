import React from "react";

function Book(props) {
  return (
    <div>
      <h1>{`Book : ${props.name}`}</h1>
    </div>
  );
}

export default Book;