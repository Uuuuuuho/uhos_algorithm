import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import reportWebVitals from './reportWebVitals';
// Chapter 3
import Library from './ch3/Library'
// Chapter 4
import Clock from './ch4/Clock';
// Chapter 5
import CommentList from './ch5/CommentList'
// Chapter 6
import NotificationList from './ch6/NotificationList'
// Chapter 7
import Accomodate from './ch7/Accomodate'




// Default
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

// Chapter 3
// ReactDOM.render(
//   <React.StrictMode>
//     <Library />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// Chapter 4
// Not working. Didn't reason
// setInterval( () => {
//   ReactDOM.render(
//     <ReactDOM.StrictMode>
//       <Clock />
//       {/* <Library /> */}
//     </ReactDOM.StrictMode>,
//     document.getElementById('root')
//   );
// }, 1000);

// Chapter 5
// ReactDOM.render(
//   <React.StrictMode>
//     <CommentList/>
//   </React.StrictMode>,
//   document.getElementById('root')
// )

// Chapter 6
// ReactDOM.render(
//   <React.StrictMode>
//     <NotificationList />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// Chapter 7
ReactDOM.render(
  <React.StrictMode>
    <Accomodate />    
  </React.StrictMode>,
  document.getElementById('root')
)
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
