import React from "react";
import Comment from "./Component";

const comments = [
  {
    name: "BB",
    comment: "Hello, I'm BB.",
  },
  {
    name: "CC",
    comment: "Hello, I'm CC.",
  },
  {
    name: "DD",
    comment: "Hello, I'm DD.",
  },
]

function CommentList(props) {
  return (
    <div>
      {comments.map((comment)=> {
        return (
          <Comment name={comment.name} comment={comment.comment}/>
        )
      })}
    </div>
  )
}

// function CommentList(props){
//   return (
//     <div>
//       <Comment name={"JB"} comment={"Hello, reply~!"}/>
//       <Comment name={"AA"} comment={"Fun React!"}/>
//     </div>
//   )
// }

export default CommentList;