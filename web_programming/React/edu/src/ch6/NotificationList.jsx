﻿import React from "react";
import Notification from "./Notification";

const reservedNotifications = [
    {
        message: "Hello, here's to do today"
    },
    {
        message: "Lunch time."
    },
    {
        message: "It's time for the meeting."
    },

]

var timer;

class NotificationList extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            notifications: [],
        };
    }

    componentDidMount(){
        const {notifications} = this.state;
        timer = setInterval(()=> {
            if(notifications.length < reservedNotifications.length){
                const index = notifications.length;
                notifications.push(reservedNotifications[index]);
                this.setState({
                    notifications : notifications,
                });
            }
            else {
                clearInterval(timer);
            }
        }, 1000);
    }

    render(){
        return (
            <div>
                {this.state.notifications.map((notifications)=> {
                    return <Notification message = {notifications.message} />;
                })}
            </div>
        )
    }
}

export default NotificationList;