import React, {Component} from "react";

// class MyName extends Component{
//   render(){
//     return (
//       <div>
//         Hello! My name is <b>{this.props.name}</b>
//       </div>
//     )
//   }
// }

// MyName.defaultProps = {
//   name: 'Default'
// }

// function Component
const MyName = ({name}) => {
  return (
    <div>
      Hello! My name is {name}!
    </div>
  );
};
export default MyName;