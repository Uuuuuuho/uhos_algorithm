import logo from './logo.svg';
import './App.css';
import React, {Component} from 'react';
import {ReactDOM} from 'react';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div> 
//   );
// }


// class App extends Component{
//   render(){
//     // Place "const variable" in front of return...
//     const name = 'react';
//     const value = 1;
//     const style = {
//       backgroundColor: 'black',
//       padding: '16px',
//       color: 'white',
//       fontSize: '12px'
//     }
//     return (
//       // Tag should be closed
//       <div style = {style}>
//       {/* <div> */}
//         {/* 삼항연산자 */}
//         {/* {
//           1+1===2
//             ? (<div>Correct!</div>)
//             : (<div>Incorrect!</div>)
//         } */}
//         {/* Wrapped Element */}
//         {/* <div>
//           Hello
//         </div>
//         <div>
//           Bye
//         </div> */}
//         {/* condtion */}
//         {
//           (function() {
//             if (value === 1) return (<div>ONE</div>);
//             if (value === 2) return (<div>TWO</div>);
//             if (value === 3) return (<div>THREE</div>);
//           })()
//         }
//       </div>
//     )
//   }
// }

class App extends Component{
  // App component styling
  render(){
    return(
      <div className='App'>
        React!!
      </div>
    )
  }
}


class Square extends React.Component {
  render() {
    return (
      <button className="square">
        {/* TODO */}
      </button>
    );
  }
}

class Board extends React.Component {
  renderSquare(i) {
    return <Square />;
  }

  render() {
    const status = 'Next player: X';

    return (
      <div>
        <div className="status">{status}</div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}
// export default Game;
export default App;
