import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import App from './MyName';
// import App from './App_orig';
// import Game from './Game';
import reportWebVitals from './reportWebVitals';
// ch3 Library
import Library from './ch3/Library'

ReactDOM.render(
  <React.StrictMode>
    <Library/>
  </React.StrictMode>,
  document.getElementById('root')
)

// ch4 Clock example
// import Clock from './ch4/Clock';

// setInterval(() => {
//   ReactDOM.render(
//     <React.StrictMode>
//       <Clock />
//     </React.StrictMode>,
//     document.getElementById('root')
//   )
// },1000);

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     {/* <Game /> */}
//     {/* <App /> */}
//     {/* <MyName /> */}
//   </React.StrictMode>
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
