import { render, screen } from '@testing-library/react';
// import App from './App';
import Game from './Game';

test('renders learn react link', () => {
  render(<Game />);
  // render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
